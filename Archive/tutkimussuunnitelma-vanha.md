# Tutkimussuunnitelma

## Working title


## Johdanto


Ajatuksenani on tehdä ensi viikolla lyhyt tutkimussuunnitelma ja seuraavalla viikolla aloittaa toteuttaminen.

Valvoja ???

Dippani ohjaisi Sami Vihavainen ja projektissa olisi mukana Hanna-Riikka Sundberg, joka on myös väitellyt tohtoriksi.

Projektissa on mukana Lancasterin yliopisto ja he vastaavat myöhemmin persoonien validoinnista. Leadinilla ja minulla voisi olla mahdollista rakentaa heidän kanssa yhteistyötä ja päästä mukaan heidän projektiin liittyviin artikkeleihin.

Tavoitteena on edelleen valmistua kesäkuun kokouksessa, eli huhtikuun lopussa pitäisi olla lähes valmista.

## Tutkimuksen tavoitteet

Aiheena olisi rakentaa prosessi persoonien ja/tai skenaarioiden rakentamiselle sekundaarisen ja kvalitatiivisen datan perusteella.

Koko projektin tavoitteena on luoda "multi-platform application toolkit” televisiosisältöä varten. Live ja on demand -sisältöä voisi vaihdella lennosta useilla eri näyttöpäätteillä. Sisältöön voisi rakentaa applikaatioita esimerkiksi äänestyksiä tai muuta käyttäjän osallistumista varten.

Projekti on parivuotinen ja tässä vaiheessa meidän firman osuutena on luoda persoonat ja skenaariot palvelua käyttävistä kuluttajista. Käytössä on käytännössä tilastotietoa eri lähteistä, enimmäkseen julkisista lähteistä. Dippani tukisi tätä osaa projektista, osallistuisi datan keräykseen ja käsittelyyn.

Projektin nettisivut: http://mpat.irt.de/


## Taustakirjallisuus

Hakusanoja:
- quantitative persona
- secondary data persona ?



## Tutkimuskysymykset

Aiheena olisi rakentaa prosessi persoonien ja/tai skenaarioiden rakentamiselle sekundaarisen ja kvalitatiivisen datan perusteella.


## Metodologia
miten dataa hankitaan ja analysoidaan


## Odotettavissa olevat tulokset

Prosessi kvanttidatan käytöstä persoonien ja skenaarioiden luomisessa