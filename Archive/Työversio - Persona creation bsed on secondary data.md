# Persona creation based on secondary data

*Pekka Siika-aho*

Project plan January 11th - preliminary version

# TODO

- Aloita Datan keruu


## Notes 13.1

- Kaikki dokumentoidaan
	+ Kaikki dokumentointi
- Next TODO
	+ Selvitä akateeminen ad hoc persoona generointi kirjallisuus


# Introduction

The aim of the thesis is to explore persona creation with the help of secondary data. The secondary data can come from statistical or demographic data, market research, public research or other internal projects. The secondary data can be either qualitative or quantitative in nature. The common factor is that the data is not collected initially for this project, thus it is secondary data.

Persona is a design tool popularized by Cooper (1999) that helps the designer to empathize with the end user. Cooper (1999) defines persons to be hyphotetical archetypes of actual users. Persona is formed by combining aspects of multiple target users into one relatable 'person', a persona. Usually personas are formed based on empirical research: e.g. interviews and ethnographies.

<!--

	Kirjoitin pikku pätkän persoonista Leadinin sharepointiin viime keväänä taustaksi. Pyrki avaamaan hieman historiaa ja eri koulukuntia.

	https://leadin.sharepoint.com/sites/Leadin24.fi/_layouts/15/start.aspx#/Lists/Viestit/Post.aspx?ID=84

	Projects/MPAT/Literature

	Inmates running asylum

 -->

The thesis research tackles these main issues:

- how personas can be formed based on secondary data
- can secondary data produce reliable personas
- how well the generated personas can communicate real user needs to the designers

	Oletus: Even inaccurate personas add value to design process, if they are precise enough.

The outcome of the research is expected to be a process that can be used to generate valid and design-inspiring personas from secondary data. The process should be general enough that it can be used in other contexts and projects.

# Schedules

## External deadlines

- Personas deliverable to MPAT DL Feb 29th (wk 10)
- First complete version of thesis DL Apr 29th (wk 18)
- Revised, finalized & printed thesis June 3rd (wk 23)

## Weekly schedule

|                  WP                  |       Weeks       |
|--------------------------------------+-------------------|
| Project plan                         | 2, 3              |
| Literature review & related research | Every week, 1 day |
| Persona data collection              | 3-7               |
| Persona generation                   | 4-9               |
| Persona evaluation                   | 10-14             |
| Analysis                             | 11, 13-17         |
| Discussion                           | 16-17             |
| Conclusions                          | 16-17             |
| Editing                              | 18, 20-22         |
| Presenting the thesis                | May               |
| Maturity examination                 | May 25th, wk 22   |
|                                      |                   |



# Related Research

- A. Cooper's (1999) introduction to the concept of personas
	+ Other persona literature
- Data based persona generation methods
	+ Qualitative
	+ Quantitative
- Market segmentation literature
	+ To support quantitative persona generation literature
- Scenario based user insight literature
- How user insight is incorporated into design work, other than personas?
- NPD (New Product Development) user insight literature

## Search keywords

Persona creation and development:

- Persona creation/generation/development
+ Qualitative persona creation/generation/development
+ Quantitative persona creation/generation/development
+ Persona based design
+ Provisional / ad hoc / prospective persona

Scenario creation and development:

+ Scenario creation/generation/development

User insight creation and development:

+ User insight creation/generation/development
+ User insight design

Marketing literature on focus groups, market and user segments:

- Focus group creation/generation/development
- Market segment creation/generation/development
- User segment creation/generation/development


## Research gap

***N.B.*** *Research gap section is based on relatively short research. Might be wrong or inaccurate.*

*TODO* Tsekkaa provisional/ad hoc/prospective personat

	Voisi olla hyvä katsoa mitä eräs persoonapioneeri Tamara Adlin on tehnyt. Käsittääkseni hän tekee bisnestä provisional persoonilla, mutta datan keruuseen käyttänee workshoppeja asiakkaisen kanssa.

	http://adlininc.com/adhoc/

There is little research on personas generated from qualitative data without machine learning tools and advanced algorithms. Sometimes project situation doesn't allow for qualitative research based persona generation. Light weight qualitative persona generation process can be used to generate valid and reliable personas to help with design work.


# Methods

## Persona data collection

User demographics and data are garthered from public research, public data sources and from partners. Possible data sources include HbbTV research, TV usage statistics, hybrid television usage research, open data libraries from public entities in Finland and abroad, TV marketing research.



<!--

	Yksi melko ratkaiseva juttu datan keruun ja MPATin kannalta on varmasti on että kuinka data/käyttäjät rajataan. Mulla ei ihan tarkkaa kuvaa ole. Ensin ajattelin että nimenomaan HbbTV:n liittyvä data/tutkimukset. Mutta ehkä järkevämpi olla hirttäytymättä tuohon “tavaramerkkiin” ja etsiä dataa/tutkimusta yleisemmin TV:n, mobiilin, tietokoneen yhteiskäyttötavoista. Vai kuinka näette? Onko MPATissa määritelty tätä? Tuolla laajemmalla skoopilla luulisi löytyvät kvalitutkimustakin joka on pureutunut ihmisten tavoitteisiin (goals) etc. Jotka perinteisesti on persoonien lähtökohta ja johon kvantitatiivinen tilastotieto ei yleensä sellaisenaan vastaa.

	Ericsonilta tuore tutkimus http://www.ericsson.com/res/docs/2015/consumerlab/ericsson-consumerlab-tv-media-2015.pdf

	Ei pelkkää HbbTV:tä.

	Orig TV, web, mobile connected devices (Tsekkaa projektisuunnitelma):
		connected TV, web, mobile connected devices

 -->

## Persona formation

Persona formation research will be consulted and reviewed for best practices on persona formation. Marketing research on customer segments and customer insight will supplement the quantitative methodologies. It is likely that the literature does not contain a feasible process to be applied in this process. In that case, a new process for persona formation will be created and documented.

## Evaluation of personas by interviewing UX Designers who work with them

	Kysymys mikä vielä jää hieman avoimeksi on se miten varmistetaan persoonien paikkansapitävyys. Se että kysytään meidän suunnittelijoilta on hyvä kuvaamaan kuinka käyttökelpoiseksi he kokevat persoonan sisällön/formaatin (tut. Kys. 3.). Se ei kuitenkaan kerro sitä kuinka luotettava representaatio persoona on todellisuudesta (tut. kys 2. ). Ehkä tällä aikataululla se että linkkaa persoonan mahdollisimman hyvin dataan ja näyttää (visualisoi?) sen hyvin on paras tapa kertoa luotettavuudesta. Sanoisin että jos löytää tuohon jonkun hyvän tavan on se jo sellaisenaan hyvä kontrbuutio. (voisiko olla jopa dipan pääkontribuutio?)

	Focus group sessio?

Two to four interviews will be conducted with Leadin UX designers, who will use the personas to support their design work.

**Open question**: What measures will be used to examine the success or failure of the process or the personas?

Possible measures:

- Do the personas communicate relevant user insight to the designers to base their design on?
- Do designers feel confident continuing with the design task based on the personas?

Other possible evaluating methods:

- Interviews with target user segment (Are the personas representative of the target user group?)
- Interviews with platform developers in German partner organizations
- Expert reviews on personas


# Results

## The process used to create the personas

## Evaluation results


# Analysis

Do the results answer the research questions? What are the answers?

*From introduction:*
- how personas can be formed based on statistical and secondary data
- can secondary and statistical data produce reliable personas
- how well the generated personas can communicate real user needs to the designers


Sources of the success/failure of personas?
- Data sources
- Persona generation process
- Test setting or other external factors?
- Something else?


# Discussion

What might the answer imply and why does it matter? How does it fit in with what other researchers have found? What are the perspectives for future research?

Is the process generalizable? Can it be used in other projects? What are the limits of the process?

How do the results compliment the existing literature?

Future research topics?

- How well the personas match the later user research in the project?
- Can the developers of the platform utilize the personas?
- Can the developers of the plugins utilize the personas?
- How well the personas translate into user experience of end users?


# Conclusions

Concluding remarks.