# Non-consumers of media

- DM1N - Analogue negative
- SMF 8 - Reijot ja riitat, keski-ikäiset laadun arvostajat
- S1 - Non-prosumers

Summary:

They don't see the need to use TV or other media. Their life is not focused on media at all. They might use it from time to time, but the use is occasional and inconsistent. They value other things in life than media and that show's in their behaviour.