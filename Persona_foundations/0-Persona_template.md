# Persona template


## Private information

- Name
- Age
- Gender
- Marital status
- Number of children
- Where does the persona live?
- Disabilities
- Role(s) and tasks
	+ Specific company or industry
	+ Job title or role
	+ Salary/disposable income

## Description of work and daily life

+ What does the persona do during an average (work) day in connection to the focus area?
+ Typical activities
+ Important atypical activities
- Describe the persona's use of different digital platforms?
	+ Which sources of video media are used?
	+ How much they are used?
	+ What they are used for?
- What is the personas relation to different medias?

## Favourite shows


## Media use goals

- Motivations
- Behaviour

## Pain points

- Frustrations

## Second screen use

- If different digital platforms are used together? Second screening habits


## Social context of TV media use

## Skills and knowledge

- General computer and/or internet use
- Frequently used products, product knowledge
- Special skills


----


## Sisäiset kommentit

- VOD & TV käyttötottumukset
	+ Mistä video tulee?
	+ Mitä videoita katsoso? youtube vs haluatko miljonääriksi
- Päätelaitteet
- Social media use
- Mistä design patternit avuksi
- Kuinka suhtautuu televisioon laitteena?
	+ käyttääkö kaukosäädintä
- Katsooko yksin vai ryhmässä?
	+ Halutaanko sisältöä tuoda sosiaaliseen kanssakäymiseen mukaan? esim jälkeenpäin keskusteluihin tai livenä yhdessä
- Eläkeläiset
	+ Fyysisiä/henkisiä rajoitteita
	+ punavihersokeus ym., huono näkö
(- Kuinka suhtautuu ongelmatilanteisiin?)


## Social media use and relation to social media

## Other relations to media



## Influencers that surround the persona and that may influence choices

- Partner, children, boss etc

## Goals

- Short term, long term
- Motivations
- Work-related goals
- Product-related goals
- General (life) goals, aspirations
- Stated and unstated desires for the product



## Context/environment

- Equipment
- "A day in the life" description
	+ Work styles
	+ Time line of a typical day
- Specific usage location(s)
- General work, household and leisure activities


---

# Persona template requirements

- VOD & TV käyttötottumukset
	+ Mistä video tulee?
	+ Mitä videoita katsoso? youtube vs haluatko miljonääriksi
- Päätelaitteet
- Social media use
- Mistä design patternit avuksi
- Kuinka suhtautuu televisioon laitteena?
	+ käyttääkö kaukosäädintä
- Katsooko yksin vai ryhmässä?
	+ Halutaanko sisältöä tuoda sosiaaliseen kanssakäymiseen mukaan? esim jälkeenpäin keskusteluihin tai livenä yhdessä
- Eläkeläiset
	+ Fyysisiä/henkisiä rajoitteita
	+ punavihersokeus ym., huono näkö
(- Kuinka suhtautuu ongelmatilanteisiin?)

## @nielsen_personas_2014

"When the spectator or reader engages with the character, it happens on three levels: recognition, alignment, and allegiance (Smith 1995). Recognition happens when the reader uses the information available to construct the character as an individual person. ... The alignment happens when the reader can identify with the actions, knowledge, and feelings of the character. ... The last level, allegiance, that includes the moral evaluation that the reader makes vis-à-vis the character but also the moral evaluation that the text allows the reader to create. The allegiance is thus both dependant on the access to understand the actions of the character and the reader’s ability to understand the context within which the story plays out."

"But for the reader to engage, it has to be possible to feel both sympathy and empathy. This means that engagement partly is created based on the informa- tion that the text provides and partly based on the reader’s own experiences."

Template:

- Private information
	+ Age, job marital status, where does the persona live, personal interests
	+ Consider what you want these information to convey about the persona
	+ Watch out for stereotypes
	+ Consider if you have described only one character trait in different forms
- Use of focus area, relation to focus area
	+ How much does the persona use the focus area?
	+ What is it used for?
	+ Hod does the persona relate to the focus area?
- Media use, relation to media use (internet, mobile phone etc) - If relevant
	+ Describe the persona's use of different digital platforms
	+ How much they are used
	+ What they are used for?
	+ If different digital platforms are used together
	+ What is the personas relation to different medias?
- Description of work or daily life
	+ waht does the persona do during an average (work) day in connection to the focus area?
- Other relations to the focus area
	+ Motivations
	+ Frustrations
	+ Behaviour
	+ Touch-points
- Influencers that surroudn the persona and that may influence choices
	+ Partner, children, boss etc

## @pruitt_persona_2006

"At the very least, complete personas must include core information essential to defining the persona: the goals, roles, behaviors, segment, environment, and typical activities that make the persona solid, rich, and unique (and, more importantly, relevant to the design of your product)."

"A lofty but worthy goal is to have every statement in your foundation document supported by user data. You likely will not achieve this, but the attempt helps you to think critically about your details, and highlights places where you might want to do further research."

"As you build these detailed depictions, you will be making educated guesses and adding fictional elements, some of which will be directly related to the data you have collected and some of which will not. (It is a good idea to document these assumptions and to consider them possible research questions that may need answering during the validation of your personas.)"

"Allow realism to win out over political correctness. Avoid casting strongly against expectations if it will under- mine credibility"

- Identifying details
	+ Name, title or short description
	+ Age, gender
	+ Identifying tag line
	+ Quote (highlighting something essential to that persona, preferably related to the product)
	+ Photograph or brief physical description
- Role(s) and tasks
	+ Specific company or industry
	+ Job title or role
	+ Typical activities
	+ Important atypical activities
	+ Challenge areas or breakdowns, pain points
	+ Responsibilities
	+ Interactions with other personas, systems, products
- Goals
	+ Short term, long term
	+ Motivations
	+ Work-related goals
	+ Product-related goals
	+ General (life) goals, aspirations
	+ Stated and unstated desires for the product
- Segment
	+ Market size and influence
	+ International considerations
	+ Accessibility considerations
	+ General and domain-relevant demographics
		* Income and purchasing power
		* Region or city, state, country
		* Education level
		* Marital status
		* Cultural information
- Skills and knowledge
	+ General computer and/or internet use
	+ Frequently used products, product knowledge
	+ Years of experience
	+ Domain knowledge
	+ Training
	+ Special skills
	+ Competitor awareness
- Context/environment
	+ Equipment
	+ "A day in the life" description
		* Work styles
		* Time line of a typical day
	+ Specific usage location(s)
	+ General work, household and leisure activities
	+ Relationships to other personas
- Psychographics and personal details
	+ Personality traits
	+ Values and attitudes (political opinions, religion)
	+ Fears and obstacles, pet peeves
	+ Personal artifacts (car, gadgets)