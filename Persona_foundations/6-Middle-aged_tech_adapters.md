# Digitally adapted middle-agers

- DM2P - Digital positive
- SMF7 - Harrit, urakeskeiset tiedolla johtavat miehet
- SMF4 - Piat ja Petterit, nopeatahtiset perheelliset
- CD3 - TV & Multimedia
- M2 - Family man who takes care of IT at home

They adopted internet at a later age, but they have adapted it fully. Their history of traditional media usage is clearly seen. They watch their content through television, but use digital television recordings and VOD services to set the times they watch the content. Their life is very busy because of family or career. They like to use media for learning or other useful resources.

# Persona template

## Private information

Name: Danny

Tagline:

Age: 47

	SMF4:
	25-34: 37%
	35-44: 47%
	45-54: 13%
	Avg: 37

	SMF7:
	25-34: 6%
	35-44: 29%
	45-54: 54%
	55-64: 10%
	Avg: 46

	M2:
	57

Gender: Male

	SMF4:
	M: 56%
	F: 44%

	SMF7:
	M: 76%
	F: 24%

	M2:
	Male

Marital status: Married

	SMF4:
	Partner & Children: 85%

	SMF7:
	Single: 34%
	Partner: 23%
	Partner & children: 34%

	M2: Wife & 2 children

Number of children: 2, aged 14 & 18

	SMF4:
	Partner & Children: 85%

	SMF7:
	Partner & Children: 34%

	M2: 2, both have moved out

Where does the persona live? In an owned apartment near the center of Turku

	SMF4 & SMF7: Kaupunkimainen, ei pk-seutu

	Townhouse in a small town in Sweden

Disabilities?

	Puna-viher-sokeus? Ei dataa. Miehillä yleisempää.

## Role(s) and tasks

Specific company or industry


Job title or role: IT manager at a shipyard

	M2:
	Integration coordinator

Typical activities:

Hektisen arjen keskellä vapaa-aika kuluu pääosin lasten kanssa puuhaillessa ja heidän harrastuksiaan seuraillen. Kun omaa aikaa on, käytetään se juoksulenkillä

ovat työelämässä aktiivisia ja käyttävät usein myös osan vapaa-ajastaan iltaisin ja viikonloppuisin töiden tekemiseen.

On the weekends he spends a lot of time watching live sport


	SMF4:
	"Hektisen arjen keskellä vapaa-aika kuluu pääosin lasten kanssa puuhaillessa ja heidän harrastuksiaan seuraillen. Kun omaa aikaa on, käytetään se juoksulenkillä, joukkuelajeja pelaillen tai DIY (Do It Yourself) -harrastusten parissa. He ovat valmiita käyttämään rahaa laadukkaaseen sekä itselleen että perheelleen mieluisaan vapaa-ajanviettoon."

	Joukkuelajit

	SMF7:
	"Harrit ovat työelämässä aktiivisia ja käyttävät usein myös osan vapaa-ajastaan iltaisin ja viikonloppuisin töiden tekemiseen. Harrastuksille heillä jää vain vähän aikaa, mutta esimerkiksi viikoittaisista tennisvuoroista haluavat pitää kiinni. Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä."

	M2:
	"Before he goes to bed he and his wife watches some series on SVT Play. On the weekends he spends a lot of time watching live sport, especially football premier league. At the evenings, if they don’t have friends over, they usually watch television."


Important atypical activities

Salary/disposable income

	SMF4:
	Average # of earners in household: 1,92
	Average earnings of household: 3800
	Average earnings: 2000

	SMF7:
	Average # of earners in household: 1,6
	Average earnings of household: 3280
	Average earnings: 2015


## Description of work and daily life

What does the persona do during an average (work) day in connection to the focus area?

### Kiire

elävät todellisia ruuhkavuosia ja kokevat aikapaineita sekä työssä että vapaalla. Hektisen arjen keskellä vapaa-aika kuluu pääosin lasten kanssa puuhaillessa ja heidän harrastuksiaan seuraillen.

On usein tunne, ettei tehdi vapaa-ajalla kaikkea haluamaansa


### Ura

myös osan vapaa-ajastaan iltaisin ja viikonloppuisin töiden tekemiseen.

he haluavat koko ajan kehittää itseään työhön liittyen. He katsovat usein itseä kiinnostavia sisältöjä netti-TV:stä omien aikataulujensa mukaisesti


### Hajakäyttö

käyttävät sujuvasti eri medioita silloin kun heillä on siihen aikaa, usein töiden lomassa, arjen keskellä tai liikennevälineissä.

Katsoo uutisotsikoita netistä aina kun mahdollista

On aiempaa yleisempää, että lukee/katsoo artikkelista/ohjelmasta vain pätkän


### Rentoutuminen

Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

Musiikki, viihde tai urheilu tarjoavat heille elämyksiä hektisen työn vastapainoksi.

Välittävät mielellään muille eteenpäin löytämiään kiinnostavia artikkeleita ja sisältöjä.

Before he goes to bed he and his wife watches some series on SVT Play (a Swedish video streaming service).



	SMF4:
	"Piat ja Petterit elävät todellisia ruuhkavuosia ja kokevat aikapaineita sekä työssä että vapaalla. Hektisen arjen keskellä vapaa-aika kuluu pääosin lasten kanssa puuhaillessa ja heidän harrastuksiaan seuraillen.

	Piat ja Petterit käyttävät sujuvasti eri medioita silloin kun heillä on siihen aikaa, usein töiden lomassa, arjen keskellä tai liikennevälineissä. Heille on tärkeää oman arjen helpottaminen ja oman perheen hyvinvointiin vaikuttava tieto. He lukevat usein kevyempiä uutisia ja viihdesisältöjä. Heillä ei ole useinkaan mahdollisuutta syventyä tarkemmin uutisiin.

	Pysähtyvät lukemaan ensin kevyempiä uutisia nettiotsikoista, hakeutuvat television viihdesisältöjen pariin ja valikoivat mitä teemoja seuraavat.

	Musiikki, viihde tai urheilu tarjoavat heille elämyksiä hektisen työn vastapainoksi.

	Välittävät mielellään muille eteenpäin löytämiään kiinnostavia artikkeleita ja sisältöjä.

	On usein tunne, ettei tehdi vapaa-ajalla kaikkea haluamaansa

	On aiempaa yleisempää, että lukee/katsoo artikkelista/ohjelmasta vain pätkän

	Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota

	On nykyisin lähes jatkuvasti jonkun median tavoitettavissa"

	SMF7:
	"Uralla ja työllä on tärkeä rooli heidän elämässään ja he haluavat koko ajan kehittää itseään työhön liittyen. He katsovat usein itseä kiinnostavia sisältöjä netti-TV:stä omien aikataulujensa mukaisesti.

	Harrit ovat työelämässä aktiivisia ja käyttävät usein myös osan vapaa-ajastaan iltaisin ja viikonloppuisin töiden tekemiseen.

	He eivät halua sitoutua mihinkään mediaan, vaan käyttävät eri medioita silloin kun heidän aikatauluihinsa sopii. Heillä on mahdollisuus hankkia uusinta teknologiaa ja he ovat kiinnostuneita laitteiden kehityksestä. Harrit käyttävät eniten omaan työhönsä tai harrastuksiinsa liittyviä sisältöjä.

	Kehittävät itseään jatkuvasti työhön liittyen ja ovat kiinnostuneita oppimaan uutta.

	Hankkivat ja haluavat saada paljon tietoa harrastuksiinsa tai kiinnostuksen kohteisiinsa liittyen. Syventyvät perinpohjaisesti heitä kiinnostaviin aiheisiin.

	Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

	Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota

	Katsoo uutisotsikoita netistä aina kun mahdollista

	On nykyisin lähes jatkuvasti jonkun median tavoitettavissa

	Kotimaiset uutiset eivät riitä, seuraa myös kansainvälisiä uutispalveluita"

	M2:
	"During the weeks he is going to his work, where he is from 8-16:30 every day. When he comes home after his work he usually cooks some dinner and prepares lunch boxes for the following day. Before he goes to bed he and his wife watches some series on SVT Play (a Swedish video streaming service).

	On the weekends he spends a lot of time watching live sport, especially football premier league. The rest of the weekend he is usually with his wife strolling in the city and/or stopping for coffee at some coffeehouse. At the evenings, if they don’t have friends over, they usually watch television."


## Media use, relation to media use (internet, mobile phone etc)

Describe the persona's use of different digital platforms?

	SMF4:
	"Välittävät mielellään muille eteenpäin löytämiään kiinnostavia artikkeleita ja sisältöjä.

	Oppivat itse ja opettavat lapsilleen uusia asioita usein pelien kautta.

	Pysähtyvät lukemaan ensin kevyempiä uutisia nettiotsikoista, hakeutuvat television viihdesisältöjen pariin ja valikoivat mitä teemoja seuraavat.

	On nykyisin lähes jatkuvasti jonkun median tavoitettavissa.

	Surffailee usein netissä samaan aikaan kuin katsoo televisiota

	Sormeilee vapiana hetkinään laitetta lukiakseen uutisia tai käydäkseen somessa

	On hyvin kiinnostunut käyttämään mediaa uusilta päätelaitteilta

	Maksuttomat eivät riitä, haluaa tilata myös maksullisia sisältöjä

	He käyttävät sujuvasti eri medioita, mutta viihtyvät erityisesti sähköisen median parissa. "

	SMF7:
	"Käyttävät uusinta teknologiaa usein ensimmäisten joukossa.

	Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

	Ovat nykyisin lähes jatkuvasti jonkun median tavoitettavissa.

	Surffailee usein netissä samaan aikaan kuin katsoo televisiota

	Katsoo uutisotsikoita neitäst aina, kun mahdollista

	He katsovat usein itseä kiinnostavia sisältöjä netti-TV:stä omien aikataulujensa mukaisesti."

	M2:
	"At the evenings, if they don’t have friends over, they usually watch television.

	Before he goes to bed he and his wife watches some series on SVT Play (a Swedish video streaming service).

	Like an Apple TV for example, then he usually is having problems in the beginning to understand how the technology works and how to make it work smoothly"

	CD3:
	"Tablet use: Overall, several times per week

	Several times per week: Email, agenda, search info, surfing, social networking, view online media, gaming

	2-3 times a month: online shopping, watch live tv

	Once a month: use second screen apps, watch films, airing an opinion on social media"

	DM2P:
	"Has domesticated iDTV and is a heavy digital TV viewer who is perfectly aware of its affordances. This persona makes full use of the EPG and the possibility to watch TV a la carte. Records a lot of content on the digital recorder and watches it at a convenient time. Is very familiar with the set-top-box and its possibilities and has subscription to specific channels and/or Video on Demand services."

Which sources of video media are used?

	SMF4:
	TV, tablets, smart phones

	SMF7:
	Internet TV, TV, tablets, smart phones

	M2:
	SVT Play (VOD), TV, Apple TV

	DM2P:
	Digital TV, digital recordings, subscription channels, VOD services

	CD3:
	"42% has a YouTube account" (no differences between groups)

How much they are used?

	SMF4:
	"Piat ja Petterit käyttävät sujuvasti eri medioita silloin kun heillä on siihen aikaa, usein töiden lomassa, arjen keskellä tai liikennevälineissä.

	Heillä ei ole useinkaan mahdollisuutta syventyä tarkemmin uutisiin.

	Sormeilee vapaina hetkinään laitetta lukeakseen uutisia tai käydäkseen somessa

	On nykyisin lähes jatkuvasti jonkun media tavoitettavissa"

	SMF7:
	"Viime aikoina jotkut Harreista ovat yrittäneet päästä pois turhasta hektisyydestä ja oravanpyörästä ja ovat hieman ”downshiftanneet”.

	He eivät halua sitoutua mihinkään mediaan, vaan käyttävät eri medioita silloin kun heidän aikatauluihinsa sopii.

	Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita.

	Hankkivat ja haluavat saada paljon tietoa harrastuksiinsa tai kiinnostuksen kohteisiinsa liittyen. Syventyvät perinpohjaisesti heitä kiinnostaviin aiheisiin

	Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

	On nykyisin lähes jatkuvasti jonkun media tavoitettavissa

	Katsoo netistä uutisotsikoita aina kun mahdollista

	On nykyisin lähes jatkuvasti jonkun median tavoitettavissa"

	M2:
	"During the weeks... before he goes to bed he and his wife watch some series on SVT Play

	On the weekends he spends a lot of time watching live sport

	At the evenings if they don't have friends over, they usually watch tv"

	DM2P:
	"Heavy digital TV viewer. Records alot of content on the digital recorder and watches it at a convenient time."

What they are used for?

	SMF4:
	"Digitaalisista kanavista haetaan uutistietoa ja hyötypalveluita. Nopeat vinkit ja perheen hyvinvointiin liittyvä tieto kiinnostavat erityisesti. Kiireisen arjen vastapainoksi musiikki, viihde ja urheilu tarjoavat elämyksiä – TV on heille tärkeä keino rentoutua.

	Musiikki, viihde tai urheilu tarjoavat heille elämyksiä hektisen työn vastapainoksi.

	Haluavat sujuvoittaa omaa arkeaan tekemällä ostokset helposti ja löytämällä nopeasti erilaisia vinkkejä ja ohjeita.

	Haluavat saada omaa perhettään hyödyttävää tietoa ja tarjota lapsilleen mahdollisimman laadukkaita virikkeitä.

	Oppivat itse ja opettavat lapsilleen uusia asioita usein pelien kautta."

	SMF7:
	"Harrit haluavat saada tiedon tärkeistä uutisista ensimmäisten joukossa.

	Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita.

	Kaipaa käyttöönsä entistä enemmän luotettavan tahon kokoamaa tietoa

	Kehittävät itseään jatkuvasti työhön liittyen ja ovat kiinnostuneita oppimaan uutta.

	Hankkivat ja haluavat saada paljon tietoa harrastuksiinsa tai kiinnostuksen kohteisiinsa liittyen. Syventyvät perinpohjaisesti heitä kiinnostaviin aiheisiin.

	Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä."

If different digital platforms are used together? Second screening habits

	SMF4:
	"Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota"

	SMF7:
	"Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota"

	CD3:
	Almost never watches only TV.
	With TV, weekly:
	40%: radio, desktop PC
	80%: laptop, smartphone, tablet
	90%+: newspaper, magazine

What is the personas relation to different medias?

Social context of media use?

	SMF4:
	Puolison kanssa rentoutumiseen.

	Jakavat mielellään löytämiään kiinnostavia artikkeleita ja sisältöjä.

	Lasten kanssa.

	SMF7:
	Ei kerrota ryhmässä katsomisesta. Mediankulutus henkilökohtaista.



## Attitude towards media

Use of remote control & relation to it

## Social media use and relation to social media

## Other relations to media

Motivations
Frustrations
Behaviour
Touch-points

## Influencers that surround the persona and that may influence choices

Partner, children, boss etc

	SMF4:
	Lapset, puoliso

	SMF7:
	Ei muita

	M2:
	Puoliso

## Goals

Short term, long term
Motivations
Work-related goals
Product-related goals
General (life) goals, aspirations
Stated and unstated desires for the product

	SMF4:
	"Haaveissa Pialla ja Petterillä olisikin ehtiä tehdä vielä paljon kiinnostavia asioita.

	En enää myöskään epäröi ostaa palveluita, joilla saan aikaa itselle ja perheelle.

	He elävät lapsiperheen hetkistä elämää ja panostavat erityisesti lapsiin ja arjen kiirettä helpottaviin palveluihin.

	Nopeat vinkit ja perheen hyvinvointiin liittyvä tieto kiinnostavat erityisesti"

	SMF7:
	"Uralla ja työllä on tärkeä rooli heidän elämässään ja he haluavat koko ajan kehittää itseään työhön liittyen. Harrit ovat melko aktiivisia verkostoitujia, mutta ihmissuhteet jäävät usein hieman pinnallisiksi.

	Viime aikoina jotkut Harreista ovat yrittäneet päästä pois turhasta hektisyydestä ja oravanpyörästä ja ovat hieman ”downshiftanneet”.

	Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita."

## Skills and knowledge

General computer and/or internet use
Frequently used products, product knowledge
Special skills
Competitor awareness

	SMF4:
	On kiinnostunut käyttämään mediaa uusilta päätelaitteilta


	SMF7:
	Käyttävät uusinta teknologiaa usein ensimmäisten joukossa.

	Edelläkävijöitä, 5,5/8

	On kiinnostunut käyttämään mediaa uusilta päätelaitteilta

	M2:
	Tech savvy

	In the household he is the one buying and taking care of the technology

	DM2P:
	Fully DTV adapted. Well versed in set-top-box and TV functionality.

## Context/environment

Equipment
"A day in the life" description
	+ Work styles
	+ Time line of a typical day
Specific usage location(s)
General work, household and leisure activities