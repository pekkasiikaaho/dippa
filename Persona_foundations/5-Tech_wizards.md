# Media-trancendent tech gurus

- S3 - Millenial prosumer
- S5 - Passive prosumer
- M3 - Student with interest in tech
- BL2 - Content kings

Tech gurus who are extremely tech savvy, but consume media in quite traditional way. They have abandoned the traditional analog media altogether and consume all of their media through internet. They do not like paying for media content, but consume high amounts of it. They are active users of VOD services. They do not interact with the media at all and are traditional consumers in that sense.