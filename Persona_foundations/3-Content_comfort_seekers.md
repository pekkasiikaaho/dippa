# Content comfort seekers

- DM3N - Internet TV negative
- DM1P - Analogue positive
- SMF3 - Jennat ja Jesset, lapsettomat viihtyjät
- SMF5 - Minnat ja Markot, perhekeskeiset mukavuudenhaluiset
- SMF6 - Tuulat ja Timot, keski-ikäiset tavalliset
- BL1 - Efficiency experts

Media is used for comfort and escapism. They use media to relax after a busy day either at work or with family. They know about newer technology and are slowly adapting it as it becomes easier to use. However, they prefer their set comfortable ways of using the media. They are pretty content at how their life is, but sometimes wish for a bit of excitement.


# Persona template


## Private information

Mary, the Content Comfort seeker
Name: Mary
Age: 51
Gender: Female
Marital status: Married
Numer of children: 2, aged 18 & 19
Lives in suburbs of Helsinki, with husband and kids
Job title: Executive Secretary at a large company
Household disposable income (after taxes): 2700€/month

- Age
- Gender
- Marital status
- Number of children
- Where does the persona live?
- Disabilities
- Role(s) and tasks
	+ Specific company or industry
	+ Job title or role
	+ Salary/disposable income

asdf

	DM1P:
	Age: 60
	Work: Executive secretary
	Lives alone and has no children

	SMF3:

	Keski-ikä: 26
	Keskitulot per capita 1475€/kk
	Keskitulot per talous: 2500€/kk
	Ei lapsia
	Asuu: vanhempien luona (19%), yksin(39%) tai puolison kanssa (34%)

	SMF5:

	Keski-ikä: 41,3
	Keskitulot per capita: 2100
	Keskitulot per talous: 4000
	Asuu: Puoliso ja lapsia

	SMF6:
	Keski-ikä: 49
	Keskitulot per capita: 1970
	Keskitulot per talous: 3200
	Asuu: Puoliso ja lapsia



## Description of work and daily life

+ What does the persona do during an average (work) day in connection to the focus area?
+ Typical activities
+ Important atypical activities
- Describe the persona's use of different digital platforms?
	+ Which sources of video media are used?
	+ How much they are used?
	+ What they are used for?
- What is the personas relation to different medias?

Mary likes that her life is steady and stable. She has a husband, Adam, and two children who are on the verge of moving out to study. The children are quite independent but she still cooks dinner for them almost every day.

At work Mary uses computers as a normal part of her job as a secretary. However, at home she isn't very interested in them. Her husband is more interested in technology and has bought a lot of new devices, but Mary doesn't really know how to use them. She's happy to use EPG to find TV shows and to occasionally check the teletext for news.

Mary doesn't think it is a hobby, but she spends a lot of time knitting and making clothes. She uses her husband's iPad to surf the internet for knitting ideas and instructions. Often she watches the TV at the same time.

Mary and Adam have a couple favourite drama series they watch together every week. On the weekends she has a habit of watching TV while chatting with her friends on the phone.

Other shows she watches by herself for relaxation.

She likes to forget herself to a familiar TV show she's been watching for a long time.

Mary enjoys shows where she can identify with the main characters and lose herself for the duration of the show.

Mary has a favourite TV actress who she follows.

Mary likes shows that make her happy.

As a media user she is moderately active. She watches a few regular shows from television. She uses internet mostly to find new knitting instructions and ideas.

	DM1P:

	does not spend a lot of money on media, it is not important to her

	Uses the internet for her work, rarely in her spare time: 1 hour max in the weekend

	She often uses teletext for games and news

	DM3N:

	The purchases was largerly influenced by others in the immediate environment

	is characterised by a need of assistance by others to use teh technology

	SMF3:

	Vapaa-aika kuluu viihteen ja tietokone- ja konsolipelien parissa.

	Omasta ajasta he nauttivat kaveriporukassa joukkuelajien parissa tai konsolipelejä pelaten.

	Kotona viihtymisen maksimoimiseksi he ovat panostaneet viihde-elektroniikkaan.

	Jennat ja Jesset käyvät baareissa viikonloppuisin tapaamassa tuttuja ja viettämässä aikaa

	Arki rullaa mukavasti ja päivät ovat toistensa kaltaisia. Haaveissa kuitenkin olisi joskus tehdä jotain sykähdyttävämpää.

	Ovat useimmiten jonkin median tavoitettavissa, suuri osa päivästä kuluukin tietokoneella.

	SMF5:

	Minnat ja Markot ovat tyypillisimmin perhekeskeisiä, kouluikäisten lasten vanhempia sekä kuluttajina harkitsevia.

	MInnan ja Markon arki täyttyy lasten kanssa puuhailusta ja kotona oleilusta.

	Omille harrastuksille heillä ei juuri jää aikaa, ennemmin kuljetaan lasten harrastusten mukana.

	Minnat ja Markot elävät perinteistä perhearkea, mutta he eivät oikeastaan haaveilekaan muusta. Näin on hyvä.

	Heidän harrastuksensa liittyvät kotiin ja kotona oleiluun. Kun aikaa on, he viettävät rauhallista koti-iltaa ihan vain oleillen ja nauttien hyvästä ruuasta.

	Mediaryhmistä radioiden kuuntelu korostuu heillä selvästi muita suomalaisia enemmän.

	Haluaa rauhoittaa vapaa-aikansa medialta, sulkee kaikki laitteet

	On usein tunne ettei ehdi tehdä vapaa-ajalla kaikkea haluamaansa

	Maksaa tv:stä, maksuttomat ei riitä, haluaa tilata myös maksullisia sisältöjä.

	Rahoille halutaan saada vastinetta.

	SMF6:

	Tyypillisimmin 45-54-vuotiaat Tuulat ja Timot elävät tasaista arkea. He ovat vaatimattomia, elämä on yksinkertaista eikä sitä ole tarpeen täyttää harrastuksilla. TV:tä lukuunottamatta medioiden käyttö on heillä vähäistä.

	Kuluttajina he ovat säästäväisiä ja harkitsevia. Ostoksilla käyminen on heille välttämättömyys, joka hoidetaan mieluiten rutiinilla tutussa marketissa.

	Tuulat ja Timot viettävät rauhallista ja tasaista elämää. Vaikka heidän arkea leimaakin tietty vaatimattomuus, ovat Tuulat ja Timot melko tyytyväisiä elämäänsä ja tapaansa viettää vapaa-aikaa. Aktiivinen harrastaminen ei heidään elämään kuulu eikä esimerkiksi liikuntaa koeta kovinkaan mielekkääksi ajanvietteeksi.

	Laaja-alainen päivän polttaviin asioihin perehtyminen ei ole heille tyypillistä ja ajan tasalla pysymiseen riittää usein nopea katsaus uutisotsikoihin. He pysähtyvätkin helpoiten kevyempien uutisten äärelle.

	He seuraavat pääasiassa omia suosikkimedioitaan, mutta eivät halua olla jatkuvasti niiden tavoitettavissa.

## Favourite shows

- Domestic drama shows
- Travel shows
- Soap operas
- Domestic comedy shows

asdf

	DM1P:

	Documentaries about travelling and animals

	SMF3:

	Vapaa-aika kuluu viihteen ja tietokone- ja konsolipelien parissa.

	Vapaa-aika on Jennoille ja Jesseille selvästi palkkatyötä tärkeämpää.

	Pitävät erityisesti sisällöistä, jotka saavat hyvälle tuulelle.

	Foreign comedy shows
	Adventure and survivor shows
	Foreign drama series
	Pop & rock music
	Reality TV
	Foreign movies

	SMF5:

	Mediaryhmistä radioiden kuuntelu korostuu heillä selvästi muita suomalaisia enemmän.

	News
	Domestic comedy shows, comedy sketches
	Domestic drama shows
	Police, action and thriller shows

	SMF6:

	Police, action and thriller shows
	Domestic comedy shows, comedy sketches
	Slager and music shows
	Domestic movies
	Domestic drama shows
	News

	He pysähtyvätkin helpoiten kevyempien uutisten äärelle.


## Media use goals

- Relaxation & comfort
- Escapism with long time favourite shows
- Taking the thoughts away from work
- Spending time with husband

a



	BL1:

	These consumers see the adoption of digital devices and services as a way to make life easier

	DM3N:

	is still watching TV in more traditional way

	DM1P:

	a heavy TV viewer who follow sthe linear broadcast scheme and watches her favourite shows on a daily basis

	TV is a social companion and part of everyday life

	SMF3:

	Nautinnonhalu ja viihtyminen korostuvat heidän elämäntyylissään.

	He ovat kiinnostuneita erityisesti teknolo­giasta ja uuden teknologian luomista mahdollisuuksia.

	Kodilla on tärkeä rooli heidän elämässään. He ovatkin valmiita käyttämään rahaa siellä viihtymiseen.

	Omasta ajasta he nauttivat kaveriporukassa joukkuelajien parissa tai konsolipelejä pelaten.

	Kotona viihtymisen maksimoimiseksi he ovat panostaneet viihde-elektroniikkaan.

	Median parissa viihtyminen on Jennoille ja Jesseille tärkeintä.

	Hakevat tietoa omista harrastuksista ja kiinnostuksen kohteista. Erityisesti teknologia ja uudet laitteet kiinnostavat.

	Pitävät erityisesti sisällöistä, jotka saavat hyvälle tuulelle.

	SMF5:

	Elämältään he toivovat turvallista perusarkea.

	Minnat ja Markot elävät perinteistä perhearkea, mutta he eivät oikeastaan haaveilekaan muusta. Näin on hyvä.

	Minnat ja Markot asettavat mediakäytössäkin perheen tarpeet ja toiveet omiensa edelle. He katsovat usein muiden perheenjäsenten valitsemia TV-ohjelmia, mutta pitävät kiinni myös omista suosikkisarjoistaan.

	Kiireen keskellä pysähtyvät ensimmäiseksi viihteellisempien ja kevyempien uutisaiheiden ääreen.

	Mediaryhmistä radioiden kuuntelu korostuu heillä selvästi muita suomalaisia enemmän.

	Rahoille halutaan saada vastinetta, arkea he pyrkivät helpottamaan hyötytiedon avulla.

	Lapsille halutaan tarjota laadukkaita virikkeitä.

	Oman hetken koittaessa uppoudutaan katsomaan omaa suosikkisarjaansa TV:stä tai muuhun itseä kiinnostavaan sisältöön, esimerkiksi pelaamaan tietokonepelejä.

	Median parissa laiskotellaan hyvällä omalla tunnolla.

	SMF6:

	TV:tä lukuunottamatta medioiden käyttö on heillä vähäistä.

	Haluavat viihtyä ja rentoutua medioiden parissa

	Tuulat ja Timot haluavat ennen kaikkea viihtyä medioiden parissa ja kokevat, että televisiota tulee välillä katsottua liikaakin.

	Laaja-alainen päivän polttaviin asioihin perehtyminen ei ole heille tyypillistä ja ajan tasalla pysymiseen riittää usein nopea katsaus uutisotsikoihin. He pysähtyvätkin helpoiten kevyempien uutisten äärelle.

	He seuraavat pääasiassa omia suosikkimedioitaan, mutta eivät halua olla jatkuvasti niiden tavoitettavissa.

	Seuraavat sisällöt omista suosikkimedioistaan, eivätkä ole niin tarkkoja siitä, mitä tulee milloinkin seurattua.

## Pain points

- Lacks confidence when dealing with unfamiliar devices or services
- Would like to use less internet on her free time.

Frustrations

	DM1P:

	However the [iDTV] adoption decision is being postponed because of the associated costs and because of the assumed lack of skills to use the technology

	DM3N:

	is characterised by a need of assistance by others to use the technology

	SMF3:

	Arki rullaa mukavasti ja päivät ovat toistensa kaltaisia. Haaveissa kuitenkin olisi joskus tehdä jotain sykähdyttävämpää.

	Oman hetken koittaessa uppoudutaan katsomaan omaa suosikkisarjaansa TV:stä tai muuhun itseä kiinnostavaan sisältöön, esimerkiksi pelaamaan tietokonepelejä.

	Haluaa rauhoittaa vapaa-aikansa medialta, sulkee kaikki laitteet

	On aiempaa yleisempää, että lukee/katsoo artikkelista tai ohjelmasta vain pätkän

	SMF6:

	Tuulat ja Timot haluavat ennen kaikkea viihtyä medioiden parissa ja kokevat, että televisiota tulee välillä katsottua liikaakin.

	He seuraavat pääasiassa omia suosikkimedioitaan, mutta eivät halua olla jatkuvasti niiden tavoitettavissa.

	Eivät halua olla jatkuvasti medioiden tavoitettavissa.

## Second screen use

- If different digital platforms are used together? Second screening habits

Most of the time she only watches television, with little second screen use. Sometimes she browses the news on her husbands iPad or talks to her friends on the phone.

	BL1:

	Accessing video on demand on their home TVs

	Accessing news, magazines and video content online, *primarily via their PCs*

	Accessing the internet via mobile phone to browse the web or use *mobile navigation services*

	SMF3:

	Ovat yhteydessä kavereihin samalla kun esimerkiksi katsovat televisiota. Jakavat satunnaisesti mieluisia hetkiä toisten kanssa eri palveluiden avulla.

	Erilaiset tietokone- ja konsolipelit ovat kiinteä osa arkea.

	Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota


## Social context of TV media use

Watching television is an important way for Mary and Adam to spend time together. They have their favourite shows that they watch every week. Mary also talks with her friends over the phone while watching television. Quite often Mary also watches television to pass the time while knitting or relaxing on the weekends.



	DM1P:

	a heavy TV viewer who follow sthe linear broadcast scheme and watches her favourite shows on a daily basis

	TV is a social companion and part of everyday life

	watches TV alone or with other relatives

	DM3N:

	is characterised by a need of assistance by others to use teh technology

	SMF3:

	Omasta ajasta he nauttivat kaveriporukassa joukkuelajien parissa tai konsolipelejä pelaten.

	Ovat yhteydessä kavereihin samalla kun esimerkiksi katsovat televisiota. Jakavat satunnaisesti mieluisia hetkiä toisten kanssa eri palveluiden avulla.

	SMF5:

	Perheen tarpeet ohjaavat mediavalintoja ja maksullisia sisältöjä käytetään viihtymismielessä monipuolisesti.

	Kun aikaa on, he viettävät rauhallista koti-iltaa ihan vain oleillen ja nauttien hyvästä ruuasta.

	Internetissä tai sosiaalisessa mediassa he eivät juuri vietä ylimääräistä aikaa, eivätkä koe niiden tuoman yhteisöllisyyden antavan lisäarvoa.

	Minnat ja Markot asettavat mediakäytössäkin perheen tarpeet ja toiveet omiensa edelle. He katsovat usein muiden perheenjäsenten valitsemia TV-ohjelmia, mutta pitävät kiinni myös omista suosikkisarjoistaan.

	Lapsille halutaan tarjota laadukkaita virikkeitä.

	Oman hetken koittaessa uppoudutaan katsomaan omaa suosikkisarjaansa TV:stä tai muuhun itseä kiinnostavaan sisältöön, esimerkiksi pelaamaan tietokonepelejä.

	SMF6:

	Sosiaaliset verkostot eivät ole merkittävässä roolissa. He viettävät aikaa mieluiten kahden kesken.

## Skills and knowledge

- General computer and/or internet use
- Frequently used products, product knowledge
- Special skills

- Has a Netflix account that she uses to watch one show that isn't on the TV. Her children installed it for her
- Uses both teletext and her husbands iPad to check the news
- Sees the new technology as something potentially useful, as something she will get around to some day when she has the time to learn how to use it

asfd

	BL1:

	Even aging traditionalists fall into this category as they adopt bheaviours..

	DM1P:

	PP1 is familiar with and hs domesticated the features and affordances of traditional analogue TV (e.g. Teletext, subtitling...)

	This persona does not feel threatened by new technolog at all, and is interested in it, but considers herself as a non-expert

	This persona is aware of most of the affordances of functionalities of iDTV and is considering to adopt digital TV. However the adoption decision is being postponed because of the associated costs and because of the assumed lack of skills to use the technology

	She often uses teletext for games and news

	DM3N:

	Has a TV set that is connected over the Internet, but needs time to gradually get used to the new functionalities

	is characterised by a need of assistance by others to use the technology

	SMF3:

	Edelläkävijyys 3,5/8, varhainen enemmistö

	SMF5:

	Edelläkävijyys: 2,5/8, myöhäinen enemmistö

	Mediakäytön muutoksessa he ovat myöhäistä enemmistöä, ja parhaillaan lähdössä muuttamaan mediakäyttöään kohti uusia käyttötapoja ja päätelaitteita.

	Internetissä tai sosiaalisessa mediassa he eivät juuri vietä ylimääräistä aikaa, eivätkä koe niiden tuoman yhteisöllisyyden antavan lisäarvoa.

	SMF6:

	Edelläkävijyys: 0,5/8, vitkastelijat

	TV:tä lukuunottamatta medioiden käyttö on heillä vähäistä.


