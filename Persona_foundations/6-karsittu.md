# Digitally adapted middle-agers

- DM2P - Digital positive
- SMF7 - Harrit, urakeskeiset tiedolla johtavat miehet
- SMF4 - Piat ja Petterit, nopeatahtiset perheelliset
- CD3 - TV & Multimedia
- M2 - Family man who takes care of IT at home

They adopted internet at a later age, but they have adapted it fully. Their history of traditional media usage is clearly seen. They watch their content through television, but use digital television recordings and VOD services to set the times they watch the content. Their life is very busy because of family or career. They like to use media for learning or other useful resources.

# Persona template

## Private information

Danny, the Career Driven Family Man
Name: Danny
Age: 44
Gender: Male
Marital status: Married
Number of children: 2, aged 8 & 11
Lives in an owned apartment near the center of Turku.
Job title: Project manager at a mid-size IT company
Household disposable income (after taxes): 3800€ / month


## Description of work and daily life

Danny is kept busy by his family and work. He is married to Nicole and they have two daughters. Danny's time is mainly spent working and with his daughters’ hobbies. The occasional overtime he has to do doesn't bother him too much, because his work challenges him and it feels impactful.

Even though Danny enjoys his busy life, he often feels like he doesn't have enough free time. He would like to dedicate more time to learning and watching documentaries and current issue shows on television.

Danny would like to stay current with world events, but he doesn’t have the time to delve deeply into news and background stories. Whenever he has a spare minute or two, he's on his smartphone reading the news. He's gotten used to reading only a small part of an article at a time and returning to it later later to finish it. His TV viewing is also often disrupted by his daughters and their needs.

In the evening Danny watches quality shows with Nicole. Depending on where the shows are available, they have either recorded the show on their digital recorder or they watch it from paid VOD services. Sometimes on weekends he gets to watch a football match with his friends.

## Favourite shows

- News
- Documentaries
- Current issues
- Foreign drama series
- Domestic movies with his children
- Football

## Media use goals

- Relaxation
- Family time
- Staying current with world news
- Learning

## Pain points

- Records and finds more content than he has time to watch
- His watching is often interrupted

## Second screen use

Often has a second screen with him when he watches TV. It's most usually a magazine or a tablet. Most often the second screen is used to check email, search for more info on what's on TV, social networking or just surfing the internet. Sometimes Danny does online shopping when watching television.

## Devices used to watch TV content

Most often TV screen. Also has Apple TV. Laptops and tablets are used couple of times a month. Danny prefers the better quality and larger size of TV screen.

## Social context of TV use

Mostly Danny watches TV with his wife, to help them relax in the evening. During weekends he might watch a movie with one of his daughters for some quality time. He and Nicole are very particular what the daughters see: they don't let them watch anything violent and try to pick content with good role models for the kids.

When he can, he tries to watch documentaries that interest him, but he rarely gets to watch them for long before he's interrupted by chores or schedules. Sometimes, although rarely, he watches football with his friends on a weekend evening.

## Skills and knowledge

- Early adopter of new technologies, enjoys trying new gadgets and media devices
- Is responsible for the household IT and devices
- Can install most new devices at home, but with specific problems has to rely on customer service for help
- Power user of digital TV, EPG and recorder, uses multiple VOD services such as Netflix and HBO




# Data


	### Kiire

	elävät todellisia ruuhkavuosia ja kokevat aikapaineita sekä työssä että vapaalla. Hektisen arjen keskellä vapaa-aika kuluu pääosin lasten kanssa puuhaillessa ja heidän harrastuksiaan seuraillen.

	On usein tunne, ettei tehdi vapaa-ajalla kaikkea haluamaansa

	### Ura

	myös osan vapaa-ajastaan iltaisin ja viikonloppuisin töiden tekemiseen.

	he haluavat koko ajan kehittää itseään työhön liittyen. He katsovat usein itseä kiinnostavia sisältöjä netti-TV:stä omien aikataulujensa mukaisesti


	### Hajakäyttö

	käyttävät sujuvasti eri medioita silloin kun heillä on siihen aikaa, usein töiden lomassa, arjen keskellä tai liikennevälineissä.

	Katsoo uutisotsikoita netistä aina kun mahdollista

	On aiempaa yleisempää, että lukee/katsoo artikkelista/ohjelmasta vain pätkän


	### Rentoutuminen

	Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

	Musiikki, viihde tai urheilu tarjoavat heille elämyksiä hektisen työn vastapainoksi.

	Välittävät mielellään muille eteenpäin löytämiään kiinnostavia artikkeleita ja sisältöjä.

	Before he goes to bed he and his wife watches some series on SVT Play (a Swedish video streaming service).


## Media use, relation to media use (internet, mobile phone etc)

## Describe the persona's use of different digital platforms?

Välittävät mielellään muille eteenpäin löytämiään kiinnostavia artikkeleita ja sisältöjä.

On hyvin kiinnostunut käyttämään mediaa uusilta päätelaitteilta

#### Aina saatavilla

On nykyisin lähes jatkuvasti jonkun median tavoitettavissa.

Sormeilee vapaina hetkinään laitetta lukiakseen uutisia tai käydäkseen somessa

Katsoo uutisotsikoita neitäst aina, kun mahdollista

#### Valmiita maksamaan sisällöistä

Maksuttomat eivät riitä, haluaa tilata myös maksullisia sisältöjä.  has subscription to specific channels and/or Video on Demand services.

He käyttävät sujuvasti eri medioita, mutta viihtyvät erityisesti sähköisen median parissa.

#### Rentoutuu katsomalla itselle mielenkiintoisia sisältöjä

Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä. He katsovat usein itseä kiinnostavia sisältöjä netti-TV:stä omien aikataulujensa mukaisesti. Records a lot of content on the digital recorder and watches it at a convenient time.

#### Second screen on hyvin aktiivinen

Surffailee usein netissä samaan aikaan kuin katsoo televisiota

Tablet use: Overall, several times per week

Several times per week: Email, agenda, search info, surfing, social networking, view online media, gaming

2-3 times a month: online shopping, watch live tv

Once a month: use second screen apps, watch films, airing an opinion on social media


### Which sources of video media are used?

DTV, Yle Areena, TV+cable, Apple TV


### How much they are used?

#### Nauhoittaa enemmän kuin kerkee katsomaan

käyttävät sujuvasti eri medioita silloin kun heillä on siihen aikaa, usein töiden lomassa, arjen keskellä tai liikennevälineissä. He eivät halua sitoutua mihinkään mediaan, käyttävät eri medioita silloin kun heidän aikatauluihinsa sopii. Records alot of content on the digital recorder and watches it at a convenient time. On usein tunne, ettei ehdi tehdä vapaa-ajalla kaikkea haluamaansa.

#### Haluaisivat syventyä uutisiin enemmän kuin voisivat

Sormeilee vapaina hetkinään laitetta lukeakseen uutisia tai käydäkseen somessa. Katsoo netistä uutisotsikoita aina kun mahdollista

Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita.

Heillä ei ole useinkaan mahdollisuutta syventyä tarkemmin uutisiin.

On nykyisin lähes jatkuvasti jonkun media tavoitettavissa

#### Liiallinen kiire vähän häiritsee

"Viime aikoina jotkut Harreista ovat yrittäneet päästä pois turhasta hektisyydestä ja oravanpyörästä ja ovat hieman ”downshiftanneet”.


### What they are used for?

#### Rentoutuminen

Digitaalisista kanavista haetaan uutistietoa ja hyötypalveluita. Nopeat vinkit ja perheen hyvinvointiin liittyvä tieto kiinnostavat erityisesti. Kiireisen arjen vastapainoksi musiikki, viihde ja urheilu tarjoavat elämyksiä – TV on heille tärkeä keino rentoutua. Musiikki, viihde tai urheilu tarjoavat heille elämyksiä hektisen työn vastapainoksi. Rentoutuvat vapaa-ajalla viihteen avulla katsomalla sisältöjä sopivan hetken koittaessa, useimmiten jo netti-TV:stä.

#### Arjen helpotusta

Haluavat sujuvoittaa omaa arkeaan tekemällä ostokset helposti ja löytämällä nopeasti erilaisia vinkkejä ja ohjeita.

"En enää myöskään epäröi ostaa palveluita, joilla saan aikaa itselle ja perheelle."

#### Lasten virikkeitä ja opimista

Haluavat saada omaa perhettään hyödyttävää tietoa ja tarjota lapsilleen mahdollisimman laadukkaita virikkeitä.

Oppivat itse ja opettavat lapsilleen uusia asioita usein pelien kautta.

#### Ajan tasalla pysymistä

Harrit haluavat saada tiedon tärkeistä uutisista ensimmäisten joukossa.

Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita.

Kaipaa käyttöönsä entistä enemmän luotettavan tahon kokoamaa tietoa

#### Ammatillista kehitystä

Kehittävät itseään jatkuvasti työhön liittyen ja ovat kiinnostuneita oppimaan uutta. Hankkivat ja haluavat saada paljon tietoa harrastuksiinsa tai kiinnostuksen kohteisiinsa liittyen.


### If different digital platforms are used together? Second screening habits

"Surffailee hyvin usein netissä samaan aikaan kun katsoo televisiota"

Almost never watches only TV.

With TV, weekly:
40%: radio, desktop PC
80%: laptop, smartphone, tablet
90%+: newspaper, magazine

#### What is the personas relation to different medias?

Social context of media use?

Puolison kanssa rentoutumiseen.

Jakavat mielellään löytämiään kiinnostavia artikkeleita ja sisältöjä.

Lasten kanssa.

Toisaalta myös henkilökohtaista oppimista


## Attitude towards media


## Social media use and relation to social media

## Other relations to media

Motivations
Frustrations
Behaviour
Touch-points

## Influencers that surround the persona and that may influence choices


## Goals

### Short term:

En enää myöskään epäröi ostaa palveluita, joilla saan aikaa itselle ja perheelle.

He elävät lapsiperheen hetkistä elämää ja panostavat erityisesti lapsiin ja arjen kiirettä helpottaviin palveluihin.

Nopeat vinkit ja perheen hyvinvointiin liittyvä tieto kiinnostavat erityisesti

### Long term:

Haaveissa Pialla ja Petterillä olisikin ehtiä tehdä vielä paljon kiinnostavia asioita.

Uralla ja työllä on tärkeä rooli heidän elämässään ja he haluavat koko ajan kehittää itseään työhön liittyen.

Viime aikoina jotkut Harreista ovat yrittäneet päästä pois turhasta hektisyydestä ja oravanpyörästä ja ovat hieman ”downshiftanneet”.

Haluavat ymmärtää miten asiat ovat ja tarkistavat uutisten alkuperäiset lähteet. Ovat tietoisia uutisista usein ennen muita."



## Skills and knowledge

General computer and/or internet use
Frequently used products, product knowledge
Special skills
Competitor awareness

	SMF4:
	On kiinnostunut käyttämään mediaa uusilta päätelaitteilta


	SMF7:
	Käyttävät uusinta teknologiaa usein ensimmäisten joukossa.

	Edelläkävijöitä, 5,5/8

	On kiinnostunut käyttämään mediaa uusilta päätelaitteilta

	M2:
	Tech savvy

	In the household he is the one buying and taking care of the technology

	Like an Apple TV for example, then he usually is having problems in the beginning to understand how the technology works and how to make it work smoothly"

	DM2P:
	Fully DTV adapted. Well versed in set-top-box and TV functionality.

## Context/environment

Equipment
"A day in the life" description
	+ Work styles
	+ Time line of a typical day
Specific usage location(s)
General work, household and leisure activities