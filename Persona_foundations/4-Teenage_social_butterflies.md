# Teenage social butterflies

- SMF1 - Neat, kokeilunhaluiset heimoutujat
- CD2 - TV & Screen media
- S4 - Teenage prosumer
- BL3 - Social butterflies

Teenagers who use media constantly and without stop. Interacting through media is a normal part of their life and they spend their days in full interaction with their friends. They consume moderate amount of media, focusing on social media. They understand how media works on an instinctual level, without technical knowledge of how it actually works.


## Private information

Susie, the Teenge Social Butterfly
Name: Susie
Age: 19
Gender: Female
Marital status: Single
Numer of children: 0
Lives alone in an rental apartment in the suburbs of Tampere
Job title: Cashier at a cafeteria
Household disposable income (after taxes): 1400€/month


	SMF1:

	98% female
	Average age 21

	Household average income
		Total 2400
		Per capita 1400

	Living status
		With parents: 22%
		Alone: 33%
		Partner: 35%
		Avg. size: 1,7

		=> Single, living alone, outside metropolitan area

	Education:
		Yo / Ylempi keskiaste

	No children

	Job title: ???

	Palkkatiedot & ammatti:
	http://pxnet2.stat.fi/PXWeb/pxweb/fi/StatFin/StatFin__pal__yskp__2014/020_yskp_tau_102.px/table/tableViewLayout1/?rxid=54b2db50-ee10-453e-a27a-ed9521b643d8




## Description of work and daily life

Susie's life is spent connected to her friends via media. Even at work, whenever there's a boring moment, she digs out her smartphone and checks her instant messaging app for messages. On her 15 minute break, she watches a vlog (video blog) entry uploaded by her friend on Youtube.

When Susie gets home from her work, she watches a weekly fashion makeover show to kill time. When she sees an interesting tip, she shares it to group of her friends on their WhatsApp group.

When Susie has guests over, the TV is on in the background, showing whatever's on at the moment. At the same time Susie keeps checking social media for likes or comments on her photoes. Susie also keeps sending messages to her WhatsApp groups as she chats with her guests.

If Susie notices an advertisement for a sale, she checks the online store for good deals. Sometimes she buys something, especially clothes, even though she knows she can't really afford it. "The deal is too good to pass!" she thinks.

	SMF1:

	He tapaavat usein ystäviään ja yhteisöllisyys on heille tärkeää. Neat ovat kiinnostuneita ulkonäköön liittyvistä asioista ja viimeisimmistä trendeistä.

	Neat käyttävät mediaa aina tylsän hetken tullen ja viestittele­vät ahkerasti.

	He harrastavat lisäksi liikuntaa, juhlivat, nauttivat elämästä ja haaveilevat uusista elämyksistä. Netti ja eri yhteisöt ovat isossa roolissa heidän elämässään.

	Neat käyttävät mielellään aikaa ja rahaa hemmotteluun, shoppailuun sekä hyvältä näyttämiseen. Mikäli lompakko antaisi myöten, toteuttaisivat he itseään laajemmaltikin eri harrastusten ja ajanvietteiden parissa.

	Nettisurffailu, TV:n ja elokuvien katselu, keverit, ulkona käyminen/bilettäminen, shoppailu, ryhmäliikuntatunnit, itsensä hemmottelu

	"Minusta on mukavaa seurailla trendejä ja kokeilla uusia markkinoille tulevia tuotteita ja merkkejä. Alennusmyynnit antavat minulle mahdollisuuden ostaa luksustuotteita, joita en muuten ostaisi."

	Median avulla he pääsevät hetkeksi irti arjestaan silloin kun sitä haluavat. Rentoutuvat, viihtyvät ja haluavat kokea elämyksiä median parissa. Toivovat sisältöjen koskettavan ja herättävän tunteita.

	Ajautuvat puolivahingossa sisältöjen pariin ja käyttävät mediaa aina tylsän hetken tullen. Viettävät sisältöjen parissa päivän aikana useita pikkuhetkiä esimerkiksi viestittelemällä kavereiden kanssa pikaviestipalveluiden tai sosiaalisen median kautta.

	Haluavat näkyä ja tulla huomatuksi, yhteisöllisyys erityisen tärkeää. Neat jakavat erityisesti ulkonäköön liittyviä sisältöjä    ja he haluavat myös saada arvioita toisilta. He haluavat näyttää sosiaalisessa mediassa paremmalta kuin ehkä ovatkaan todellisuudessa.

	Etsivät hyviä ostosvinkkejä ja tarjouksia.

	Viettävät vapaa-aikaansa pelaamalla ja arjen onnistumisia saadaan esimerkiksi pääsemällä pelissä seuraavalle tasolle. Haluavat saada ja välittää eteenpäin positiivisuutta.

	On useint unne siitä, että ei ehdi tehdä vapaa-ajalla kaikkea haluamaansa

	Tietää mitä haluaa ja etsii usein sisällöti itse

	Yhä suurempi tarve viihtyä median parissa

	Aivot narikkaan - Media tarjoaa parhaan tavan irroittautua hetkeksi arjesta

	Ostaa uusia tuotteita pelkästään kokeilunhalusta



	S4:

	Medium to low interaction with media, high use of social media

	Low consumption of internet

	No will to pay for digital content


	BL3:

	cannot imagine not being able to instantly access any of their friends, regardless of time or place

	often view videos from others on Youtube, for example

	They use mobile phones primarily to call and IM others, maintain/update a profile on a social networking site like facebook regurarly, visit social networking sites religiously, frequently add labels or tags to online photos and web pages and often view videos from other users on youtube for example

## Favourite shows

- Reality shows
- Health, fitness and wellbeing shows
- Soap operas
- Home decoration shows
- Foreign movies, drama series and comedy shows

asdf

	SMF1:

	Kevyemmät sisällöt riittävät heille, usein he vilkaisevatkin uutisista vain otsikot.

	Reality shows
	Health, fitness and wellbeing
	Daily drama shows, "soap operas"
	Home decoration shows
	Foreign movies, drama series and comedy shows


## Media use goals

- Inspiration and ideas for their life
- Escape from boredom
- Entertainment with strong emotional experiences
- Sharing experiences with social media community

asdf

	SMF1:

	Haluavat näkyä ja tulla huomatuiksi

	Median avulla Neat haluavat paeta arjesta ja saada elämyksiä.

	Rentoutuvat, viihtyvät ja haluavat kokea elämyksiä median parissa. Toivovat sisältöjen koskettavan ja herättävän tunteita.

	Ajautuvat puolivahingossa sisältöjen pariin ja käyttävät mediaa aina tylsän hetken tullen. Viettävät sisältöjen parissa päivän aikana useita pikkuhetkiä esimerkiksi viestittelemällä kavereiden kanssa pikaviestipalveluiden tai sosiaalisen median kautta.

	Neat ovat uteliaita mediakäyttäjiä ja haluavat saada mediasta innostusta ja inspiraatiota elämäänsä. Median avulla he pääsevät hetkeksi irti arjestaan silloin kun sitä haluavat.

	Kavereiden vinkeillä ja suosituksilla on Neoille suuri merkitys

	Haluavat näkyä ja tulla huomatuksi, yhteisöllisyys erityisen tärkeää. Neat jakavat erityisesti ulkonäköön liittyviä sisältöjä    ja he haluavat myös saada arvioita toisilta. He haluavat näyttää sosiaalisessa mediassa paremmalta kuin ehkä ovatkaan todellisuudessa.

	Etsivät hyviä ostosvinkkejä ja tarjouksia. Visuaalisuudella ja sisältöjen ulkonäöllä on merkitystä.

	Viettävät vapaa-aikaansa pelaamalla ja arjen onnistumisia saadaan esimerkiksi pääsemällä pelissä seuraavalle tasolle. Haluavat saada ja välittää eteenpäin positiivisuutta.

	S4:

	Medium to low interaction with media, high use of social media

	Low consumption of internet

	No will to pay for digital content



	BL3:

	They use mobile phones primarily to call and IM others, maintain/update a profile on a social networking site like facebook regurarly, visit social networking sites religiously, frequently add labels or tags to online photos and web pages and often view videos from other users on youtube for example

## Pain points

- The need to match expectations set by friends and media, to the extent of embellishing their life to look better than it is on social media
- Not enough money to live up to their lifestyle goals


asdf

	SMF1:

	Mikäli lompakko antaisi myöten, toteuttaisivat he itseään laajemmaltikin eri harrastusten ja ajanvietteiden parissa.

	On usein tunne, että ei ehdi tehdä vapaa-ajalla kaikkea haluamaansa

	He haluavat näyttää sosiaalisessa mediassa paremmalta kuin ehkä ovatkaan todellisuudessa.

	S3:

	Does not want to pay for digital content

## Second screen use

Susie almost never just watches television, but always has a tablet, laptop or smartphone with her. Most often the second screen is used to chat with friends or groups of friends. Sometimes she watches YouTube videos made her peers about the show she's watching currently. At times the second screen is used for gaming or surfing the web and is the main focus instead of the TV.

	SMF1:

	On nykyisin lähes jatkuvasti jonkun median tavoitettavissa

	Surffailee hyvin usein netissä samaan aikaan kun katsoo televisioita

	Sormeilee vpaiana hetkinä laitetta lukeakseen uutisia tai käydäkseen somessa

	Kavereiden vinkeillä ja suosituksilla on Neoille suuri merkitys.

	Netti ja eri yhteisöt ovat isossa roolissa heidän elämässään.

	CD2:

	At least weekly likelihood of second screening w/:
		Tablet 0,7
		Laptop 0,5
		Smartphone 0,4
		Desktop, magazine, newspaper 0,2
		Radio 0,5
		Only TV 0

	Tablet uses
		Often: Email, surfing, social media, view online video
		Sometimes: Gaming, agenda
		Rarely: Live TV, films, online shopping
		Never: Second screen apps

	BL3:

	They use mobile phones primarily to call and IM others, maintain/update a profile on a social networking site like facebook regurarly, visit social networking sites religiously, frequently add labels or tags to online photos and web pages and often view videos from other users on youtube for example



## Devices used to watch TV content

Susie watches internet tv mainly on her tablet, but also on her laptop. She also has a television that she uses, but other screens have started to replace it. She watches YouTube videos from her friends and channels she follows on her smartphone or tablet.

	SMF1:

	Perinteisen TV:n ohella sosiaalisella medialla ja netti-TV:llä on jo vahva rooli Neojen mediakäytössä.

	CD2:

	At least weekly likelihood of second screening w/:
		Tablet 0,7
		Laptop 0,5
		Smartphone 0,4

	BL3:

	They use mobile phones primarily to ... view videos from other users on youtube for example


## Social context of TV use

When she gets bored at home she often opens either a VOD service or TV to keep herself entertained. She also follows some shows just to keep up to date with gossip about them.

	SMF1:

	Neat käyttävät mediaa aina tylsän hetken tullen ja viestittele­vät ahkerasti.

	Kavereiden vinkeillä ja suosituksilla on Neoille suuri merkitys

## Skills and knowledge

- Native user of social media and internet services, but does not understand how they work. She “just uses them”
- She adopts new technology mostly based on recommendations from her friends, as a part of the early majority
- If something doesn’t work as she expects, she doesn’t know what to do

asdf

	SMF1:

	Perinteisen TV:n ohella sosiaalisella medialla ja netti-TV:llä on jo vahva rooli Neojen mediakäytössä.

	Edelläkävijyys: 3,5 (1-8, 1 innovaattori), early majority

	Kavereiden vinkeillä ja suosituksilla on Neoille suuri merkitys.

	CD2:

	(Tablet use is common)

	S4:

	Digital compentences and skills: 16,7%, very low

	BL3:

	They use mobile phones primarily to call and IM others, maintain/update a profile on a social networking site like facebook regurarly, visit social networking sites religiously, frequently add labels or tags to online photos and web pages and often view videos from other users on youtube for example

