# Habitual and traditional TV media consumers

- DM2N - Digital negative
- SMF10 - Eevat ja Erkit, uutisjanoiset eläkeläiset
- SMF9 - Marjatat, aktiiviset eläkeläisnaiset
- CD1 - Only TV
- M1 - The social worker who doesn't like tech problems

Media use is part of their daily routine. They start and finish their day with the news. They aren't interested in new technology, but rely on the traditional broadcast TV, watched live, and the newspapers. They are interested in trustworthy media content and rely on it to get their information on the world. They do not interact with media, but are consumers in the strictest meaning of the word.

Name: Elsa


## Private information

- Name Elsa
- Age
- Gender Female:
- Marital status
- Number of children
- Where does the persona live?
- Disabilities
- Role(s) and tasks
	+ Specific company or industry
	+ Job title or role
	+ Salary/disposable income

asdfs

	SMF10:
	Ikä: 67
	Elää taajamassa
	Aviomies
	Keskitulot 3,400
	Keskitulot per capita: 1900
	Eläkeläinen

	SMF9:
	Ikä: 74
	Elää taajamassa tai maaseudulla
	Aviomies
	Keskitulot 2960
	Keskitulot per capita 1700
	Eläkeläinen

	M1:
	Occupation: Social worker.

## Description of work and daily life

+ What does the persona do during an average (work) day in connection to the focus area?
+ Typical activities
+ Important atypical activities
- Describe the persona's use of different digital platforms?
	+ Which sources of video media are used?
	+ How much they are used?
	+ What they are used for?
- What is the personas relation to different medias?

Elsa starts her day with the morning paper and ends it with evening news on TV. The news are an important part of the day, and she follows them throughout the day. On the other hand, she doesn't want to be connected to media all of the time. She uses media through traditional channels: TV and newspapers.

	Heidän päivänsä käynnistyy sanomalehden lukemisella ja loppuu iltauutisiin. Uutiset ovat tärkeä osa päivää ja niitä seurataan ensimmäisenä aamulla ja viimeisenä illalla. He eivät halua olla kuitenkaan koko ajan medioiden äärellä. Uutisia ja ajankohtaisia lähiympäristön asioita he seuraavat lähinnä perinteisistä kanavista, televisiosta ja sanomalehdistä.

Elsa's live is most typically home centered pensioner life. Her and her husband's life is filled with chores, crafts, being in nature and meeting with friends and family. Occasional walks with her husband Carl are her way of keeping care of their health.

	Tyypillisimmin viettävät kotikeskeistä eläkeläiselämää. Heidän päivänsä täyttyvät kotiaskareiden ja käsitöiden parissa puuhailusta, luonnossa liikkumisesta ja ystävien tai sukulaisten tapaamisesta. Silloin tällöin tehtävät sauvakävely- ja hiihtolenkit ovat heidän tapojaan pitää terveyttä yllä. The two of them likes to do a lot of activities like stroll in the city, take the car to visit some of their friend.

Elsa's media use is directed by her habits. She chooses her viewing schedule before hand and watches every episode of her favourite shows. Elsa and Carl have one domestic drama show they always watch together. Other than that Elsa watches her favourite shows alone.

	Mediankäyttöä ohjaa tavat, ei muuta paljon käyttäytymistä. vuorokautta rytmittävät tutut mediat ja sisällöt. He valitsevat katsomansa televisio-ohjelmat etukäteen, eivätkä mielellään jätä lempisarjojaan katsomatta. On the evenings before going to bed they usually watch regular TV. If he is home alone he usually watch something randomly on a VoD services.

Elsa is particular about what she sees on media. She wants to select interesting topics and shows and watch them at her pace. She's not interested in internet TV. She often is not reachable by any media.

	eivät kaipaa jatkuvaa mediatulvaa ympärilleen. He haluavat valikoida kiinnostavimmat aiheet ja perehtyä niihin rauhassa. Is not interested in TV connected through the internet. Haluaa rauhoittaa vapaa-ajan medialta, välillä sulkee kaikki laitteet





	SMF10:
	He pyrkivät välttämään jatkuvaa mediatulvaa, vaikka ovatkin kiinnostuneita ajankohtaisista asioista. Uutisia ja ajankohtaisia lähiympäristön asioita he seuraavat lähinnä perinteisistä kanavista, televisiosta ja sanomalehdistä.

	Tyypillisimmin Eevat ja Erkit viettävät kotikeskeistä eläkeläiselämää. Yksinkertaiset ilot, kuten marjastus, kävelylenkit sekä puuhastelu kotona tai mökillä rytmittävät heidän arkeaan.

	He nauttivat arkisista askareista ja ottavat omaa aikaa lähtemällä kalaan tai luonnon helmaan.

	Myös urheilun seuraaminen TV:stä on mieluisaa puuhaa.

	Eevat ja Erkit ovat heränneet huolehtimaan omasta kunnostaan. Silloin tällöin tehtävät sauvakävely- ja hiihtolenkit ovat heidän tapojaan pitää terveyttä yllä.

	Moderni maailma laitteineen ja uudistuksineen tuntuu heille välillä hieman liian kaukaiselta.

	Eevat ja Erkit eivät kaipaa jatkuvaa mediatulvaa ympärilleen. He haluavat valikoida kiinnostavimmat aiheet ja perehtyä niihin rauhassa.

	Heidän päivänsä käynnistyy sanomalehden lukemisella ja loppuu iltauutisiin.

	Myös paikalliset uutiset kiinnostavat Eevaa ja Erkkiä.

	Uutiset ovat tärkeä osa päivää ja niitä seurataan ensimmäisenä aamulla ja viimeisenä illalla. He eivät halua olla kuitenkaan koko ajan medioiden äärellä.

	Tv:n urheiluohjelmien parissa viihdytään uutisten vastapainoksi.

	Ovat kiinnostuneita lähiympäristön asioista ja siitä, mitä omalla paikkakunnalla tapahtuu.

	He ovat kiinnostuneita perehtymään uutisten taustoihin.

	Mediankäyttöä ohjaa tavat, ei muuta paljon käyttäytymistä



	SMF9:
	Tyypillisimmin Marjatat ovat yli 55-vuotiaita, puolisonsa kanssa aktiivista eläkeläiselämää viettäviä naisia. He nauttivat kotiaskareista, käsitöistä, luonnossa liikkumisesta ja läheisten tapaamisesta. Vapaa-ajalla he suosivat aktiviteettejä, jotka eivät vaadi suuria rahallisia panostuksia.

	Mediakäyttöä ohjaavat vuosien tottumukset: he eivät ole valmiita luopumaan tutuista medioista ja sisällöistä. Aikakauslehtien lukeminen on Marjatoille tärkeä rentoutumiskeino ja uutiset he lukevat sanomalehdestä.

	Nykypäivän media- ja uutistulva välillä hieman rasittaa Marjattoja.

	Heidän päivänsä täyttyvät kotiaskareiden ja käsitöiden parissa puuhailusta, luonnossa liikkumisesta ja ystävien tai sukulaisten tapaamisesta.

	Oman fyysisen kunnon ja terveyden vaaliminen on heille tärkeää. He viettävät mielellään aikaa myös omalla mökillä.

	Heistä on myös mukavaa, kun läheiset tulevat kyläilemään.

	Oma koti tarjoaa mukavan suojapaikan maailman myllerryksiltä.

	Marjattojen vuorokautta rytmittävät tutut mediat ja sisällöt. He valitsevat katsomansa televisio-ohjelmat etukäteen, eivätkä mielellään jätä lempisarjojaan katsomatta.

	He haluavat lukea erityisesti positiivisista aiheista ja muiden elämästä, mutta seuraavat myös uutisia.

	Rentoutuvat ja viettävät vapaa-aikaansa lukemalla monipuolisesti erilaisia aikakauslehtiä. Valitsevat aikakauslehdistä ja uutisista itseään kiinnostavat teemat ja syventyvät aiheisiin rauhassa.

	Ovat kiinnostuneita eettisistä asioista.

	Haluavat välillä rauhoittaa elämänsä medioilta ja keskittyvät muihin askareisiin.

	Ovat perinteisten medioiden kuluttajia ja haluavat tilata sanoma- ja aikauslehdet kotiinsa.

	Haluaa rauhoittaa vapaa-ajan medialta, välillä sulkee kaikki laitteet

	Mediakäyttöä ohjaa tavat, ei muuta paljon käyttäytymistä

	M1:
	During the weeks besides going to work he sometimes exercises afterwards with his girlfriend. On the evenings before going to bed they usually watch regular TV.

	On the weekends the two of them likes to do a lot of activities like stroll in the city, take the car to visit some of their friend, buy things to their apartment.

	Sunday evenings he usually prepare lunch boxes for the following week and then watches a movie with his girlfriend

	If he is home alone he usually watch something randomly on a VoD services.

	DM2N:
	has adopted digital TV but keeps watching TV in a traditional, linear way

	Is aware of the possibilities with regard to time shifting and on-demand viewing, but does not use them

	Follows the treaditional broadcast schemes

	Is not willing to spend additional money, for example, to get access to additional thematic channels, to order on demand content

	Is not interested in TV connected through the internet

## Favourite shows

News
Domestic drama shows
Nature documentaries

	SMF10:
	News
	Current issue shows
	Politics
	Sports
	Documentaries

	SMF9:
	Music and entertainment shows
	Health, fitness and wellness shows
	Domestic drama shows
	Domestic moviees
	Nature documentaries
	News
	Interior decoration shows
	Food shows
	Talk shows

## Media use goals

- Motivations
- Behaviour

- Staying current with local and global news
- Relaxation and spending free time
- Following favourite shows, they've watched for a long time
- Habitual TV viewing

asdf

	SMF10:
	He haluavat valikoida kiinnostavimmat aiheet ja perehtyä niihin rauhassa. Heidän päivänsä käynnistyy sanomalehden lukemisella ja loppuu iltauutisiin. Myös paikalliset uutiset kiinnostavat Eevaa ja Erkkiä.

	Uutiset ovat tärkeä osa päivää ja niitä seurataan ensimmäisenä  aamulla ja viimeisenä illalla. He eivät halua olla kuitenkaan koko ajan medioiden äärellä.

	He ovat kiinnostuneita perehtymään uutisten taustoihin

	Ovat kiinnostuneita lähiympäristön asioista ja siitä, mitä omalla paikkakunnalla tapahtuu.

	Tv:n urheiluohjelmien parissa viihdytään uutisten vastapainoksi.

	He pyrkivät välttämään jatkuvaa mediatulvaa, vaikka ovatkin kiinnostuneita ajankohtaisista asioista. Uutisia ja ajankohtaisia lähiympäristön asioita he seuraavat lähinnä perinteisistä kanavista, televisiosta ja sanomalehdistä.

	Haluaa rauhoittaa vapaa-ajan medialta, välillä sulkee kaikki laitteet

	Mediankäyttöä ohjaa tavat, ei muuta paljon käyttäytymistä



	SMF9:
	Marjattojen vuorokautta rytmittävät tutut mediat ja sisällöt. He valitsevat katsomansa televisio-ohjelmat etukäteen, eivätkä mielellään jätä lempisarjojaan katsomatta.

	He haluavat lukea erityisesti positiivisista aiheista ja muiden elämästä, mutta seuraavat myös uutisia.

	Rentoutuvat ja viettävät vapaa-aikaansa lukemalla monipuolisesti erilaisia aikakauslehtiä. Valitsevat aikakauslehdistä ja uutisista itseään kiinnostavat teemat ja syventyvät aiheisiin rauhassa.

	Välittävät eteenpäin positiivisuutta ja haluavat saada sitä myös itselleen. Ovat kiinnostuneita eettisistä asioista.

	Haluavat välillä rauhoittaa elämänsä medioilta ja keskittyvät muihin askareisiin.

	Ovat perinteisten medioiden kuluttajia ja haluavat tilata sanoma- ja aikauslehdet kotiinsa.


## Pain points

- Frustrations

- Media overflow, especially negative news

asdf

	SMF10:
	Eevat ja Erkit eivät kaipaa jatkuvaa mediatulvaa ympärilleen

	Moderni maailma laitteineen ja uudistuksineen tuntuu heille välillä hieman liian kaukaiselta.

	SMF9:
	He haluavat lukea erityisesti positiivisista aiheista ja muiden elämästä, mutta seuraavat myös uutisia.

	Välittävät eteenpäin positiivisuutta ja haluavat saada sitä myös itselleen. Ovat kiinnostuneita eettisistä asioista.

	Nykypäivän media- ja uutistulva välillä hieman rasittaa Marjattoja.

	M1:
	His girlfriend takes care of technical problems that occur in the household.


## Second screen use

- If different digital platforms are used together? Second screening habits

Does not use second screens while watching TV.

	SMF10:
	Haluaa rauhoittaa vapaa-ajan medialta, välillä sulkee kaikki laitteet

	SMF9:
	Haluaa rauhoittaa vapaa-ajan medialta, välillä sulkee kaikki laitteet

	M1:
	These programs he is familiar with and trusts more. He thinks that if he just put in time the problem will be solved which he don’t think about VoD services.

	DM2N:
	Is not interested in TV connected through the internet



## Social context of TV media use

Watches the evening news with her husband. They have one domestic drama show they always watch together. Other than that Elsa watches her favourite shows alone.

	SMF10:
	Heidän päivänsä käynnistyy sanomalehden lukemisella ja loppuu iltauutisiin. Myös paikalliset uutiset kiinnostavat Eevaa ja Erkkiä.

	SMF9:
	Marjattojen vuorokautta rytmittävät tutut mediat ja sisällöt. He valitsevat katsomansa televisio-ohjelmat etukäteen, eivätkä mielellään jätä lempisarjojaan katsomatta

	M1:
	During the weeks besides going to work he sometimes exercises afterwards with his girlfriend. On the evenings before going to bed they usually watch regular TV.

## Skills and knowledge

- General computer and/or internet use
- Frequently used products, product knowledge
- Special skills


Not that interested in technology
If there's a problem with TV, she'd rather just do something else and wait for the problem to fix itself
Has digital TV and knows of its features, but does not use them


	SMF10:
	Edelläkävijyys: 6/8

	Moderni maailma laitteineen ja uudistuksineen tuntuu heille välillä hieman liian kaukaiselta.

	SMF9:
	Edelläkävijyys: 5,5/8

	M1:
	Not that interested in technology.

	Are more satisfied with bad video quality then having to go thought a troubleshooting.

	He would just quit an account without complaining if he was dissatisfied.

	He thinks that his computer often is the problem and he thinks that he is not technical enough or is even willing to invest the time to fix it.

	His friend once came over and sat with his computer for one hour and installed a lot of nice things and helped him with a virus that he had. After this his computer worked very well for a longer time period.

	These programs he is familiar with and trusts more. He thinks that if he just put in time the problem will be solved which he don’t think about VoD services.

	If he is alone and just wants to watch something quickly and there is a problem he will not try to solve it.



	DM2N:
	has adopted digital TV but keeps watching TV in a traditional, linear way

	Is aware of the possibilities with regard to time shifting and on-demand viewing, but does not use them

	Follows the treaditional broadcast schemes

	Is not willing to spend additional money, for example, to get access to additional thematic channels, to order on demand content

	Is not interested in TV connected through the internet

	S1:
	Non prosumers. Low interactino with any media, moderate interest in co-design

	Does not want to pay for content
