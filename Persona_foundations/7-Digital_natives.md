# Fully integrated digital natives

- DM3P - Internet TV positive
- SMF2 - Annat ja Aleksit, lapsettomat digitaaliset
- S2 - Mature prosumer
- S6 - Proactive prosumer
- BL4 - Connected maestros

They were born digital and it shows in their media use. They are mature media users in their late 20s and use media to interact with their friends, media and the world. They share content online continuously and participate in discussions online. They are willing and able to pay for content and consume most of their content online. They watch TV media when it suits them through VOD services.

Name: Mike




## Private information

- Name: Mike
- Age: 29
- Gender: Male
- Marital status: Single
- Number of children: 0
- Where does the persona live?: Downtown Helsinki
- Disabilities
- Role(s) and tasks
	+ Specific company or industry
	+ Job title or role
	+ Salary/disposable income


asdf

	SMF2:
	Asuu Helsingissä
	Keskitulot per capita: 1440€
	Keski-ikä: 26,6

	Keksitty:

	Analyst at a large accounting company -> vastavalmistuneiden ekonomiestien keskipalkka 3100 -> bruttotulot 2000e


## Description of work and daily life

+ What does the persona do during an average (work) day in connection to the focus area?
+ Typical activities
+ Important atypical activities
- Describe the persona's use of different digital platforms?
	+ Which sources of video media are used?
	+ How much they are used?
	+ What they are used for?
- What is the personas relation to different medias?

Annikat ja Aleksit ovat elämäntyyliltään aktiivisia, sosiaalisia ja seikkailunhaluisia. Heillä on vankka sosiaalinen omatunto ja he ovat taitavia verkostoitujia. He viettävät mielellään aikaa samanhenkisten ihmisten parissa sekä verkossa että kasvokkain. He käyttävät eri pikaviestipalveluita luontevasti yhteydenpitoon.

Vapaa-aikaan kuuluu matkustelun lisäksi livekeikat, elokuvista nauttiminen ja urheilu. Työ on tärkeä osa heidän elämäänsä, mutta se vie toisinaan liikaa aikaa rakkailta harrastuksilta. Osaavat myös rentoutua viihteen parissa syvällisten sisältöjen vastapainoksi ja kaipaavat elämyksiä. Erilaiset pelit ovat osa vapaa-ajan viettoa. Uses streaming services such as Netflix or Hulu in order to be able to immediately consume very recent content that is of interest, e.g. popular American fiction series are watched on the day that they are broadcasted in the US

Haluavat saada heti tiedon, kun jotain merkittävää tapahtuu. Eivät usko mitä tahansa, haluavat tietää totuuden ja tarkistavat uutisten alkuperäiset lähteet. Kansainvälisyys on luonnollinen osa heidän arkeaan. Is very interested in interacting with television and the digital press and it is highly innovative when interacting with all of the digital media

Annikat ja Aleksit haluavat olla mahdollisimman monipuolisesti selvillä ajankohtaisista asioista. He haluavat verkostoitua aktiivisesti, oppia kokoajan uutta ja sivistää itseään.

Heillä on kiinnostusta ja halua vaikuttaa ympäröivän maailman asioihin.Keskustelevat mielellään päivänpolttavista aiheista ja ovat aktiivisia jakamaan sisältöjä ja uskovat median muuttavan maailmaa. Valitsevat mediapaljouden keskeltä itseä kiinnostavat teemat ja perehtyvät niihin syvällisemmin. Lukeminen on heille tärkeää. will agree to pay for digital content




	BL4:

	These advanced activities include talking using voice IM; regularly consuming media content (like games, music or video) via mobile devices (such as phones and tablets); frequently accessing multiple apps via mobile devices; and regularly checking news, weather, sports scores and information searches via mobile devices.

	DM3P:

	uses streaming services such as Netflix or Hulu in order to be able to immediately consume very recent content that is of interest (e.g., popular American fiction series are watched on the day that they are broadcasted in the US)

	S2:
	will agree to pay for digital content and it dedicates a good deal of time to social media

	S6:
	is very interested in interacting with television and the digital press, while showing a moderate interest in interacting with the radio and it is highly innovative when interacting with all of the digital media

	agrees with paying for digital content and the time dedicated to consuming social media is moderate

	SMF2:

	Annikat ja Aleksit ovat aktiivisia verkostoitujia ja he käyttävät eri pikaviestipalveluita luontevasti yhteydenpitoon. Kansainvälisyys on luonnollinen osa heidän arkeaan. Heillä on kiinnostusta ja halua vaikuttaa ympäröivän maailman asioihin. Syvällisten sisältöjen ohella he viihtyvät erityisesti netti-TV:n parissa.

	Annikat ja Aleksit ovat elämäntyyliltään aktiivisia, sosiaalisia ja seikkailunhaluisia. Heillä on vankka sosiaalinen omatunto ja he ovat taitavia verkostoitujia.

	He viettävät mielellään aikaa samanhenkisten ihmisten parissa sekä verkossa että kasvokkain.

	Vapaa-aikaan kuuluu matkustelun lisäksi livekeikat, elokuvista nauttiminen ja urheilu. Työ on tärkeä osa heidän elämäänsä, mutta se vie toisinaan liikaa aikaa rakkailta harrastuksilta.

	He myös tietävät mitä omalta tulevaisuudeltaan haluavat.

	Haluavat saada heti tiedon, kun jotain merkittävää tapahtuu. Eivät usko mitä tahansa, haluavat tietää totuuden ja tarkistavat uutisten alkuperäiset lähteet.

	Valitsevat mediapaljouden keskeltä itseä kiinnostavat teemat ja perehtyvät niihin syvällisemmin. Lukeminen on heille tärkeää.

	Keskustelevat mielellään päivänpolttavista aiheista ja ovat aktiivisia jakamaan sisältöjä. Tarttuvat huomaamiinsa epäkohtiin ja uskovat median muuttavan maailmaa.

	Osaavat myös rentoutua viihteen parissa syvällisten sisältöjen vastapainoksi ja kaipaavat elämyksiä. Usein seuraavat televisiota netti-TV:n kautta. Erilaiset pelit ovat osa vapaa-ajan viettoa.


## Favourite shows

- Documentaries
- Foreign movies
- Foreign drama series
- Foreign comedy shows

asdf

	SMF2:
	Tiedeohjelmat
	Scifi- ja fantasiasarjat
	Dokumenttiohjelmat
	Ulkomaiset elokuvat
	Ulkomaiset draamasarjat
	Ulkomaiset komediasarja


## Media use goals

- Motivations
- Behaviour

asdf

	DM3P:

	is ready and eager to experience future TV now

	S2:

	has an immense interest in interacting with all the communication media (tv, digital press, radio) in the pre-established manner

	S6:

	is very interested in interacting with television and the digital press while showing a moderate interest in interacting with the radio

	SMF2:

	Heillä on kiinnostusta ja halua vaikuttaa ympäröivän maailman asioihin.Syvällisten sisältöjen ohella he viihtyvät erityisesti netti-TV:n parissa.

	Annikat ja Aleksit ovat elämäntyyliltään aktiivisia, sosiaalisia ja seikkailunhaluisia. Heillä on vankka sosiaalinen omatunto ja he ovat taitavia verkostoitujia.

	Annikat ja Aleksit haluavat olla mahdollisimman monipuolisesti selvillä ajankohtaisista asioista. He haluavat verkostoitua aktiivisesti, oppia kokoajan uutta ja sivistää itseään.

	He ovat tiedonjanoisia sisältöjen kuluttajia, mutta osaavat myös rentoutua viihteellisten sisältöjen ja pelien parissa niin halutessaan.

	Haluavat saada heti tiedon, kun jotain merkittävää tapahtuu. Eivät usko mitä tahansa, haluavat tietää totuuden ja tarkistavat uutisten alkuperäiset lähteet.

	Valitsevat mediapaljouden keskeltä itseä kiinnostavat teemat ja perehtyvät niihin syvällisemmin. Lukeminen on heille tärkeää.

	Keskustelevat mielellään päivänpolttavista aiheista ja ovat aktiivisia jakamaan sisältöjä. Tarttuvat huomaamiinsa epäkohtiin ja uskovat median muuttavan maailmaa.

## Pain points

- Frustrations

asdf

	Työ on tärkeä osa heidän elämäänsä, mutta se vie toisinaan liikaa aikaa rakkailta harrastuksilta.

	Haluavat saada heti tiedon, kun jotain merkittävää tapahtuu. Eivät usko mitä tahansa, haluavat tietää totuuden ja tarkistavat uutisten alkuperäiset lähteet.




## Second screen use

- If different digital platforms are used together? Second screening habits

asdf

	DM3P:

	..is also a very intensive internet user who uses multiple devices ands creens (e.g., smartphone, tablet) to access the content he is interested in

## Social context of TV media use

	SMF2:

	He viettävät mielellään aikaa samanhenkisten ihmisten parissa sekä verkossa että kasvokkain.

	Pitävät siitä, että jokin uutinen nousee puheenaiheeksi ja kommentoivat uutisia medioissa. Sisällöt saavat herättää myös tunteita.

## Skills and knowledge

- General computer and/or internet use
- Frequently used products, product knowledge
- Special skills

asdf

	DM3P:

	..is also a very intensive internet user who uses multiple devices ands creens (e.g., smartphone, tablet) to access the content he is interested in.

	is very much into new technologies and has a real innovator profile. New gadgets and ICT products are immediately adopted, price is not an issue

	is ready and eager to experience future TV now

	S2:
	consumes a great deal of the internet,  it has a high level o fdigital competences and skills

	s6:
	Has a moderate lvel of digital competences and skills

	SMF2:
	Edelläkävijyys 6,5/8
