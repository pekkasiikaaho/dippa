## Intro

- Haastattelu osa diplomityötä, jossa tutkitaan persoonia, joiden luomiseen ei ole käytetty suoraa käyttäjätutkimusta
- Haastattelun osana on myös suunnittelutehtävä, mutta haastattelussa ei arvioida suunnittelijan taitoja tai tehtävän lopputulosta

## Tausta

- Mikä on koulutustaustasi?
- Kuinka kauan olet työskennellyt käyttäjälähtöisessä suunnittelutyössä?
- Kuvaile lyhyesti nykyisiä työtehtäviäsi

## Kokemus persoonista

- Oletko käyttänyt suunnittelutyössä persoonakuvauksia?
- Mitä sinulle tulee ensimmäiseksi mieleen persoonakuvauksien käytöstä suunnittelutyön tukena?
- Milloin viimeksi käytit persoonakuvauksia?
- Kuinka monessa eri projektissa viimeisen 10 vuoden aikana?
- Kerro projekteista, joissa niitä käytettiin.
	- Minkälainen oli viimeisein projekti, jossa käytit persoonakuvauksia?
	- Minkälainen oli parhaiten onnistunut projekti, jossa käytit persoonakuvauksia?
	- Minkälainen oli huonoiten onnistunut projekti, jossa käytit persoonakuvauksia?
- Mitä mieltä olet persoonakuvauksista työkaluna?

## Design-tehtävä

Esittele design tehtävä, aikaa 15 min. Muistuta, että ei arvioida suunnittelijan taitoja tai syntynytta lopputulosta. Anna tehtävänanto, HbbTV intro ja Mary-persoona.

Materiaalit:

- Kyniä, papereita
- Tehtävänanto
- HbbTV intro
- Persoonakuvaus Mary

Kysymykset:

- Kerro konseptistasi. Mitä teit?
- Mikä oli käyttämäsi prosessi?
- Miten käytit persoonakuvausta suunnittelusi apuna?
	+ Mitä hyötyä koit saavasi persoonakuvauksesta?
	+ Mitä osia käytit hyödyksi?
	+ Oliko persoona jollain tavalla puutteellinen?
	+ Erosiko persoona jollakin tavalla aiemmistä käyttämistäsi persoonista?

## Persoonan arviointi

Ota persoona pois haastateltavalta.

- Kuvaile persoonaa lyhyesti omin sanoin
- Mitkä asiat jäi päällimmäisenä mieleen?
- Oliko persoonassa jotain yllättävää?
- Muistuttiko persoona jotain tuntemaasi henkilöä?

## Kaikkien persoonien arviointi

Pyydä lukemaan kaksi muuta persoonaa läpi kevyesti. Anna ensin luettavaksi Susie, sitten Danny. Anna myös Mary takaisin, kun persoonat on luettu.

- Mitä eroja havaitsit persoonien välillä?
- Yllättikö näissä persoonissa jokin sinut?
- Miten järjestäisit nämä kolme persoonaa uskottavuuden mukaan? Miksi?
- Kuinka uskottavana pidit persoonakuvauksia? Miksi?
- Mikä tekijät vaikuttivat persoonan uskottavuuteen?
	+ Persoonan ominaisuudet
	+ Tiedot persoonan taustoista
- Minkälaista dataa uskot persoonien taustalla olevan?
	+ Miten koet sen vaikuttaneen persoonien uskottavuuteen?
	+ Miten uskot sen vaikuttaneen persoonien luomiseen?
	+ Miten uskot sen vaikuttaneen persoonien käytännön hyödyntämiseen?

## Lopetus

- Onko kysymyksiä tai jotakin mitä haluaist kertoa aiheeseen liittyen?