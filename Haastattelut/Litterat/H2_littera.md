Jees. Eli  nauha on käynnissä. Tota tota. Kiitos, kun suostuit haastateltavaks. Ja tää tosiaan tulee mun dippaa varten ja siinä mä tutkin persoonien käyttöä ja tekemistä, jos ei pääse tekee käyttäjätutkimusta ite. Ja tässä niinkun arvioidaan sitten. Tai mä oon nyt luonut niinkun persoonat ja nyt arvioidaan sit niitä. Ja niitten käyttöä ja sit vähän selvitetään miten niinkun se on tottunut käyttää persoonia. Ja tässä on pieni suunnittelutehtävä, mutta siinä on hyvä muistaa että siinä ei arvioida suunnittelijaa eikä suunnittelun lopputulosta vaan sitä että miten niitä käytetään hyväks niitä persoonia. Ja ja ei kai siinä.. Varmaan aika tuttu formaatti.

Hyvä, eli siis tarkennuksena, että ne persoonat on luotu siis ilman että on  mitään dataa

Ilman, että.. Siis dataa on taustalta, mutta ilman, että on ite tehty käyttäjätutkimusta eli sekundaarisen datan pohjalta

Niin just et tavallaan et kuullun tai luetun tai..

Joo.

Selvä.

Joo. Onks muuta kyssäreitä?

Ei tähän hätään.

Mahtavaa. Mä otan vihkon ja kirjotan siihen muistiinpanoja. Mä sit  litteroin tän vielä perästä. Siinä on sitten tän viikon urakka myös. Tota alotetaan ihan perus taustoista: kuka oot mitä teet ja niin..

Nimiä ei saa ilmeisestikään vai..

Ihan sama oikeastaan. Mä voin. Näitä litteroita ei liitetä sitten.

No johanna 38 vuotta. Ammatilta tällä hetkellä UX project manager. Tuastalla semmonen 15 vuotta niin kun käyttäjäkeskeisen suunnittelun saralla lähtien käytettävyydestä ja tota.. Kahden lapsen äiti, 3 ja 8 tällä hetkellä ja kiireistä arkea eleleepi. Koittaa vähän töitäkin tehdä siinä ohessa. Ja tota. Siinä se ehkä. Vapaa-aika menee aika lailla noissa perhe-asioissa ja tota töissä tosiaan Leadinilla.

Ja mikä sulla on koulutustausta?

Aa no siis insinööri olen vuodelta.. Valmistunut 2000. Ja sit on kognitiotieteen opintoja päälle.

aa, okei. Minkä alan insinööri?

Anteeks?

Minkä alan insinööri?

Viestintätekniikan ja sit on tota erinäisiä kursseja työn ohessa käyty. Niin kun tiimin johtamiseen ja agileen ja erilaisia.. Nokialla siis pitkään ollut ja siellä käynyt näitä kursseja.

Joo. Sanoit, et sä oot UX projektipäällikkö, niin mitä se käytännössä tarkottaa sun.. minkälaista sun työarki on?

No se onkin hyvä kysymys. Se vähän [naurahtaa] Onks semmosta määritelmää mikä on UX projektimanageri. Se on ehkä semmosta tietynlaista lankojen käsissä pitämistä mut aika paljon myös osallistuu laadullisen työhön ja erityisesti siihen briiffivaiheeseen että on niinkun selkeä briiffi designilla käytössä asiakkaan kanssa. Alkuvaihe painottuu aika paljon. Toki sit semmonen perus seuranta. Mut ehkä se isoin lisäarvo on siinä alkuvaiheen kartoituksessa ja katotaan että on yhteiset päämäärät ja että tiedetään mitä ollaan tekemässä ja mitä tarvitaan, mitkä on ne prioriteetit semmosta. Mutta myöskin ihan kyllä siihen kuuluu ihan laadullistakin design työtäkin ja paljon tutkimusta siin osallistumista. Konseptointia. Aika laajalla rintamalla kaikennäköstä.

Ooks sä käyttäny sun suunnittelutyössä niinku koko työhistorian aikana niiku persoonia hyödyks tai persoonakuvauksisa

No itseasiassa tota noin niin, kyllä niitä on aina siinä pyörinyt mukana, mut se konkreettinen hyöty niistä on ollut kyseenalainen. Et se on varmaan sellanen aika yleinen mitä nyt kuulee muutenkin että esimerkiks nokialla pyöri. Siel oli ihan markkinatutkimusosasto ja tämmönen jotka rakensi ja segmentointi. Niinkun tavallaan kuluttajasegmentointikoneisto, jotka siis pyöri ja niinkun isolla rahalla pyöri ja kehitteli persoonia. Et dataa oli paljon käytettävissä ja sitä hyvinkin niinkun hierottua persoonadataa. Mut se mikä sen arvo loppujenlopuksi suunnittelutyössä ja paljonki niitä käytettiin oli kyllä loppujen lopuksi aika vähäistä. Ja sitten siellä näki semmosta että ihmiset väsyi niihin persooniine, että ne samat persoonatyypit vähän pyöri siellä. Taas tää Joe taas tää sitä. Että neki ois vaatinu sellasta niinkun ehkä vähän semmosta fressimpää otetta sitten jossain kohtaa. Että ihmiset alko ehkä vähän väsyäkin niiihin. Ne nähtiin semmosena vähän, vähän niinkun. Se konkreettinen hyöty ei ehkä ihan realisoitunut siinä määrin ku ehkä voisi.

Mistä.. Mistä se johtu? Tai sanot, että porukka väsy, että oliks se samoja projekteja eri projekteissa

Joo ja se saattaa olla sitten vähän sitten laadustakin kiinni ja sit siel on aina joku lentokenttä keissi ja ihmiselt alko nauraa niille että taas täällä Joe täällä lentokentällä, että come on. Et semmomosta ehkä tiettyy mielikuvitusta olis voinu käyttää enemmän ja niinkun enemmän puhuu siiitä vähän eri konteksteissa käsitellä niitä henkilöitiä ja ottaa vähän laajemmpin niitä keissejä. Ne oli ehkä niin geneerisiks rakennettu, semmoseks kansainväliseks kiireiseks joku technology tämmönen early adapter hahmo, niin. Se on niin niinkun ehkä vähän väsytti. Toki dataa oli käytettävissä paljon, että olihan semmosta autenttista videoita niistä ihan niinkun. Et oli ihmisiä haastateltu et pysty kattomaan niinku vaikk avideoita mutta se sitten se mikä aika oli käyttää.. Sehän on työlästä, työlästä, työlästä. Niin sitten siinä arjessa, se ei hkä sitten. Ja sitten ne tahot, joitten ois pitänyt ehkä ymmärtää paremmin  persoonia elikkä vähän niinku jos ajatteli tuotepäälliköitä ja sit johto, niin ne ei sitten taas ehkä. No, mulla ei ihan suoraa näkyvyyttä, että mitä ne sillä datalla teki. Mutta tuntuma on se, että ei juurikaan. Tai hyvin pintapuolisesti raapaisi. Peilasi vaan niitä omia prioriteettiä niitten kautta. Ei oikeen. Vähän semmosta puoliylimielistäkin. En tiedä. Siis ehkä tota noin niin, vähän vähiin jäänyt se hyödyntäminen. Toki niitä tehtiin paljoon ja kyllä ne siellä pyöri, mtu sit ehkä. Ehkä sitä ei sitten osattu hyödyntää ihan niin kun pitäisi. Tai sit se laatu ei niissä ollut semmonen konkreettinen.

Oliks sä ikinä ite tekemässä niitä?

En ollu tekemässsä et olin toki.. Luin paljon sitä dataa taustalla. Enemmän.. Tuli joku persoona ni sit enemmänkin kaivautu siihen et mitä siellä on takana. Et ei ehkä, ehkä varsinaisesti persoonakuvauksia käyttänyt juurikaan.

Millon sä oot viimeksi käyttänyt persoonia?

No nythän me taas raken.. Vähän niiku kokeillaan tämmösessä niinkun. No ihan tyyliin viikko sitten rakennettiin yks persoona yhteen keissiin, mutta se on enemmänkin semmonen demokeissi, joka ei niinkän pohjaudu ssitten semmoseen tutkimukseen, dataan, vaan enemmänkin tämmönen demonstroi että mitä tää persoona voi olla ja miten sitä mahdollisesti sitten sitten voi käyttää. Tai tavallaan tdemonstroida sitä että kun käyttäjätietoa kerätään niin mitä siitä voi jalostaa niin kun yhtenä esimerkkinä. Tämmönen keissi esimerkki. Ei niinkäään oikee persoona, jota käytetään oikeesti suunnittelutyössä. Demo.

Osaaks sä arvioida kuinka monessa projektissa sä oot abaut 10 vuoden aikana käyttänyt persoonia?

No ku ei suoraan käyttänyt. Ei ole suoranaisesti käyttänyt persoonia, että otetaan tästä vaikka tämä Joey ja sitten katotaan, että.. Enemmänkin tutustunut kylläkin siihen käyttäjädataa, mutta.. Sanotaan, että [tauko] no vuosi sitten.

Siis kuinka monta kertaa?

Kuinka monta kertaa on käyttänyt? Ei pysty sanomaan. En pysty sanomaan. Useita.

Onks sul mielessä joku projekti niitä ois käytetty hyvin tai josta niistä ois ollut apua? [epäselvää] Projekti, jossa olis käytetty onnistuneesti persoonia

No valitettavasti ei kyl oo. Ei kyl tuu mieleen semmosta hyvää. enemmänkin tulee mieleen niitä huonoja esimerkkejä. ETtä tavallaa on semmonen data siellä, mutta se on vähän että hohhoijaa. Että se ei.. Ei tuu mieleen semmosta niinkun kuningaspersoonaa, joka ois niinku konkreettisesti auttanut eteenpäin. Et enemmänkin on tavallaan nähnyt miten jotkut on tehnyt niiitä persoonia, heijastellut omia niinkun tavallaan mielipiteitään ja tota niinkun näkemyksiään niissä. Nähnyt eoikeesti huonoja persoonia. Ni enemmänkin ne on ollut haitaks. Et enemmänkin haluis pyyhkiä sen tyypin ja kyseenalaistaa et mikä tääkin homma tässäkin on. Mistä tää on tullut ja.. Semmonen tietynlainen epäluotto siihen. Et haistaa sen, et se ei oo kauheen laadukas.

Ja onks se ollu seemmonen. . Tai siis miten se on nkäynyt siinä projektissa et onks ne persoonat vaan unohdettu tai koitettu...

Sitä on koitettu tappaa [naurua] Konkreettisesti niinku että. Mutta jokuhan siihen aina ihastuu sitten ja pyrkii.. On nähyny sitä että designerit tekee itte itelleen, joskus on vääntänyt jonkun esimerkin, että täll mä nyt suunnittelen ja.. Ja tota sitten väännetään, että mistäs tää nyt tuleekaan tää tyyppi ja semmosta. Enemmänkin yrittänyt vaan silleen niinku unohtaa sen [epäselvää] Yli ja eteenpäin. Silleen nätisti.

Nyt täs niin.. Haluuks sä summeerata vielä mitä mieltä sä oot yleisesti persoonista työkakuna?

Mä väitän että se on niinkun voimakas työkalu kun se tehdään hyvin. Se ei riitä se että, on niiinkun muutama slaidi tai yks a€ tai yks kortti, joka kertoo, että tässä on tää Sonja tai joku tyyppi joka on niinkun tän ikänen ja täs on sen goalit ja motivaatiot. Joo, se on ihan kiva et saa.. Mut sit tavallaan, et se olis suunnittelun tukena, niin se pitäs niinkun tavallaan niitten.. Esim palvelumuotuoilssa tehään consumer journey ja mietitään niitä touchpointteja tai day in of life tyyppisiä juttuja. Ne ois niinku oikeesti hyvin, laadukkaasti tehty, konkreettisitä, josta designeri sitten näkis niitten touchpointtien, sen kontekstin kautta, että missä se.. Mitä tämä tyyppi nyt sitten tekee tai miten konkreettisesti.. Ettei se jää semmoseks että tässä on tää kortti, tyyppi. Se tekis, tykkäs näistä asioista. Se ei ohjaa suunnittelua vielä riittävästi. Se jää liian hyhmäseks. Et tokihan se on hyvä pitää siel taustalla, mut sitten kun aletaan oikeasti tekee wireframeja ja miettiä että mitä siinä interaktiohetkessä on tärkeää tälle tyypille. Ni se ei auta se, että on vaan se ylätason hyhmänen. Pitää ymmärtää et mitä se.. Minkälaisessa kontekstissa se on se tyyppi sillä hetkellä. Minkälainen se on se käyttötilanne. Mitä se.. Ketä muita siiinä on ympärillä. Minkälainen hetki sillä on siinä käytettävissä ja sitte mitä se oikeesti tekee siinä. Niin, sen tason kuvaukset on jos itten aika paljon konkreettisempia. JA siihen kun yhdistää sen että, sen ihmisen niinkun tavat toimia ja prioriteetit, ni sit sä niinkun tiedät paljonko sillä kestää hermot tietyssä hetkessä tai mitkä sil on.. Siihen pystyy niinku eläytyy ihan erilailla siihen hetkeen. Et ehkä enemmänkin sen kautta et joo, tehdään ne persoonat, mutta sitten jalostetaan ne sitten vielä semmoseks laadukkaiks käyttötilanteiksi. unohdetaan se lentokenttä. Ni et on oikeesti niitä päivittäisiä juttuja mitä se tkeee mieluummin. [tauko] ehkä tää lentokenttä on Nokian siäsinen vitsi, et tiiäksä on aina joku kiireinen tyyppi mobiililaitteen kans lentokentällä ni se ei jotenkin.. voi come on. Mutta sillee se vois ehkä jalostua. Semmoseks konkreettiseks designin työkaluks. [tauko] Näin minä sen ehkä mieluiten tykkäisin nähdä.

No, katotaan mitä tykkäät, koska täs seuraavaks pääset vähän tutustuu sit yhteen persoonaan. Ja täs on niinkun on pieni design-tehtävä, jossa on.. Täs on myös kirjallinen tehtäävänanto, mutta voin lukee sen vaikka tästä.. Eli. Yleisradion tuottamassa uutta kotimaista sairaaladraamasarjaa. Draamasarjan yhteydessä on tarkoitus yhdistää HbbTV-teknologina tuomia mahdollisuuksia lusätä tv-lähetyksen vuorovaikutuksellisuutta. Tavoitteena on tarjota katsojille uusia tapoja nauttia sarjan katsomisesta. Tehtävänäsi on suunnitella konsepti, joka hyödyntää televisiota mediana. Yksi draamasarjan pääkohderyhmistä havainnollistaa Mary the Content Comfort Seeker. Yleisradio on antanut vapaat kädet luoda kohderyhmän tarpeisiin parhaiten sopiva konsepti. Voit havainnollistaa konseptia ja sen käyttötapaa tekstin tai piirosten avulla. Ja TV-näytöllä olevan käyttöliittymän ohjauslaitteena on kaukosäädin. Eli nyt konseptointitehtävä, johon tukena yks näistä persoonista. Ja aikaa on noin 15 minuuttia ja täs muistutan nyt taas et ei arvoida sua suunnittelijana tai suunnittelutyön lopputulosta

Eli tää hahmo on siis tää Mary

Joo ja se on tässä

Ja se on tässä.. Ja tavoitteena siis tämmönen niinkun vuorovaikutteinen TV. Ja sairaalasarja.

Ja siinä on.. Hetkonen, mä otin näköneen mä otin yhen kynän vaan, mut sinä on kynä ja paperia

Tää on siis.. Tää on kaikki sitä tyyppiä.

Joo. Ja siinä on lyhyt kuvaus myös HbbTV:stä ja sti siinä on kaukosäätimen kuva materiaalinan ja tos on vielä tyhjää paperia

Niin niin, tääl on tää käyttis. Pitääks tästä. Pitääks tää lukasta?

Se kannattaa lukasta myös. Siinä on se HbbTV:n tosi yleistasoinen kuvaus.

Okei.

[Haastateltava lukee materiaaleja]

Onks call to action täällä jossain niinku?

Taitaa näkyä täällä alareunassa tässä kuvassa

[Haastateltava lukee materiaaleja]

Eli se launchaa jonkun appin, mut täs ei määritellä mikä se appi..

Ei, se on HTML-teknologialla toteutettu, eli suurinpiirtein se mitä HTML-teknologialla pystyy

Okei..

[Haastateltava lukee materiaaleja]

Niin täs ei määritelllä sitä niinkun use casea periaattessa.. Onks tää niinkun konseptin idea niinkun se, että tiedät tehdään vaikka niiku mikä se ois tälle tyypille houkutteleva se äppi

[Haastateltava lukee materiaaleja]

Mikä tää second... Most of the time she watches only the televition with little second screen.. Mitä se tarkottaa?

Second screen tarkoittaa toista näyttöä tai toista laitetta. Toinen näyttö suhteessa televisioon.

Ja siis et se käyttää vaikka ipadia mutta se televisio on taustalla... Mitä täs haetaan?

Se, että käyttääkö jotain muuta laitetta samaan aikaan ku katsoo televisiota.

Okei.. [Tauko] Eli täs se siis tarkottaa tavallaan, et se on toissijainen näyttö se telkkari sille? Mä en ymmärrä tätä lausetta

Eli tota, enimmäkseen katsoo ainoastaan telkkaria ja käyttää hyvin vähän jotain muuta...

Okei. Pääfokus on telkkarissa sillon kun..

Joo.

Okei

[Haastateltava lukee materiaaleja]

Saaks kommentoida tota persoona itsessään, minkälainen fiilis tulee

Saa, tai joko nyt tai myöhemminkin

Tää niinkun vaan herättää semmosia niinkun.. Tulee heti semmonen et onks tä äniinkun oikeesti oikee tyyppi koska, täällä on tää on tällasia niinkun ristiriitasuuksia niinkun et, et tavallaan se telkkarin kattominen on sille semmonen, et hän tykkää kutoa ja viettää niin kun aikaa. Se on niinkun vähän taustalla se talkkari ja sit toisaalta katsoo miehensä kanssa sarjoja. Ni sit tääl on kuitenki se keissi et se niin.. Samanaikaisesti ku se juttelee kavereitten kanssa, niin kattoo telkkaria. Se on niinku.. Tulee semmonen ristiriita jotenkin. TAvallaan ku sä luot sellasen kuvan et tää tyyppi on.. ET miten se suhtautuu ylipäätään televisioon, ni sitten just tää että miten se niinkun.. Tuntuu yllättävältä, et se yhdistää sitten tota noin niin tämmösen.. Niinkun se on puhelimessa kavereiden kanssa ni se samalla muka kattois jotain ohjelmaa. Ni se on tosi yllättävä. Mutta siis, kun en nyt tiiä mistä tää. Niin täähän voi oikeesti ollakkin näin, mutta.. Mutta äkkiseltään tulis semmonen olo, että nyt on joku tuotepäällikkö tunkenut sen use casen tänne persoonan jo valmiiks. Se oli niinkun semmonen ensimmäinen mikä heräs. Mä en tiedä ku on niin skeptinen niitten persoonien kanssa muutenkin. Elikkä nyt se tehtävä on siis luoda tälle Marylle...

Jonkinlainen konsepti.

Et miten se tykkäis tehä.. No. No juurikin tämä tälleen vähän tälleen tuntuu vähän niinkun väkisin työntää, et nyt se sitten chattaa kavereitten, on puhelimessa kavereitten, niin ehkä sen ympärille lähtis sitten raknetaa muta vähän silleen.. Vähän silleen niinku pitkin hampain niiku lähtis tekee sitä.

Onks siel sit jotin niinku.. Tai niinku onks se niinku se mihin sä näkisit et se... olis sit se paras niiku Marylle tän mukaan?

No ku siis mä saan tästä sellasen kuvan, että tää ei ylipäätään ole aktiivinen TV:n katsoja ollenkaan. Tai jos on niin kattoo.. Käyttää sitä niinku yhdessä istuu iehensä kanssa ja rauhassa kattoo jotain sarjaa. Et siihen ei kuulu sellanen nyprääminen ollenkaan. Et jos se jotain nyprää ni se nyprää sitä kudinta. Ja mulla tulee semmonen et sillä ei missään nimessä oo kaukosäädin kädessä vaan sen miehellä on. Ja samoten myös se teleteksti et tää ei ees vähimmässä määrin.. Sekin oli yllättävää et niinkun tuli semmonen ristiriitaolo, et miks hän teletekstiä käyttää. Yleisesti se telkkari on enemmänkin sellanen tausta-aktivitettiin. Tai sit jos sitä katsotaan niin katotaan, keskitytään siihen sarjaan. Mut oisko siihen sitten jossain sen favourite sarjoissa jotain semmosia, semmosia niinkun.. [tauko] [epäselvää] tai sit jos sen miehensä kanssa haluis jotenkin tehä. Mä en jotenkin usko tähän, et se kavereitten kaa haluu jotain [epäselvää: hieroo?] siellä. Onks tossa määritelty et mitä se kattoo? Onks se edelleenkin niitä sen favoirite sarjoja mitä se kattoo sen kavereitten kanssa

Siin ei oo määritelty sen tarkemmin

Vois olettaa, että ne on sen favourite sarjoja myöskin. Ku se ei varmaan muuta siel aktiivisesti katokaan.

[Haastateltava lukee materiaaleja]

Ehkä tähän jotenkin smmonen. Ehkä tätä lähtis jotenki aukasee sieltä sitten. Et jos otetaan tuolta joku sen perus favourite sarja et se avaa jonkun chatti ikkunan ja sil on ne ipadikin joskus käytössä. Et se niinkun tsättäis tavallaan sillä ipadilla, sit se jotenkin näkyis sit siellä niinku.. Sen kaverit kattos sitä samaa sarjaa. Sen kaverit kattos sitä samaa sarjaa, mut se jotenkin näkis siihen. Mut se ei missään nimessä niinku kauko.. Mä en usko tähän. Et se tähän [puhuu kaukosäätimestä] lähtis ikinä. Mä en päivänä mitään tsättää tai yrittäkään.. Ni se vois vaikka, jos lähtis siitä et use case ois niinku se et ois niinku.. [piirtää samalla] Se ois niinku istuis sohvalla sen miehensä kanssa tai jotain, mutta.. Ei nyt tässä keississä se mies ei voi olla mukana, jos se on kavereittensa kanssa siinä. Sitten sil on tavallaan niinku ipadi.. Tää on joku tota noin.. tsätti käynnissä täällä näin. Joka sit jotenki heijastus tähän näin. Mut sit tää ukko ei ois kauheen kiinnostunut kattoo.. Tää on vähän kyssäri et onks tää täs mukana ollenakan. Et jos se ois vaikka työmatkalla sitte. Ehkä näin. Ja sitten täällä pyörii se sairaalasarja. Sitte siellä on se tota.. Tota heijastuuki tänne ne. Varmaan niiku tää. Mä en usko et tää ipadi on sille se enssijainen laite kuitenkin. [epäselvää, liittyen piirtämiseen]. Tääl ois se komee lääkärimies sitte.

[puhuu kaukosäätimestä]

nyt on Pari minuttia aikaa

Joo. Mutta Tommonen keissi. Se niinkun tavlalaan se mitä se nyt tekee, jos oletetaan et se siis tekis tota et se smaan aikaan juttelee kavereiden kanssa, kattoo vähän sarjaa ni.. Ni ehkä se ois parempi, onnistus sit paremmin jos ne oikeesti haluu keskittyy myös siihen sarjan kattomiseen, et se ois enemmänkin tällänen tsätti tyyppinen juttu. [tauko] sillon ku se ei jaksa kutoo eikä sen ukko oo paikalla. [tauko] Mut joo siis. Se mitä lukee tämmösta persoonaa ni tulee, tulee aina niinkun. sä tavallaan aina haet niin un omasta lähipiiristä koitat miettii semmosen oikeen tyypin ja sit miettii niinkun sen kautta. Se on aika vaikee välttyyki semmoselta et aa tää on. Se on aika yleistäkkin et "aa tää on niinkun mun veli" tai "aa tää on niinku mun veli" et semmosen johtopäätöksen varmaan ihmiset keskimäärin tekee kun ne lukee jotain persoona et ne saa jonkun oikeen persoonaan siihen. Ja sit ne miettii miten se käyttäytyy eri tilanteissa. Ja tässäkin auttas niinkun se... Täs tulee sellasii ristiriitasuuksia. "et eihän se mun mutsi näin tee" et tiiäksä rupee miettii näin. Ja sit ku on muutenkin puoliks jo epäileväinen näitten kanssa et kuka tän on tehny ja mistä se data on tullu. Ni vähän semmonen.. Tiäksä, et tuntuu et pitäs pystyy perustelee et miks se tekee näin. Ja sit ehkä niinkun tota se on niinkun se mikä ensimmäisenä tulee. Mut kylhän tästä pystyy niinkun poimimaan ne. Tääl on ihan valmiit niinkun syötteet. Mikä on ehkä vähän huonokin, ku ne tuntuu semmosilta et ne on joku tuotepäällikkö sinne tuntenut sinne persoonaan.

Mistä se.. Tai mikä se.. Mikä siinä vaikuttaa  erityisesti siltä, että se on jonkun sinne laittama poimuttavaksi?

Nää on. Nää on tommosii jotenkin niin yksit.. sillai teletext ja epg.. Tavallaan ne use caset on vähän niinku kirjoitettu sinne sisään valmiiksi, vähän ohjailevasti. Ehkä siitä. Ku tää.. Heti tää "They sometimes watch the same show while they are chatting" ni se kuulostaa silät et "noni, syöttä sinne, et tehkää siitä use case". Mä en tiedä osaaks näitä kattoo sillee niin.. Tiiäksä.. On vähän huonoja kokemuksia tai semmosta jotain.. [tauko] Mut kyl tästä niinkun semmosen.. Kyl täst alko muodostuu hahmo, siinä mielessä, et voi sit kelaa sita Marya oikeesti. Ja sen takia alko kysenalaitaaki et mikä, mikä sitte..

Oliks toi.. muistuttiks toi.. tai millä tavalla se eros, jos millään, niistä aijemmista persoonista mitä sä oot nähny? Oliks se samaa muottioa?

Aika lailla samaa joo. Tää oli aika tota noin niin.. Tää oli aika yksityiskohtanen tai sillaan, nimenomaan tähän. Tässä keskityttiin aika paljon sen.. sen.. Mikä varmaan on tarkotuskin, mut aika pitkälle viety toi TV, second screen. Nää tämmöset, että mitkä sitten ohjaa sitä ajattelua niinkun tiettyyn, mikä mun mielessä luo vähän sitä resistanssia. En tiedä oisko se parempi pitääkin ne semmosena geneerisenpänä. Et pysyis enemmän näissä, näissä niinkun pain pointeissa ja tavassa toimia. Miten muuten niinkun viettää iltaa. Niinkun vähän laajemmin sitä kontekstia, ku sitä että niinkun se nyt vaikka käyttää teletekstiä. Se alkaa jo lemaan semmonen jotenki... Aika yksityiskohtainen. Et tarvis varmaan olla sit oikeesti jotain dataakin siel taustalla. Että näin on oikeesti, että se käyttää niinkun... Sieltä ne just ristiriidat ainakin tulee. Ku tavallaan sä luot sen kuvan siitä tyypistä et se on semmonen. sillä on tietyt haasteet ja sillä [epäselvää] tehä ja sit siellä on se teknologian käyttö on vähän ristiriidassa sen kanssa mitä sä jo loit päässä ja siitä tulee semmonen "täh". Vaatis vähän niinkun sulatteluu. [tauko] et täälki on aika paljon niinku semmosta.. Sanosko sen, vähän ku semmosta noisee myöskin

Minkälaista niinkun.. hälyääntä..

Niin et se niinkun lähtökohta.. Kuvataan se tyyppi, niin tota, enemmän haluis niinku sen niinku semmosta et ei välttämättä puhttas niin paljoa sen teknologinakäytöstä niin yksityiskohtaisesti. [tauko] Se on vähän niinku sekä että, et se on sekä hyödyks että haitaks. [tauko] Mut kyl tän pohjalta helppo niinku keissejä on lähteä tekee, koska täs on hyvii syötttöjä. Ehkä se ongelma on se et ne syötöt vähän niinku "täh".. tulee vähän semmonen, jotenkin semmonen "ootteks te ihan varmoja?"

Niin sä mietit koko ajan et onks se niinku luotettavaa

Vähän joo. Mut se on ehkä niinku just.. Kun.. Et ku on semmonen jo valmiiks semmonen no niin taas tämmönen persoona, et heti haluu tietää, muutenkin on halunnut aina tietää, et mitä siel takana oikeesti on, mistä se data on kerätty ja mihin se perustuu et onks se vaan joku tuotepäällikkö heittänyt ideoita ilmaan, joka on sit ohjannut tämmöstä persoonaa. ET semmonen olos iit vähän tuli, et vähän epäileväinen.

Joo.  Mielenkiintoista. Mä otan tän nyt hetkeks pois, kunv ähän.. Tota jutellaan tosta et mitä sulla jäi mieleen niinku.. Haluuks sä kuvailla lyhyesti Marya omin sanoin?

No Mary on semmonen, joka tota noin niin.. Töissään erittäin tehokas ja luotettava ja semmonen niinkun todella kokenut tyyppi, joka tekee mitä lupaa ja siis todella ammattitaitoinen sitte, mutta sen työminä on hyvin erilainen ku sitten kotona. Kotona hän haluu.. kun hän just niinkun neuloo ja tällee, haluu rentoutua ja unohtaa sen semmosen.. Hän on johdon assistentti, sehän on niinkun aika jäätävän niinkun tiivistä asioiden kontrolloimista ja semmosta niinkun lankojen käsissä pitämistä ja pitkiä päiviä. Et vois kuvitella et hän on.. Kun hän tulee kotiin niin hän on aika väsynyt. Haluu vaan nostaa jalat ja alkaa kutoa. Sit siihen ei jotenkin yhdisty se et se alkaa näplää kaukosäädintä ja teletekstiä. Ehkä se antaa ohjat käsiin sit sen television katsomisen kanssa. Et jost varmaant ulee toi ristiriitakin. Et tavalaan sit jos se on enemmän semmosta.. TV on hänell enemmän semmonen taustajuttu tai sit jos hän haluu niin rauhassa kattoo miehensä kanssa jotain.. Se on vähemmän semmosta aktiivista käyttöä. Semmonen kuva mulle tuli siitä tyypistä.

Joo. Oliks jotain, niinkun, yllättäviä muut ku nää teknologia.. tietyt spesifit tekonologiat? Tai oliks ne mitkä siellä yllätti?'

Ehkä mua yllätti se et se on niin aktiivinen televisionk äyttäjä sit kuitenkin. Tai tosta kuvauksesta tulee semmonen, että joo hän teletekstiä ja kaukosäädintä ja sit vähän niinku tämmöäst. Et se alko vähän niinku luomaan siitä tyypistä semmosen. ET okei, mitä se sit kun se tulee kotiin. Sil on pitkä päivä takana ja mitä se niinku. Et sit alko se ristiriita tulee siitä et sitten se niinkun meni niin tekniseks se kuvaus siitä eteenpäin et mitä se kotona teee. Et enemmän vois kuvata sitä et mitä se niinkun laajemmin tykkää iltasin tehdä kotona. Et haluuks se niinku.. Tieksä että.. Kuinka väsyny se vaikka on ja mitä sillä liikkuu mielessä  ja haluuks se antaa ohjat toiselle vai olla niinku ihan tiiäksä ku se koordinoi koko päivän, pitkän päivän, haluuks se vaan olla rauhassa. Et semmosta. Jotenki ois.. Tavallaan odotti et se täydentyy sillä et se kertoo sitä tarinaa sitte mitä se on se kotona oleminen. Mikä priorisoi sit sen muita tekemisiä. Mut se loppu oli sit se.. Kotuosuus olikin kuvattu sit silleen et miten se käyttää kaukkaria. ET se oli vähän semmonen niinku ohkanen jotenki..

Sanoit etn iinku monesti etitään jotenkin tuttuja tyyppejä.. Oliks sulla joku tuttu tyyppi..?

Joo, tuli heti semmonen johdon assistentti, ihan oikee hahmo. Ei ihan viisikymppinen, mutta 40+ päälle selkeesti. Just ton ikäsiä lapsia. Pysty niinkun hakee semmosen ihan oikeen ihmisen siihen tueks. Tietämättä et mitä hän niinku kotona tekee, mutta siis tiedän mä sen mitä se sen työn luonne on. J akun hänen kanssaan monet niinku kahvikeskustelut käyny, että.. että ei se.. niinku jotenkin senkin kautta lähti sit hakee sitä niinku oisko hän nyt kiinnostunut kotona niinku tekee tämmösiä juttuja. Kun sitten lasten niinku dinnereitten tekeminen ja tämmönen niinku lapsille, niin jotenkin se.. Siitä muodostu semmonen, osittain senkin takia semmonen ristiriita mun päähän. Et se ei tuntunukaan enää niin. Se alko hyvin se tarina, mut sit se niinku alko.. Tiäksä kun tulee levyyn se särö? Vähän semmonen et niinku.. semmonen mun journey oli kun mä luin tota.

Ja mikä sul jäi sit päällimmäisenä ehkä mieleen sitten?

Tyypistä?

Niin

Siis ehkä päällimmäisenä juurikin se että se haluu niinkun relaksoitua ja niinku nauttia sitten kotona ja se telkkari on semmonen niinku rentoutumisen keino sen kutomisen ohella. Et tiedän näit tyyppejä myöskin ku kutoo, ja sehän on niinku semmosta.. Sä et näprää siinä muuta samalla, se telkkari on siellä taustalla. Et pysyty välillä kattoo just jotain sarjaa, rentoutuu. Mut et sinänsä kudot et sä et tosiaankaan tsättäää keneenkään kanssa just sillä hetkellä. Mut ehk ätommonen keissi vois sit olla, jos on niinkun.. ei oo ihan perus ilta et se mies on sit jossain matkoilla ja lapset on jossain. Sit se tavallaan korvaa sen et jos hän tykkää sitten puhelimessa roikkua paljon niin sitten joku tommonen vois ehkä mennä. Se vähän vastaa sitä. [tauko] Joo. Et sillee.

Sellanen. Joo. Miten.. tos ehkä nyt vähän sivuttiin, mut mitä mieltä sä olit tosta sun omasta konseptista, mitä sä tykkäsit siitä ite? niinku lopputuloksena

Niiku tää?

nii?

No siis täähän on niinku maailman ilmeisin use case. Semmonen vähän et tehään nyt toi perus vaik se ois.. Kai se ois toi sitten, kun mä yritin välttää nyt sitä kaukosäädintä silleen, et jos hän niinkun perinteisesti oo niinkun teknologia.. [epäselvää] tää ei ehkä ensisijaiset oo se mitä haluu käyttää et hänel on se ipadi mikä on sitte kädessä iltasin. Joo.

Hyvä. Tota, täs on pari muuta persoonaa. Jos sä haluisit lukasta nää läpi. Ei tartte yhtä tarkkaan lukee, mut sillai kattoa, lukea läoi ne. Jutellaan sit niistä vähäsen myös. Ja täs on myös Mary jos sä haluut vertaa siihen jotenkin.

Okei.

[Haastateltava lukee persoonakuvauksia]

Täällä on taas niinku use caseja täällä persoonan seassa. Se on vähän mikä häiritsee. Sä yrität niinku luoda siitä tyypistä niinku.. Saaks kommentoida?

Joo tottakai

Sä yrität luoda siitä tyypistä kuvan et mikä tää on ja mikä sillä tikittää, niin mä en ehkä haluis lukea näin yksityiskohtasia.. Näist tulee se olo, et nyt on tungettu use case persoonaan.

[Haastateltava lukee persoonakuvauksia]

Sama tää että et nää tyypit et ne tosi harvoin todennkösesti ees kattoo TV:tä. Sit tääl on TV on in the background, et heti tuli semmonen että...

[Haastateltava lukee persoonakuvauksia]

No tää, tää Susie almost never watches television, tääl tää on sit taustalla päällä et heti semmonen niinku ristiriita.

[Haastateltava lukee persoonakuvauksia]

Edelleenkin jää vähän niinku... haluis sen jotenkin pitää sen geneerisemmän tasolla sen itse persoonana, että kuvais sitä tarkemmin että mitä se niinku miten sen päivä menee ilman et puhutaan et lähettääks se sen whatsapp viesitn ku se kattoo jotain telkkaria. Se detaljin taso on vähän.. Se pomppii. Sit on vaikee... Se häiritsee niinkun persoonan.. Sä saat sen tyypin päähäs. Se häiritsee vähän sitä.

Millä tavalla?

Ku se menee liian tota noin.. Mä haluisin niinku tietää et mitä se ylipäätään niinku.. Ylipäätään niinku suhtautuu asioihin asioihin ja viettää vapaa-aikaansa tai mitkä asiat on sille tärkeitä. Ku se yhtäkkiä pomppii, niinku tässäkin toisessa kappaleessa, ni sitte se näkee tipin se lähettää viestin whatsappiryhmään. Se detaljitaso on niinku.. Sit on vaikee kii siitä tyypistä, ku se kertoo enemmänkin et koska se lähettää sen.. Noin mikro hetkisen siinä. Enemmänkin ois kiva niinku kuulla niinkun ylipäätään.. Nähdä sen ylipäätään päivän, et joku on piirtäny sen auki ilman et se välttämättä tunkee sinne yhtään mitään niinku keissiä. Et tääl on.. Ne use caset on kirjotettu sisään tähän persoonaan, mistä mä en tykkää.

[Haastateltava lukee persoonakuvauksia]

Ja se myöskin rajaa sitä ajattelua siitä, että sä rupeet miettimään niitä keissejä sen olemassaolevan infran ympärille. Sen sijaan et rupeisit niinku miettimään et mitä tää ihminien oikeesti haluaa tulevaisuudessa. Et se rajaa ehkä miettii liikaa sitä nykyhetkeä, mitä se just niinku tekee. Et ehkä vähän geneerisemmällä tasolla ylipäätään, ja sit tässäkin tää ristiriita tän telkkarin kanssa. Et nä ätulee heti.. Sä alat luomaan sellasta kuvaa, et nää on niit tyyppejä, jotka ei keskimäärin hirveesti kato telkkaria, vaan et niil on ihan muut mediat käytössä. ni sit se heti alkaa säätää. SE uskottavuus kärsii saman tein kun tulee se telkkari on siel taustalla auki, mikä kuulostaa taas siltä et se tuotepäällikkö on kirjottanu et okei sinne sisään. Semmonen fiilis siit tulee.

[Haastateltava lukee persoonakuvauksia]

Täällä taas niinku että.. Dannnyllä on niinku busy life. Ja se kokee, et sil ei oo riittävästi vapaa-aikaa ja sillä on perhe, suht pienii lapsia. Ja sit se mihin se haluu sitä aikaansa käyttää on kattoa telkkarista dokumenttejä. Ni tulee smmonen, et no niin et taas tänne on tungettu se Tv sen elämään. Ni välttämättä niinku heti.. Niinkun heti istu. ET jos sul on vähän aikaa ni yleensä se mikä ihmisiä harmittaa ettei oo riittävästi aikaa perheelle tai omiin harrastuksiin tai omaa aikaa tehä niinkun lastensa kanssa, ni tuli epäuskottavuus et miks se telkkari on nyt siel taas.. Et jotenkin se on niinkun väkisin tungettu näihin kaikkiin se tv. Aika vahvoja oletuksia ainakin.

[Haastateltava lukee persoonakuvauksia]

Just et TV:n katselua.. TV:n katselua häiritsee hänen tyttärensä ja heidän tarpensa ni se.. Mitkäs elämän prioriteetit onkaan.

[Haastateltava lukee persoonakuvauksia]

Tää on muuten ihan uskottava tää skills and knowledge tyyppiset jutut et..

[Haastateltava lukee persoonakuvauksia]

Ni sit täs taas tää second scriin, et jos on kiireinen ihminen, projektimanageri, ni todennäkösesti ku se istuu siin sohvallaa sil ei tosiaankaan. Se ei online shoppaile vaan se tekee töitä. En mä tiiä. Mä oon jotenkin liian skeptinen, jotenkin semmonen et nää ei oo uskottavia nää tyypit. Tai ehkä se tulee siitä, että ku se tää on niin tää TV TV TV TV TV TV TV... Ku eihän se.. Et tavallaan tää on liikaa tää persoonassa kuuluu se TV.

sä tykkäisit sen asian niinkun enemmän yleisellä tasolla

Ehdottomasti joo. Ku eihän persoona oo tavallaan se... Se on tavallaan siitä seuraava steppi kun miettii siitä että no okei, tässä on nytten tää meiän tää persoona. Mites se sitten.. Millanen TV on sen elämässä. En mä haluis nähä sitä kun mä vasta luon kuvaa siitä tyypistä, ni halua et mua pakotetaan ajattelemaan sen TV:n käyttöä siinä vaiheesssa. Mä ymmärrän ensin sen kokonaisukuvan siitä tyypistä, mihin se tikittää, mitkä ajankäytön haasteet sillä on , miten se haluu käyttää aikaansa.. Millä asenteella se on sen himassa sohvalla, ku TV on läsnä. Ja sitten.. Vast sitten alkaa miettii sitä, niitä keissejä. TV use keissit, ne on kirjotettu tänne persoonaan, se on häiritsevää.

Oliks niis jotain eroja nois kolmessa persoonassa? Niinku...

Oli toki joo. Nää on nää.. Niinku. Siis tarkotaksä niinkun laadullissesti?

Laadullisesti tai jotenkin niinku.. Nousko jotenkin muuta ku nää ilmeiset henkilön erot?

Emmä tiiä, must nää noudatti aika samaa kaavaa kaikki, et siis niinku persoonakuvaukset. Et ensin niinku lyhyt kuvaus et kuka se on ja sit alettiin puhuu TV:n käytöstä. Et ei näistä ainakaan tullu semmonen heti et tää on tehty oi.. täs on oikein tehty, täs on niinku keksitty.. Sellast ei tullu. Mut must nää on aika samaa niinku.. Nyt mä lukenu ehkä sillä pieteetillä et mä oisin semmosia sävyeroja oikeen ettiny

Ei se ehkä ollut oikeen tarkotustkaan..

Mut se yleiskuva näis kaikissa oli aika sava, et miten se tökkii se telkkari siel niin vahvasti osana sitä persoonaa.

Jos sun pitäs laittaa noi johonkin uskottavuusjärjestykseen, niin miten sä laittasit ne?

Emmä osaa sanoa. Emmä laittas.. Emmä pysty laittaa. Nää kaikki.. Koska näissä kaikissa häiritsi se.. Ku nää kaikki juu, nää kaikki pystys olee ihan oikeitakin tyyppejä, mutta tota.. Enkä, ei se oo se juttu välttämättä, mutta.. Niitä lukiessa oli niinkun vaikee saada se tyyppi päähän ku se ähti niin paljon sinne.. Se häiritsi sen kuvan muodostumista. Ehkä jos mä en ois.. oisin mä kyl varmaan, jos ois puhunu etukäteen et tää on TV-keissi vaan me tehään persoonat. Oisinks mä ehkä kattonu tätä eri silmin, mut toisaalta kun tääl on tää TV tulee niin vahvasti. Sä heti toisesta lauseesta arvaat et mikä täs niinku.. Et mitä varten tää on tehyt. Ja mun mielestä persoonakuvauksessa ei sais olla sitä et sul tulee semmonen fiilis et mitä varten se on tehty

mitä tarkotat et mitä varten?

Et niinku tv:tä.

Persoonassa oikeesti semmonen niinku... Et sit et niinku tavallaan luodaan se skenaario siitä tyypistä, sithän se saa olla niinkun tv tv tv. Ni ehkä vähän niinku liian yksityiskohtaiseksi use case kuvaukseks nää..

Joo. Tota tota. Mitä dataa sit luulet et tuol on ollu taustalla? tai minkälaista dataa?

Vaikee sanoo. Siis voi olla ihan oikeetakin tyyppejä ja oikeita keissejä ja ihan oikeesti oikeita hahmoja, mut se et miten ne on kirjoitettu auki tohon ja mitä niistä on korostettu.. Ni tota se on niinkun ehkä se mikä siinä häirittee, ettei.. Kaikissa noissa tuli semmonen tietynlainen resistanssi, mikä on ehkä enemmän sitten oma ongelma ja johtuu siitä että on niinku huonoja kokemuksia. Tai semmonen ennakkoasenne jo vähäsen. Et mä en tiedä pystyynks tätä niin sillee puhtaasti siitä miettiikään.

Pystyisikäs sä käyttää noita tota niinku.. Tai saisiks sä noista hyötyä oman työn suunnittelutyön ohessa?

Joo, mut ne pitäs sitten vieedä tosta pidemmälle. Et pitäs oikeesti kuvata se sen tyypin tota noin niin.. Siis tyypillinen päivä, nostamatta niinkun niitä use case esierkkejä niinku tälleen tarjottimelle. Vaan oikeesti kattoo se sellaneen mediaanipäivä ja kattoo niitä ilman että ne use caset on sinne väkisin työnnetty.

Mites luulet et toi taustadata on vaikuttanu sitte noitten persoonien muodost.. Tai luomiseen? Jos siin ei oo sitä suoraa käyttäjädataa ollu. Tai että käyttäjätutkimusta siis. Tai siis toissijainen..

Ehkä ne jää just vähän tommosiks use case.. Tavllaan sit korostoo siinä kuvauksessa se, et mä en tiedä et onks tässäkin persoonan kirjottajalla ollu jo mielessä mitä varten näitä tehdään. Ni se heijastuu sit siellä. Et jos ihan puhtaaksi lähdettäs tekee tutkimusta ja kirjotettas jonkun ihmisen niinku päivä, ni mäen usko et se sisältäs näin niinku yksityiskohtasia kuvauksia suhteessa siitä telkkarin käytöstä. ehkä. Et ehkä siinä voi ehkä aistii semmosen.. Et se ei oon iinku puhdas persoona vaan se on enemmänki semmonen.. Joku tietty juttu mielssä tehty joku. Yritetty kaivaa et mikäs tällä ihmisellä vois olla se mitä se tekee sohvalla: okei no se vois tehä.. Et kyl siel voi olla jotain dataa taustalla et se on jotenkin korostettu, et samaan tyyppiin sit ängetty hirveesti kaikkee. Et..

Joo, tota.. Mulla on kysymykset tässä. Onks sulla jotain kysymyksiä tai kommentteja tähän aiheeseen liittyen tai jotain mitä jota..

no ny tois kiva tietää,et onks joku näis. Tai mikä näitten teossa on ollu se ero. Et danny, susie ja mary. Et onks ne kerätty jotenki eri lailla?

Joo. Niis itseasiassa on kerätty samallalailla data, mutta siellä on kerätty tämmäsiää. Tää on niinku ei enää osa tätä haastattelua. Niin tota kerätty tutkimuksia TV:n käytöstä, jossa on sitten tehty jonkunnäkösiä ryhmittelyitä, jossa nää ryhmittelyt on affinitymapping eli post-it-leikillä saatu akselistolla saatu kohtaamaan toisiaan ja sit näis on vähän eri määrä ja vähän erilaisia tutkimuksia taustalla. Ne erot on aika pieniä kyl. Susiessa tais olla niinkun vähiten niinkun eri tutkimuksia taustalla. Ja sit taas toi Mary oli tällänen niin mediaanikäyttäjä, et sille sit taas oli paljon.. paljonkin erilaisia juttuja. [tauko] Mikä fiilis jäi?

No se nyt on aika monta kertaa sanottu, että on se, että.. Se yleinen semmonen, että.. Vähän semmonen resistanssi, et eihän se.. et mitä ihmettä. Semmosia niinku ristiriitasuuksia. Mikä on itseasiassa tosi tärkeetä että kun onse persoona et jokanen löytää tavallaan sieltä sen niinku pystyy sama.. tavlaaan niinku ymmärtää sitä. Mä en usko et se ymmärrys tulee sen kautta, että sä tiiät miten se niinkun.. Tai siis jotenkin vähän niinkun kenelle se.. enemmänkin se tunne semmonen asenne, motivaatiotasolla käsittää se tyyppi.. Kun että otetaan ne.. Että on semmonen ehyt tarina, jonka ihmiset ostaa. Et jos on yksikin semmonen [zzzt], tiiäksä semmonen, tiiäksä levy särähtää. Sä alat mudoostaa jo sitä kuvaa, et se pitää ainakin sit hyvin perustella. Et ei tuu semmonen pakkosyöttö fiilis, että noku piti vaan kirjottaa tänne tää, et me saadaan tehtyy tästä nää konseptit

Joo. Luuleks sä et siin auttas että siin näkis että mistä noi on tullut noi mikäkin kohta?

Voi olla, et jollain tapaa, auttas, mut tavallaan jos se tarina menee rikki ni sit se on rikki. Et en mä tiiä, et vaikka kuinka selittelee niin tota auttaaks se. Et ehkä sen takia vähän vaarallistakin nois persoonissa mennä liikaa sinne, kertoo niinkun yksityiskohtasesti mitä ne.. Niinku noin yksityiskohtaisesti et mitä ne tekee, sillai korostetusti kertoo just siitä. Ku sit se tavallaan se suhde pitäs olla et sä .. Et sä kuitenkin ymmärrät enemmän sen, sitä yleis tyyppiä, saat sen kuvan muodostettua päähäs. Et joku niinkun.. Me ollaan.. Siis mä oon paljon nähny niikun videoita, jossa oikeet henkilöt, oikeet hahmot kertoo elämästään. On ihan siellä omassa kodissaan selittää, et miten ne tkee. Ni se on niinku super paljon jotenki semmosta arvokkaampaan. Saat ihan erilaisen tyypin päähäs ku se et joku kirjottaa niinkun itekseen. Et ehkä semmoset niinku videot oikeesta ihmisistä on parempia sit kuitenkin. Tavallaan siit tietysti kohderyhmästä. Jos sä näät ihan konkreettisesti et miten ne asioista puhuu. Se on tosi tärkeetä, miten ne asioista puhuu, millä sävyllä itse selittää juttuja. Ni se on niinku pajon arvokkaamapa ku semmonen generoitu. Nää on kaikki.. Siis se mikä näistä kaikista tuli et nää on generoitu, nää on persoonia, nää ei oo oikeita tyyppejä.

Itseasiassa unohtu kysyy, et oliks tossa Maryssa jotain mitä siitä selvästi puuttuu? Et niinku tää tarina menee niinku rikki, mut olisk jotain puutteellisia asioita?

No Mary oli kyl silleen aika hyvin kuvattu et se niinku.. Sen yleisesti arkee et miten se menee ja mitä se tykkää tehdä ja.. Se oli ehkä niinku, sil oli ehkä helppo päästä siihen tyyppiin sisään. Mut tota mitä siitä puuttu.. Ehkä siitä puuttu sit se, että.. No olihan tossa toi se, että se haluaa. Niinku.. Media use goals, relaxation, comfort, mut sehän on niinku ylipäätään et ku se on himassa et mihin se pyrkii sit ku se on kotona. Et miettii et mitä se niinku haluuun, et millasen tunnelman se sinne haluu. Haluuks se olla hirveen aktiivsesti tehä jotain vai haluuks se vaan olla. Et täs käsitellään nyt sit enemmänkin sen media käyttöä, minkä kyllä ymmärtää ku tää on sitä varten tehyt, mut sit että sä pääsisit siihen tiettyyn tyyppiin kiinni ni vois auttaa se et mitä se ylipäätään priorisoi himassa. Ja et paljon se vaikka sitte.. Toi tuli vähän yllätyksenä nurkan takaa toi kavereitten, et kavereitten kaa paljon puhelimessa. Et se ois ollu kiva kuulla niinkun erillään siitä tv:stä, et se on paljon kavereiden kanssa puhelimessa. Et se puhelimessa olo ei liity siihen et se kattoo telkkaria

Tulkitsiksä sä sen tsättäilyn just tollaseks niinku teksti tsättäilyks?

Joo.

Okei..

Ei ei, mä aattelin et se on niinku näin, luurissa niinkun juttelee kavereitten kanssa. Se tekee sitä todennäkösesti. Se ei.. Mä en kuvitellu et se TV on se juttu. Se on sen takia puhelimessa, et se kattoo TV:stä jotain sarjaa. Et se ylipäätään haluu jutella kaverinsa kaa kuulumiset ja TV sattuu sit olee siin taustalla. Et täähän on sit semmonen, et se vaatis et se on aktiivisesti.. ET se sen takia, et nyt tulee toi sarja et mäs oitan tolle tyypille. et ese oli vähän semmonen et.. Soittaaks se sen sarjan takia vai soittaak sse vähän muutenkin vaan kavereilleen. Se ois hyvä ymmärtää siin tyypissä, et tykkääks se olla paljon puhelimessa just kaverinsa kanssa. Ja et mitä ne sillon.. Tyypillisestihän ihminen jos se haluu jutella kaverinsa kanssa, ni menee pois jonneki syrjempään, varsinkin jos nää muut on kotona ja kattoo telkkaria. Sit jos sillä on niinku sellanen aika, jolloin se tyyppi, telkkari.. Sit se ovis olla sitten siinä telkkarin ääressä. Harvoin kukaa.. jos joku kattoo telkkaria jat oinen ottaa luurin se on häritsevää puhuu, se on häiritsevää. Niinku semmosia.. ET tavallaan, et jos se tossa kertoo vaikka Marysta et nyt se haluu vaikka kerran viikossa vähintään soittaa parhaalle kaverilleen. Tai että jutteleeks se joku ilta niitten kanssa, niinku kuinka paljon se ylipäätään on puhelimsessa kaverinsa kanssa. Et miten se niitten kanssa kommunikoi ja missä se sen puhelun ottaa, et onks se aina sohvalla telkkarin ääressä. Ni sit siit tuli semmone et.. Se jäi vähän vajaaks. Et ylipäätään se kommunikointityyli ois hyvä ymmärtää. Nyt se on vaan nii kapee ala. Kapee siitä.

Siis noihan on yhteen asiaan tehty nää persoonan. tai niinku yhteen kontekstiin.

Mikä si tmyöskin on varmaan tavallaan tehokasta, mut se on häiritsevää sen persoonan luomisen kokonaiskuvan kannalta

Tos on itseasiassa niinku.. Mun ymmärrys siitä mitä kirjallisuudessa oon puhuttu on se et nimemonaan se et persoona pitäs luoda siihen yhteen kontekstiin ja välttää sitä geneerisyyttä. Ei nyt mahollisimman paljon, et se ei niinkun jää semmoseks ylätason kaveriks.

Kyllä, mut tavallaan, et jos miettii mistä asiakasymmärrys tai ihmisymmärrys, se ei tuu.. Se niinku.. SE on vaan kapee. Jos sä saat sen kapeen sektorin, ni mä en tiedä.. Mä luulen et suunnittelussa auttas kuitenkin enemmän et sä ymmärrät sen tyypin ja pystyn niinnku7 ymmrtää sen eri sektoreita sit sitä kautta. Et jotenkin jää semmonen et täs on tää Mary, joo. mut sit sä näät siitä vaan sellasen pienen palasen, ni se häiritsee. Koska sit se on niinku vaikee miettii sen boksin ulkopuolella, et mitä sen elämässä on kaikkee muuta ja mitä tuol ois mahkuja muuta ku se et sä katot vaan sen tietyn et miten nyt asiat on.

Tota pitää kyl pohtia.

Et mä tiedän et nää persoonat on semmosii mitkä niinkun jakaa ihmisiä tosi paljon. et jotkut pitää niitä tosi hyvinä työkaluina, jotkut sit taas vihaa niitä. Ja ei niinkun ythään usko niihin. Tai sit pitää löytää niihin joku semmonen itselle toimiva tapa.

Ooks sä enemmän ttä vihaa ryhmää?

En mä vihaa, mut siis ku on siis jotenkin vähän skeptinen. Skeptinen, joo.

Jännä nähä, mitä tänään. Sit ku koko haastattelu pala.. saanu tehtyy, et mitä tääl nousee. Sit täs on vielä aika niinkun vaihtelevaa porukkaa. Et on porukkaa jotka on just ja just käyttäny persoonia ja sit on sit niinkun sinä ja suvi jotka on käyttänyt paljonkin persoonia tai ainakin nähnyt

Tai ainakin nähny sitä tekemistä..

Suvi vissiin, mä en tiedä onks se käyttänyt mut ainakin käyttänyt niinkun paljon. Et se on jännä nähä siinäkin mielipiteitä, et miten ne sitten eroaa.

Jo mä luulen et täs on aikapaljon just mielipdie eroja. Et ei oo semmosta, et joku study sanoo jotain ja yleinen käsitys on jotain. Et sit enemmän määrittä se et miten sä ite koet sen hyödylliseks ja millasii kokemuksii sulla on

Siinähän tavallaan, se onkin osa tätä niinku selvitystä et miten se niinku.. Persoonien käyttö suhtautuu omiin aiempiin kokemuksiin

Niinpä. Mut kyl mä silti edelleen, vaik mä oon skeptinen, ni jotenkin se.. Se on tosi tärkeetä ku sitä asiakasymämrystä, ihmisymärrystä luodaan, ni se on hyvä et sitä jollain tavalla dokumentoidaan, jalostaa. mut siin pitää sit oikeesti olla tarkka ja miettii et miten sen just siihen keissiin tekee. Että, et.. Semmosta yht.. Mä en usko että on semmosta yhtä formulaa, mikkä niiku. Meilläkin on se castin ja siinäki on ne kortit ni.. Ku mä neki luen, vaik kuin monta kertaa tankaaa, ni en mä pysty niit sellasenaan käyttää. Että ne pitäs niinku miettii, miettii oikeesti sitten jalostaa eteenpäin. Ja saada ensin se tyyppi sisään, pään sisään, muutaki ku sen työn kautta tai sen teknologiakäytön kautta ja.. Sit lähtee siitä niinkun kattoo sen tyypin eri sektoreita. Ni se vois olla sit.. Mikä mulle toimis. Sillon ei tuu se alun reisstanssi, tiiäksä. Jos sä heti niinku syötät sinne sen yhen sektorin, ni sillon se on semmonen [pfft].. Et ootteks te tosissanne, et tän elämä on tätä

ET se ei kerro koko kuvaa kuitenkaan. Joo, tää on kyl hauska ja siis harvoin saa näin tota perusteellista palautetta omasta tekemisestään, kun tota jos... [nauha loppuu]