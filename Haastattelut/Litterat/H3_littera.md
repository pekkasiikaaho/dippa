[nauha alkaa] ...recording.. Tsek.. Niin. Mennään rennosti jutusteluhengessä, mennään.

Mistä on kyse?

Mistä on kyse? Dipasta. Eli tehään niinku mun dippaan liittyen haastatteluita ja mun dippa liittyy persoonien rakentamiseen sekundäärisen datan pohjalta.

Okei

Eli jos ei oo suoraa kosketusta käyttäjiin, niin mitä sitten voi tehä. Ja tässä sitten haastattelussa käsitellään vähän niinku kokemusta persoonista ja suhtautumista persooniin ja sitten päästään myös tekee pieni suunnittelutehtävä persoonien avulla. Ja siihen on erityisen tärkeetä muistaa et siinä ei arvioida suunnittelijaa eikä sitä lopputuotosta. Et se ei oo se pointti, et ole tässä testavana. Ja mites tästä eteenpäin nää sitten litteroidaan ja teen sellasen laadullisen analyysin tästä ja katotaan mitä löytyy. Kuus haastattelua on yhteensä. Kaikki Leadiniläisiä. Joo, siinä niinku taustoja multa. Onks sulla täs vaiheessa kysyttävää tai..?

Onks tää luottamuksellisia tietoja, mihin nää niikun käytetään.

On luottamuksellisia, käytetään mun diplomityöhön ja diplomityö on sitten julkinen. Litteran ja muut ei tuu liitteiksi eli voi kiroilla jos kirjoiluttaa. Et tota kyl näis sitte niinku kooste rapsa tulee firmallekin, mut ei tuu sillai et Janne oli ihan paska. Et nyt mä joudun tuoltaki ton litteroimiaan. Joo. Mut ei siis tosiaan mitään.. Voi huoletta kertoa huolia murheita

Turhia toiveita

No mulle on huono esittää toiveita ku mä en mihinkään vaikuta muutaku omaan dippaani

Onks tää siis sun dippa, onks täs toimeksantajana leadin vai?

Joo. Tässä taustana on niinkun MPAT projekti, jossa ollaan niinku TV:n tulevaisuuden standardin työkaluissa, niin niitä kehittämässä. Ja sit nää on sit siihen TV yhteyteen kehitetty näitä persoonia. Ja mä teen siinä dipan sitten ohessa, kyljessä.

Tämä selvä. Ei muuta ku shoot.

Shoot.. Niin ja tosiaan nää tunnit voi sitten laskuttaa siltä proejktilta

MPATilta

Mpatilta. se on sulla avattu ja WP3 on sulla kohde.. Hyvä. Käyntiin. Kerro vähän itestäs, tällai klassinen alotus. Kuka oot, mitä teet?

Mä oon Janne ja mä oon aalto-yliopistosta muotoilun maisteriopiskelija, international design business management-ohjelmasta, elikkä opiskelen ehkä muotoilun liiketoimintaa ehkä enimmäkseen. Taustana teollisen muotoilun koulutus metropolian ammattikorkeakouluusta, valmistunut 2014. Ja mut on käytännössä siis koulutettu tuotesuunnittelijaks. Tuotesuunnittelijaks et mä olen niiku koulutuksen perusteelta, oma harrastuneisuus on sit ehkä vieny enemmän niinku käytettävyyttä ja tota visuaalista puolta eritoten sit siitä fokuksena sieltä kautta. Keskisuomalainen. Käynyt intini keuruuna ja näitä asioita, mut ne nyt ei oo ehkä tässä olennaisia sinänsä, mutta.. Leadinilla aloittanut tässä hiljattain. Oonko itseasiassa viidettä viikkoa tällä hetkellä töissä.

Tota.. Minkäverran sä oot tehny hommia sitten niiku käyttäjälähtöisessä suunnittelussa tai tehnyt käyttäjälhtöistä suunnittelutyötä niiku muuten?

Käyttäjälähtöistä suunnittelua mulla n kokemusta ehkä paristakin toimistosta. Fokus on ehkä ollu - vaikka ollu käyttäjälähtöistä - ni se on sit kuitenkin olllu brändi tai business vetoista. Että vois sanoa käyttäjälähtöistä suunnittelua on kaikki suunnittelutyökokemus. Mä en oikeen tiiä onks olemassa muunlaista suunnittelua ku käyttäjälähtöistä suunnittelua jos puhutaan teollisen muotoilun koulutuksesta, siitä haarasta. Vois sanoo siitä et kaiki on oikeestaanse mitä siihen liitännäistä on tehny, ni on liittyny jotllakin tapaa käyttäjäkokemukseen ja niiku loppukäyttäjän mielipiteisiin ainakin

Kuinka paljon sä oot suurinpiirtein tehny sitten?

No toimistoduunia mulla on.. Sanotaan, että puoltoista vuotta, siinä paikkeilla. Mut siihen mä en laske sit näitä erinäisiä projekteja, mitkä osittain menny myös samaan aikaan. Samaan aikaan.. Ja tota et ne on ollu enemmänkin pitkälle tämmösiä freelance kautta kouluprojekteja kautta puoleks virallisia puoliks kouluhommia. Niinku tiedät et tää kuvio menee. Ja tota, et yhteensä varmaan kaks ja puoli vuotta, kolme vuotta on semmosta alan kokemusta.

Alright. JA mitä.. Niin sano vaan.

Ja liittyen ihan sitten ihan tällä hetkellä toimin myös sivutoimisena tuntiopettajana metropoliassa vanhassa koulussa ja opetan siellä sitten tota visualisointiohjelmia ja digivisuhommia käydään siellä vahvasti ux painotuksella.

Mikä sul on nykynen työtehtävä?

Nykynen työtehtävä on.. Mä oon nykyään Leadinillä UX designer tittelillä ja tällä hetkellä pitää sisällä aika paljon tota aika paljon alkupään tutkimusta ja  niinku konseptinvalidointeja ja... Ja tän tyyppisiä projekteja. Käytännössä yhdellä asiakkaalla tähän menenssä tehny niitä, toivomusten mukaan myös niinkun.. Myöskin niin että fokus jatkuu niiku tämmösenä yksi asiakkaisena projektina, et tästä eteenpäin... Toivotaan näin. Joo, haastatteluja pitkälti tehnyt, haastatteluja. Muistiinpanoja ottanut ja tota mitä siihen nyt liittyviä suunnittelutehtäviä ollu. Ja välillä vähän sit visuaalisempaakin toimintaa sitten hätäapulaisena.

Joo. Entä sitten persoonakuvauksii. Ooks sä käyttänyt niitä sitten käytännössä?

Persoonakuvauksia on.. Oon käyttänyt. Ne on niinku opetettu tosi varhaisessa viaheessa muotoiluopintoja. Persoonien tekemistä, mutta se mikä on ehkä jääny siinä vähän hämäräks et missä... Minkälaisissa tilanteissa niitä niiku oikeesti pitä skäyttää. ET se on ehkä menny noissa koulutushommissa vähän semmosiin... Kouluhommissa siis että tehään nää persoonat kosak persoonia on kviva tehä ja sit niitä ei oo oikeen yhestetty ehkä siihen tekemiseen ja siihen käytännön tekemis.. Ne on monesti unohtunu siitä matkan varrelta, mikä usein näkyy sitten siinä.. Tai johtuu siitä, että taustatyötä ei oo tehty tarpeeks taikka siihen ei oo varattu tarpeeks aikaa tai sitte että tota ne persoonat sit on niinku kehityetty vaan niiku kehittämisen halusta. Niit ei oo käytetty työkaluna. Mutta suunnittelutöissä aika vähällä ollu, jos puhutaan tämmösten niinkun mainostoimistomaisista duunesita ja palvelumuotoiluprojekteissa missä on ollu niin sinne niiikun.. Persoonan käyttö. Niinku että, kuvitteellisen persoonan käyttö on aika vähän et ne on ollu yleensä esimerkkikäyttäjiä ehkä, joiden kanssa on suunniteltu

Ni se on sit ollu joku yksi oikea hnekilö jonka kanssa on tehty

Joo, näin käytännössä. Ja ne on myös ollu joko käyttäjiä tai päätöksentekijöitä, mikä on vähän niiku.. Siitä välistä tippuua aika paljon ihmisiä pois tietysti.

Niin... Ni sä et oo töissä sit käyttäny persoonakuvauksia juurikaan?

No siinä mielessä että jos niitä on käyttäny ni ne on tullu avlmiina asiakkaalta, että tässä on nää meidän määrittämät persoonat taikka target groupit, jotka on myöskin niitä.. Millon nimetään. Ja sit niitten pohjalle on suunniteltu. Et se on tavallaan helpottanut siinämielessä et ne on tavallaan jo puoliks suunnitelut ja mun mielestä siinä vaiheessa ku määärittää.. määrittelyt on katettu et mitä se pitää sisällään. Mutta varsinaisesti siihen.. Se ei oo kuulunut mun suunnittelutehtäviin tehdä niitä persoonia.

Onks siitä sit ollu apua ku sä oot käyttäny niitä valmiita persoonia?

On. Siitä on siis tosi paljon hyötyä siinä mielessä, että tavallaan tuntuu että se suunnittelu tarvii alottaa tutkimuksella, että se on tavallaan tehty sit siihen kohtaan. Ja ajos niinku persoonat on tarpeeks kattavia ja kuvaavia ni mun mielestä se helpottaa sitä suunnittelun aloittamista tosi paljon, et ainakin nopeuttaa jos ei muuta. Et se tavallaan taustatyönä tosi tärkee, siinä mielessä, että tiettää sen niinku käyttäjään tosi hyvin ja voi sit itekki keskittyä sitten siihen luovaan työhön siihen. Siihen et ei tarvii niinkään keskittyä miettimään et toliks tää nyt jonkun mutooin vielä et miksi me tehdään tätä. Vaan aina vastaukset löytyy suoraan seinältä.

Kuinka monta projektia sul on tullu vastaan, arviolta..?

Sanotaan.. Jos enemmän tai vähemmän aktiivisia tämmösiä niiku persoonakuvauksen käyttökohteita ni.. Oisko puolen kymmentä.

Joo. Onks sul sittei ollu joku projekti jossa se ois ollu epäonnistunut kokemus tai että ois huonoja kokemuksia persoonien käytöstä?

On sitte semmonen yks tapaus tulee mieleen, jossa oli tämmönen innovaatioprojekti ja tota siinä oli persoonat suunniteltu lähtökohtasesti sen perusteella että mikä oli vanhentunutta dataa. Että tavallaan persoonat oli vuodelta 2014, tää porjektikin oli, mutta et se oli aikasemmin perustettu tai tavallaan kehitetty nää eprsoonat. Mut sitte myöhemmin sitten vasta selvis siinä vaiheessa ku suunnittelu oli jo pitkällä, suunnitteltyö oli pitkällä, et data olikin niiku monta vuotta aikasempaa. Et tavallaan ne persoonat mitä siinä oli määritelty todellata rakasta että niitä ei ollu ees ollu olemassa moneen vuoteen. Et se muuttu se briiffi siinä matkan varrella, mikä taas sotki kyllä kommunikaatiota ja tota lopputulokseen vaikutti varmasti. Söit niiku turhanpäivästä aikaa sen takia että persoonat piti yhtäkkiä sitten tavallaan päivittää. Päivittää kesken kaiken

Teitteks te uudet persoonat vai päiviteitte vaan..

Me päivitettiin niitä vanhoja.. Siinä sitten. Ei siinä suoranaisesti tullu mitään niinkun suurempia mitkä ois vaikuttani siihen, siihen niiku lopputulokseen, utta joktaun muutoksia jouduttiin tekemään sen takia että alkubriiffi tavallaan oli väärin tehty.

Vanhentunut..

Vanhentunut.

Millon sä oot viimeks käyttänyt persoona.. persoonaa apuna?

Tota.. Varmasti.. Ei siitä montaa viikkoo voi olla. Kyllähän se tossa niiku edellisessä.. Itseasissa tää oli tossa niiku mun meneillä olevaan kurssiin liittyen, siinä käytettiin persoonia, jotka viel aikasemmin luotu, ni käytettiin tavallaan niistä tiedon keräämiseen. Et niinkun koulutehtävänä.

Mikä sul sit on yleinen fiilis persoonista työkaluna?

No sellanen.. Sellanen fiilis, että monesti niiku siinä tekohetkellä tuntu et persoonien luonti on jotenkin niiku ajanhukkaa ja se ehkä niiku tulee niinku vähän semmosena empaattisenäa ajatteluna siinä mielessä, et jos tekee monialaisessa tiimissä et sitä tavallaan varsinkaan buisnesspuolen tyypit tai välttämättä insinöörit, ku ne ei oo muotoilijota, si tne nelposti ajattelee sen helposti et miks näitä tehään et tää on ihan leikkimistä tai pelleilyä, ni se tavallaan tarttuu se ajatusmaila siitä vähän itseenki. Mut sit jälkikäteen on oikeestaan joka kerta huomaa, et ne oli ihan sika hyödyllisiä. Hyödyllisiä suunnittelulähtökohtaa, koska.. Projektit on usein laajoja ja pitkiä ja niinku data häviää sieltä matkan varrelta. Persoonat jotenkin paljon helpommin muistaa, et nää mitä.. Mitä iteki omissa isommissa projekteissa huomannu ni.. Onhan se niiku käytettävyyden kannalta todella tärkeetä et sielät niiku pysyy kaikki semmoset detaljit jotka on oikeesti tärkeetä. Tärkeetä niiku käyttäjille. Ni sit ne pysyy siellä paremmin mukana. Et kyl se selkeyttää sitä prosessia tosi paljon. Suunnitteluprosessia ja sitä niiku et niihin voi aina palata, et se on tavallaans emmonen fail safe siellä, siel suunnittelu...

Et se on enemmänkin semmonen työkalu, joka varmistaa sen et tää niinku alkupään käyttäjädata on sit niiku myöhemminkin mukana?

Joo, kyl vois sanoo näin. Et mä ajattelin sitä jotenki tämmösena, persoonia, jotenkin tämmösenä evaluaatio menetelmänä. Mikä taas suomesks taas arviointimeneltelmä.

Tarkottaen mitä?

Et tulee tavallaan just ku sen konseptin saa valmiiks tai et erivaiheissa konseptin kehitystä varsinkin ku valmiiks ku se sahataan. Tavallaan et jos ajattelee persoonan briifiniä ni siihen on aina hyvä palata ja siit on hyvä tarkistaa et pitääkö nää hpyoteesit ja väittmät paikkansa kyinka hyvin siin. Et mä ajattelen sitä niiku arvioinitmeneltelmän, et äydään niiku kerta kerralta jokaisen persoonan mitä siihen on ehitetty. Et persoonien tavallaan detaljit ja katotaan kuinka se just siihen spesifiin asiaan, mitä se vaatii tai haluaa tai se on rajottunu niin... Et miten se vastaa just isiihen. Et nään tosi hyödyllisenä sen ja jotenki se auttoo myöskin sitten siinä niinkun.. Jos nyt jargoniin mennään niinku, niin oman kognitiivisen kuroman vähentämiseen auttaa myöskiin siinä et suunnittelussa on paljon helpmopi suinnitella jollekin ihmiselle muotoilijana ku että läjälle dataa. Et se niinkun personointi on tosi tärpeetä siinä, henkilökohtaisesti.

Tota sitten ois sit tää niinkun.. Pieni design tehtävä. Eli mul on täs vähän lähdematskuja, tehtävänanto viel kirjallisen,a mutta käytännössä. Tai jos mä luen sen tehtävänannon suoraan ni se on helpoin.. Tulee sit kaikille samalla tavalla. Eli, tehtävänäanto. Yleisradio on tuottamassa uutta kotimasita sairaaladraamasarjaa. Draamasarjan yhteydessä on tarkoitus hyödyntää hbbtv-teknologian tuomia mahdollisuuksia lisätä tv lähetykseen vuorovaikutustoiminnallisuutta. Tavoitteena on tarjota katsojille uusia tapoja nauttia sarjan katsomissta. Tehtävänäsi on suunnitella konsepti, joka hyödyntää televisiota mediana. Yksi draamasarjan pääkohderyhmistä havainnollistaa persoonaa Mary the Content Comfort seeker. Se on liitteenä. Yleisradio on antanut vapaat kädet luoda kohderyhmän tarpeisiin parhaiten sopiva konsepti. Voit havainnnoliistaa konseptia ja sen käyttötapaa tekstin tai piirrosten avulla. TV-näytöllä olevan käyttöliittymän ohjauslaitteena on kaukosäädin. Eli.. Ja tähän on 15 minuuttia aikaa ja edelleenkään ninkun ei arvioida sua eikä sitä lopputulemaa. Mä voin antaa tästä nää matskut.. Onks tässä vaiheessa.. Heräsikö suoraan kysymyksiä? Siinä on..

Ensimmäinen.. Ensimmäinen kysymys. Mikä on HbbTV?

Tossa on lyhyt briiffi aiheessa. Ja tässä on muistutukseksi vielä miltä kaukosäädin näyttää.

Tuttu tavara.

Joo. Ja sitten on kynää ja paperia.

Joo'o. Ja millon alkaa aika?

Onks sulla nyt tässä alkuun kysymyksiä vielä?

Ei.

Ei. Mä kerron sitten ajan kulkua myös. Ja vaikka nytten voi alkaa.

Okei. Onks sul tässä hyötyä et mä ajattelen ääneen tätä?

Ihan miten sun on helpoin tehä. On siitä varmaan vähän hyötyäkin.

[Haastateltava lukee materiaaleja]

Vastaaks sä kysymyksiin tehtävän aikana?

Rajoittuneesti. Voit kysyä.

Toimit googlena täs tavallaan. Okei. [tauko] Onks nää semmosia papereita mihin voi tehä merkintöjä?

Joo, voi merkkailla ihan vapaasti.

[Haastateltava lukee materiaaleja]

Keskipalkkanen mary.. Itseasiassa varmaan vähän parempi ku keskipalkka. Steady and stable.. Mul tulee mieleen tästä toi electroluxin briiffi. Husband...

[Haastateltava lukee materiaaleja]

Mä haluan tietää sit ku mul on viis minuuttia jäljellä.

Joo.

[Haastateltava lukee materiaaleja]

EPG on yhtä kuin..?

Electronic Program Guide, eli TV-ohje

Jaa infonappi.

Käytännössä.

[Haastateltava lukee materiaaleja]

Teletext.. Seki on vielä olemassa..

[Haastateltava lukee materiaaleja]

Joo, tää on.. Tuntuu selkeesti olevan kohderyhmää. Saippuaoopperat. Domestic mainittu moneen kertaan. Eskapismi mainittu.

[Haastateltava lukee materiaaleja]

Saako tätä kommentoida samalla?

Saa kommentoida.

Musta tuntuu että mulle yritetään tyrkyttää ipadia tässä käytettäväksi.

[Haastateltava lukee materiaaleja]

Joo. Vaikuttaa siis siltä et tässä niinkun Maryn lisäks on Adam aika tärkee tekijä. Tärkee tekijä tässä ja.. Mutta Adamista ei oo persoonaa olemassa?

Ei oo.

[Haastateltava lukee materiaaleja]

Joo eli Mary vaikuttaa vähän taustalla pyörivä.. Taustalla pyörivä tv on osa juttu ja tärkeetä niinku viettää aikaa yhdessä miehen kanssa tv:n ääressä. Ipadi löytyy. Ja.. Mä nyt sanoisin et mä suunnittelisin tässä nyt niinkun.. Elikkä tää niinkun tapahtuu tää HbbTV tapahtuu ihan normitelevisiossa, tavan televisiossa?

Joo.

Et sillä on nyt sit niinku se telkkari missä pyörii matsku. Hospital drama, siitähän tässä oli kyse. Ja sit siel on toi.. Viiskyt yks.. Ni se ei välttämättä nyt ihan keinutuolissa.. Mut aika lähelle. Niillä on myös kaukosäädin, josta se tarkistelee välillä jotain teletekstiä. Mitäs mun aika näyttää?

Sellanen kaheksan minuuttia vielä.

Noni. Ja si tsillä on tuolla toi Adami on tuolla taustalla, jolla on piippu. Koska minä päätän silleen. Molemmilla omat sarjansa. Elikkä mä haluisin nyt semmosen idean viedä täs läpi, että.. Että tota, molemmat pystyy löytämään.. Koska molemmilla on omat TV-sarjansa, tai niinku favouritet. Ni näitä sydämiä.. Et molemmilla on niinku omat sarjansa, mitä ne haluaa seurata. Mä haluaisin tuoda niitten.. Tähän yhteiskäyttöiseen ipadiin, mikä niillä nyt on, selkeesti miehen ipadi, mut sitä niinku toi mary myöskin käyttää. Tuoda niinku vähän samantyyppisen ratkasun ku mitä tossa netflixissä on, et kumpi katsoo. Et tossa ninku mary ja sit Adam ois sitten se toinen. Et siel on niinku profiilit. Joo'o. Ja mitä tällä tehdään sit on se, että tällä niinkun vaikutetaan.. Vaikutetaan siihen mitä tässä hospital draamasa, jota ilmeisest niinku... Mä suunnittelen sillai et hospital sarjaa seuraa mary. Adamia ei niinkään kiinnosta se. Mut vaikka kiinnostaiskin, niillä ois illon niiku omat profiilit mitä n pystyy laitaa siihen. Eli ku mul oli pointtina se, idenaa tässä, et mite.. Ku siinä tapahtuu jotain siinä tv-sarjassa.. Elikkä siinä  on nyt niinkun tohtoreita tietysti näyttää tältä kun on toi skooppi ja kynä taskussa. Toinen makaa tossa sängyssä potilaana. Ni sitten tavallaan se, että täällä selkeesti näkyy täällä tv-ruudussa, niinkun tässä mainittiin, että logo tai jokin muu vastaava. Ni täällä on esim hotspotti. Et siinä vaan ilmoitetaan et siinä fomraatissa on jotain saatavilla, ylimääräästä tietoa. Josta ne vois sitten kattoa tällä niinku ipadin kautta.. Ipadin kautta pystyis sitten kattomaan sen, sen lisää oppimaan näistä tyypeistä mitä näis sarjoissa. Tvallaan tämmösiä henkilökuvauksia. Mitä mä oon niinkun ite kokenut, mitä mulla on usein omgelmina näissä että.. Tota jos mä haluisin tietää jostain henilöstä lisää tai jos mä unohdan yksinkertaseksi kuka se on, et nää on ikaks irrallist apalvelua. Et esimerkiks game of thrones oli mulle semmonen.. Et siinä oli helvetisti niitä hahmoja et mä en musitanut niitten nimiä. Sit ku ne puhu keskenään niinku dialogissa ni mä en muistanu kuka on kukin. Et tavallaan sillon sais tommoset.. Personointi. Sanotaan näin. Et sais niinkun personoinnit niistä niinkun henkilöistä, jotka on just sillon pelillä. Eli tää ois niinkun real time. Tavallaan niinkun augmented augmented reality. Se on vähän semmonen, semmonen niinku inceptionimäinne tyyppi siitä. Et se ois vaan niinkun.. Vois pystyy käyttää tätä ipadia ja sitte siihen, että doctori ja patient. Mitä tässä just tässä kohdassa tapahtuu, niin sillon pystys tavallaan seuraamaan sitä.. Sitä niinkun.. Vähän niinku kolmiulotteisemmin.. TAi kahesta eri perspektiivistä et mitä niinkun oikeesti tapahtuu siellä ja pystys sitten palaaman tähän niiku tietoihin, et mitä siin sarjas oikeesti tapahtuu. Et onks siiinä.. Tois vähän semmosen urheliumaisen elementin, et jengi kattoo peliä ni ne myöskin kattoo, niinkun puhelimesta seuraa tuloksia ja jotain.. Mitä tää on niinku.. Uhkapelisivustoja. Et niin seuraa sitä vedonlyöntiä siinä samalla, et miten ne kertoimet muuttuu. Et tois ne vähän samanlaiset.. Samanlaisen ajatuksen tähän. Et se ois vaan yksinkertaisesti lisää tietoa näistä henkilöistä. Mitä täällä niinkun tapahtuu. Heidän kahden välillä esimerkiksi, onks siinä jotain. sit siinä pystys helposti.. Tääki tykkää tää Mary, ni se tykkää tuolla tota kavereitten kanssa soitella TV-ohjelmine aikana. Ja mä oletan nyt et ne seuraa samaa sarjaa ja koska niillä on saman tyyppiset intressit ilmeisseti, ni tää ois sitten myöskin mahdollisuus olla niinkun.. Laitetaas tosta niinkun lisäksi.. Ois niinkun puhelimen ääressä mahdollista olla, olla niinkun ton.. Kolmannen osapuolen kanssa. Omasta profiilistaan siihen, jolloin yöskin tää sama appi kertos siellä. Jotenkin tavallaan et ois semmonen kommunikointi platformi et mä uskosin et tommonen niinkun ajatustapana ni se.. Se niinku yhdistäs niitä kävijöitä tai niitä tv-sarjan katsojia ja sit se yhdistäs myöskin sitä niinkun.. siinä oikeesti ois jos on tehty hyvä skripti tohon taustalle tohon sarjaan ni se pääsis oikeuksiinsa. Ja se tavallaan.. Tää materiaalihan tavallaan on jo olemassa monesas TV-sarjassa, mut sitä ei koskaan päästetä näkemään. Jos sitä vaan seurataan sivusilmällä. Ja sit just joku, mitä mua ois kinnostanu esimerkiksi, jos puhutaan sairaalasarjoista, ni toi ois kyl tähän joku mahdollisuus myös niinkun kattoa.. Et siinä ois helppo kerätä sitä tietoa et mitä tapahtu viime jaksossa ja miten tavallaan se.. Onks se kontekstisidonnainen millään tavalla että onks se niinku.. Onks se verrattavisas oikeeseen elämään millään tavalla. Elikkä kuinka tarkasti ne niinku prosedyyrit ois mietitty siellä, et minkälaisia.. mitä n elääkärit tekee ja miten nää ois [epäselvää].. Ni semmonen additional information platform. Mä kirjotan sen othon alas. [tauko] Näin.. Ja sitten. Tuol oli profiili. Ja tossa on niinkun.. Tää ois tavallaan niinku IM service.

IM service eli instant messaging vai?

Joo. Tai video call.. Laitetaan se. VOIP kautta IMS sieltä. Hyvä. Se on valmis.

Mahtavaa.

Jäis mulla aikaa?

Jäi minuutti aikaa vielä.

Sit mä voin hifistellä tähän vielä tota sydänäkäyrälaitteen vielä. Ja bad news. Se vaan.. Sä oot kuolemassa, että "tohtori, kuinka kauan mulla on aikaa" paljos se kello nyt näyttää?

No, se tais aika loppua just.

Näin.

Mahtavaa.

Tää oli siis.. JOs sä käytät tätä viel jossakin.. Ni tää on niinkun nyt se konsepti, uus konsepti.

Okei, mahtavaa. Hienoa. Mikä sul..

Tää on nopein skissi mitä mä oon koskaan tehny.

Mä tykkään näist tota teollisen muotoilijan perspektiivesitä. ne on aika ![]()niinkun

Mietitty ja harkittu

Ja sillai vahvoja.. Mut joo. Mikä sul oli tossa prosessi, jos sä kerrot vähän. Sä tos aika hyvin jo puhuitki läpi, mut niinku.

No prosessi mitä niiku ekana tulee mieleen tosta.. Tossa on niinku aika hyvin kerrottiin tässä.. Tässä status quo, et missä nyt mennään. ET mikä on tämän hetkinen tilanne. Ja sit se antaa aika paljon vinkkejä siihen et mitä laitteita on nykysin käytössä ja nykyisin kattoo TV:tä ja mitä nykyään käyttää siinä. Vähän antaa myöskin sitten rajotteita mieleen. Mieleen.. Ja tota. Tavallaan niinku rakentaa sen frameworkin minkä sisällä melkeen pitää niinkun lähtee suunnittelemaan, jos on kyse just tästä persoonasta. Ni siitä oli ehkä niinku helppo lähtee pirtää tota ensimmäistä status quo kuvaa. Mä yleensä teen niinkun tämmösen kuvauksen siitä ja itselle muistiinpanoksi, et mitä, minkä.. Niinku, vaikuttaa siihen asiaan. Sit oli oikeestaan aika iisi idea.. Mä sain niinkun ajatuksen siitä et oisko näist tämmösen notifikaatio.. Tai niinku ajattelu.. ET jokaisen karakteerin kohdalla ons iel tv sarjassa pienii täppiä joista pystyy painaa. Samalla mä tiesin et nää ei tuu kahdestaan seuraamaan mitään sarjaa niinkun pelkästään ipadin näytöllä. Et briiffi oli tietyisti et tv:seen suunnitellaan jotain. Et onks se sit toi.. Periaatteessa toi sama tekniikka. Varmaan vois upottaa tämmöstä teletextiä viel iisisti.. Suhteellisen iisisti. Mut se ois vaan niinku seuraava askel sit siitä mä näin luonnolliseks. Et tossa niinku... En osaa sanoa onko ton tyyppisiä palveluita olemassa, mut mulla tuli niinkun ekana mieleen toi urheilun seuraaminen mitä on tottunu kattomaan vierestä itekki ni.. Mutta tota, semmonen niinku suunnitteluprosessi. Mites sitten muuta? Mä niinkun alleviivasin aikapaljon kaikke tärkeen, itselleni mielestäni tärkeimmät asiat, elikkä niinkun käytös.. käytös miten ne käyttää näitä laitteita. Kyselin vähän noi jargonit auki tuolta. Ja sit et minkä tyyppinen ihminen.. Siit saa aika hyvän käsityksen siitä, siitä niinkun persoonakuvauksesta. Siin ei itse persoonsta.. Mä ihan hyvin pystyn kuvittelemaan tämmösen niinku Maryn olevan olemassa. Mitä mä oisin toivonu et ois ollu, mitä mä piirsin siihen ekana ni kuvan..

Joo mä huomasin.

Auttaa mua niinkun suunnittelussa.. SE auttaa sit helposti.. Sen ei tartte olla karaktaarinena kummonen, et kunhan se on olemassa, et se on helppo personoida. Mun mielst tää oli äärettömän spesifi. Spesifi niinkun.. Ja tarinanmuotoinen tää persoona. Mutta, tää on niinku paljon spesifimpi mitä on tottunu näkemään, että nää on monesti ois just kerrottu niinkun.. Tässä ollu esimerkiksi niinky yhtä ainutta quotea Marylta. Mitä sanoa.. ET se on semmonen hyvä infon lisäys, niinku tone of voicesta ni vähän niikun auttaa personoitumaan siihen. Laittamaan itseensä tohon samaan perspektiiviin. Mut et mitä on tottunu näkemään on just enempi ehkä tämmöstä et siinä on vaan niinku bullet pointeilla kerrotu listaus, et mitkä on rajoitteita ja mahdollisuuksia, et mihin on niinku tottunu. Et se ois tavallaan jo semmonen, puoliks suunnitteltu se prosessi. Mut täs oli mun mielestä kaikki tarpeellinen. Tarpeellinen. Täs oli ehkä niinkun pointtina jsut siinä, että riippuen nyt projektin laaduta, mut mun mielestä täs on jopa niinku liikaa informaatiota.

Mitä siel on ylimäärästä?

Elikkä esimerkiks toi niinku social context on aika pitkälti niinku se päivä kuvaus. Tässä on tosi paljon sitä samaan. ET täällä pariin kertaan mainittiin siitä et toi.. Et toi niinku soittelee kavereitten kaa, jos mä en väärin muista. Se soittelee kavereitten kanssa tässä ni se on ollu turhaa toistoa, siinä mielessä.. Ja sitten oli myöskin tää et se se tykkää myöskin kutoo samaan aikaan.  mun mielestä sekin oli pariin kertaan tässä. Et vaan niinkun semmoset asiat. Mutta, aikapaine oli tässä myöskin..

No se on. Tää on lyhyt aika.

Niin. Ni tota.. Mutta näkisin että tämmösen persoonan kautta niinkun.. Miten mä ite lähtisin suunnittelee, et käyttäsin just 80% sen persoonan tuntemiseen ajasta ja 20% siihen suunnittelutyöhön. Et mä uskon et käyttäjälähtöisestä näkökulmasta, et se on just niinkun oikein päin. Monesti mennään niinku toisin päin.

Mitä osia sä sit tosta erityisesti käytit nytte hyödyksi?

Kaikki mitä mä alleviivasin. Mä alleviivasin aika paljon tosta niinku päivän tarinasta. Vähän sitä fiiliksiä, mitkä asiat sielt vaikuttaa. Ja sitte niinku teknologiakuvauksesta kuinka hän niinku käsittää niikun teknologian, et ei myöskään ole hirveen kiinnostunut siitä, tämmösiä asioita. Jotka on yhtä kuin mun alleviivatut kohat on aikalailla suunnittelubriiffiä, osa. Et jos esimerkiks se ei oo kauheen kiinnostunu käyttöliittymästä tai et ei oo niinku kauheen kiinnostunut siitä niinku teknologiasta ylipäätään, ni se mulle aikalailla on yhtä ku et se pitää olla aika todella simppeli käyttää, et siinä on niinku mahdollisimman alhanen kynnys lähtee sitä opettelemana, houkutella siihen. Ja.. Mut sitten taas toisaalta se kuitenkin, kuitenkin tottunut käyttämään ipadiä ja niinku tekniikkana, mikä on jo itsessään jo helppokäyttönen monelle vanhemmellakin ihmiselle. Tiedän sen omasta äidistä, joka on niinkun aika ton ikänen ja tota.. Osaa sitten yhdistää.. ET osaa helposti niinkun youtubesta ettiä jotain, ni sit pitäs olla jotain vähän niikun tutun näköstä tai siis semmosta fiilistä et mitä youtube vois olla televisioissa. Youtubea ei tässä mainittu, mutta jos etsitään ohjeita ja tän tyyppistä johonkin neulomiseen, todennäkösesti puhutaan just siitä. Aika paljon piti tehä semmosia.. että niinkun.. Toi nyt kertoo tosi paljon et mitä se tykkää nykysellään kattoo ja mitä se niinkun haluaa sillä et mitä se.. Mitä niinkun median käyttö on. Ja sitte niinku pain pointsit. Tää on niinkun tää osio tässä missä on nää kolme kohtaa, ni tää on mulle itsessään jo.. Niinku tää vois olla se persoona jo itsessään tosi paljolti. Et ehkä yhdistettynä tohon kuvaan ja niinku.. Tohon perus. Perus dataan siitä, taustaan, tää jo niinku auttas tosi paljon. Et se ois jo riittävä. Mua niinkun, varsinkin niinku toi description of work and daily life, se on aika niinku sillai nice to know, mutta tota.. Käyttäsin sitä ehkä niinkun varauksella. Et menisin ehkä suoraan näihin bullet pointteihin. Spoilaisin ensin tän persoonan ja sitten vasta menisin tutustumaan siihen. Niitä nyt ehkä enimmäkseen tässä käytin.

Oliks tossa muita puutteita ku toi kasvokuva tai kuva?

Puutteita. Ei sitte.. Joo, no se quote. Mun mielestä se on tärkee juttu. Et se antaa hirveen paljon sillee niinkun persoonasta. Tekee siitä jotenkin vaan niinku ihmisemmän. Ihmisemmän. Et kuva on nyt semmonen et se voi mun mielest olla vaan joku, joku niinkun pyyhkäsy. Et sen ei tarvii olla mikään valokuva tai sellanen. Mut se on jotenkin niin paljon suunnittelijana helpompi.. Helpompi ku lähestyä ku seon kuvallisesti esitetty jollakin tavalla se ihminen. Et sille.. Et niinku tälle ollaan suunnittelemassa, joka tekee sitte myöskin siitä prosessista ja suunnittelusta niiku tärkeämpää. Et sillee.

Tärkeämpää niiku millä tavalla?

Et se tekee siitä ehkä henkilökohtaisempaa suunnittelua. Ja se on niiku muotoiluhommissa mun mielestä aina täytyy suunnitella jollekin henkilölle, se on niinkun äärettömän tärkeetä et se tuntuu kans siltä et siinä suunnitellaan ihmiselle. Ihmiselle.. Ja sitte myöskin niinkun se konseptinluomisprosessi se osu huomattavasti nopeammin ku pysty jo niinku päässään kuvittelemaan et minkälainen nojatuoli sillä maryllä on sen adamin kanssa. Et pystyy tavallaan ehlpommin tuntemaan sitä kautta. Joo. Siinä oikeestaan tästä kommentit. Hyvä persoona. Hyvä persoona, tää anto paljon tietoa tästä. Kuva puuttuu, muuttuu.

Jotain maintsit, et tää on laajempi ku mitä sä oot tottunu näkemään. Oliks jotain muita eroja?

Se mitä mä en tässä tajunnu oli toi second screen use.

Minkä takia? Onks se niinku terminä vai..

Tarkottaaaks se niinku.. Mä en ollu ihan varma et tarkottaaks se kahden näytön käyttöä samaan aikaan käytännössä. Kattoo telkkaria ja selaa puhelinta samaan aikaan

Joo käytännössä tarkottaa samaa.

Jos jotakin niinku muuttaisin tästä itse, niin lähtisin pois jargonista. Kuten EPG ja teletextkin on jo jargonia mun mielestä. Kaikki ois mahdollisimman simppeliä. Et täs on ehkä niinkun liiallisesti monimutkaistettu tiettyjä asioita. Missä taas kerrotaan, sit mikä on niinkun lempparisarja esimerkiks. Sen vois ihan hyvin olla niiku sarjan nimi, et onks se sit niinku holby cityn sairaala vai mikä, mikä se on. Tämmösiä asioita. Et se antais ehkä enemmän viitettä siihen et minkä tyyppinen se on se ihminen, ku et tehtäs tämmöstä niinkun.. Johonkin laatikkoon laittamista. Et spesifempiä esimerkkejä mut sti samalla taas jargonista pois päin.

Tota.. Arvioiks sä tos ton tota lukiessa tai sitä käyttäessä sen luotettavuutta tai uskottavuutta?

En oikeestaan. Mun mielestä tää oli niinkun.. Jos tää mulle työnnettäs yhtäkkiä eteen niiku design briefin ohessa ni mä automaattisesti kuvittelisin et tää on oikea henkilö. Ehkä se on myöskin että, aika niinkun oma mielikuvitus on vähän semmonen et oppi niiku rakentamaan sen mitä jää välistä ni oppii rakentamaan sen välin, että.. Että tavallaan se ei tarvii olal hirveen spesifi kun niinku ajattelee sitä ihmisenä. Se on ehkä niinku omista aivoista enempi kiinni ku siitä persoonasta. Mutta en kyseenalaistanut hänen luotettavuuttan, että..

Mä vois ottaa tän hetkeks pois ja nyt jos tästä niinkun.. Mitä kuvittelit Maryn olevan ni haluuks sä kuvailla Maryä lyhyesti omin sanoin?

Tää on hyvä kysymys. Tota.. Mary on aika pitkälti semmonen, semmonen nainen ku mun äiti on. Mä jotenkin kuvittelin sen että, mun mielestä niinkun hyvä persoona. Hyvä persoona on semmonen et jonka voi kuvitella olemassa. Ja niinku et hei et täähän on vähän niiku sen tyyppinen, et mä tiedän. Et sen osaa yhdistää johonkin olemassa olevaan ihmiseen. Tää oli tosi lähellä mun äitiä. vaikkakin mary oli niikun iältään nuorempi, se oli silti niikun samoista asioista kiinnostunut. Et sillä oli tää niinkun käden työt ja tämmöset puhdetyöt ja sit et se on yhteistä aikaa miehen kanssa viettäminen, niikun teleivision äärellä lööllöttelyä, et se on pitkälti sitä samaa. Et oli jopa samoista tv-sarjoista kiinnostunut, et siel oli kotimianen tuotanto yks tosi tärkee juttu. Ja mun äiti on myöskin fanittaa sitten tämmösiä, tämmösiä niiku sairaalsarajoja ja ennen kaikkea suomalaisia ni.. Voisin kuvitella et se niiku.. Mä olin ehkä suunnittelemassa omalle äidilleni tässä sitte uutta konseptia. Mut se vaan nyt sattu olemaan sen tyyppinen, mut mitä Mary on sitten.. Siin ei sanottu minkä maalainen se oli, mut mä ajattelin sen jotenki brittiläinen, perustuen sit tohon et britissä on TV kulttuuri ollu valliollaan pitkään aikaan. Mä jotenkin ajattelin et se oli seiltä päin, sieltä päin. Tai ajattalin sen semmosena... Semmosena tota teetä juovana neitona siellä ja tota... Jolla on tämmösta, jolla on tämmönen tyyli niikun suhtautua hyvin eskapistisesti niinkun teknologiaan ylipäätään. Et viihde on tarkoitus päästä töistä pois, töistä pois, mikä tossa aika suoraan sanottiiinkin. En usko, että Mary ois itse sitä itse osannut sanoa, mut tän niinku ois tutkimuksen myötä tullu tähän tulokseen. Mary ei vaikuta niinkun maailman sosiaalisimmalta ihmiseltä, vaikka se käy niinkun kavereitten kansa päivän asiota ni sit se, että.. Et se kattoo televisiota koska se ehkä tapaa ammatissan niin paljon ihmisiä et se haluaa niinkun ammattinsa kautta sitte jotenki.. Tapa atarpeeksi ihmisiä siinä hommassa ja haluaa tv:ltä sitte omaa aikaa ja omaa rauhaa. Mä en nyt päässy käsityks.. Mä en päässy käsitykseen et oliks niillä niiku sen miehen kanssa et oliko niillä minkälaisia lemmikkejä, et niitä ei tossa mainittu. Vähän niiku jack russellin terrieri sopis tohon, tohon samaan syssyyhn. Vähän et aikuiset pojat.. Tai lapset, käy siel syömässä melkeen joka päivä. Tämmösiä asioita että. Pakko sanoa, et niinkun persoonakuvauksen luettua kyl se mielikuvan rakentanu siiitä persoonasta ni. Se on paljon helpompi muistaa tämmösiä yksityiskohtia myöskin.. myöskin että. Et jos tää ois ollu pelkästään bullet pointteja eikä ois sitte yhtä ainutta kuvaa piirtäny, en muistas et sillä on kaks aikusta lasta, jotka käy kotona syömässä melkeen joka päivä. Et siinä mielessä kyllä. ammattia en muista, mtu se oli joku tärkee. Executive something. Et se oli semmonen tärkeempi. Sitä en muista, mutta tota.. Näkisin tälleen että suhteellisen kiireistä aikaa elää elämässään ja niinkun töissään ja sen takia nyt teknologia on sille tärkee, niinkun tärkee pois pääsy. Ja ja.. Mitäs sitte muuta? Sillä ei ollu silmälaseja mun mielestä. Se oli ehkä semmonen sepsifi.. Ei ainakaan niiku näkyviä silmälaseja ollu. Siinä ne tärkeimmät.

Oliks siinä persoonassa jotain yllättävää?

No yllättävää ehkä siinä mielessä, et mä kuvittelisen sen alkuun vähän vanhemmaksi. Et mä menin aika suoraan sen päiväkuvaukseen ja sitten vasta lähin selaamaan niitä niinkun tarkemmin noita speksejä. Elikkä niinkun demografista tietoa hänestä, hänestä. Mä jotenkin kuvittelin sen niinku viis kymmenen vuotta vanhemmaks ku mitä se tossa sanottiin. Et se oli ehkä yllättävin juttu tossa. Muuten niiku sanoin oli tosi lähellä mun äidin tyyppistä kansalaista ja että en niiku nähny ihan hirveen llättävänä siät. siinä tärkeimmät.

Joo. Sit mul ois tässä itseasiassa pari muutakin persoonaa, joka niinkun toivon että luet läpi. Ei tartte yhtä ajatuksella, mutta sillai lukaset läpi. Katot vähän mitä siinä on. Siinä on niinku Susie ja Danny.

Susie. Liittyyks tää tähän samaan konseptiin?

Samaan kontektsiin joo. Ja jutellaan sit sen jälkeen vähän lisää niistä.

[Haastateltava lukee persoonia]

Saaks näihin tehä merkintjä?

Saa toki.

[Haastateltava lukee persoonia]

Siinäkin toinen sivu vielä. Ne kaikki on kakssivusia. Tai tollai reilun sivun mittasia

[Haastateltava lukee persoonia]

Ookei. Danny...

[Haastateltava lukee persoonia]

Mihin nää perustuu?

Hmm?

Perustuuks nää oikeeseen dataan nää persoonat?

Voidaan jutella siitä kohta.

Joo.

[Haastateltava lukee persoonia]

Okei. Tunnen heidät nyt.

Tunnet heidät nyt. Huomasiks sä noissa persoonissa jotain ni eroja? En nyt tarkota henkilöiden eroja, mut sellai laadullisia eroja?

Joo. Itseasiassa tää Susie, 19 vuotias Susie, vaikutti jotenkin niiknu mä en tiedä miksi, mutta vaikutti et siihen oli ehkä helpompi päästä sen motiiveihin käsiksi.

Täs on muuten tää Mary myös ni voi otta senkin huomioon tässä keskustelussa. Mut niin, Susieen oli helpompi päästä..

Se oli jotenkin hekä helpompi päästä, päästä niinkun perille niinkun tästä et mikä tää tyyppi. Tääl oli muun muassa, ainoo juttu minkä mä merkkasin, oli se et siinä oli quote olemassa tästä tyypistä. Jos oli jossain muualla ni en ainakaan huomannu. Mut siihen oli ehkä niiku helpompi jotenkin niiku ajatella ihan pelkästään sanamuodosta minkälainen tyyppi se on ja miten se ajattelee. Et se  on ihan pieni juttu. Et täähän on tosi helppo mun yhdistää tietinlaiseen ihmistyyppiin. Et tää oli ihan selkeesti. Ihan selkeesti mun kämppis. Ihan just semmonen, mitä mä niiku näkisin et miten nää niiku toimii. Missä taas Danny oli taas sitten.. Tää oli ehkä teknisesti speksattu tosi tarkasti, mut sitte.. Mä en niiku saanu irti ihan hirveesti tästä ehnkilöstä. Et tää oli tosi teknologislähesty.. Lähestymistapa. Et jos näitä pitäs käyttää suunnittelulähtökohtina, ni se ois ehkä alkuvaiheessa ku suunnittellaan konseptia tai ideoidan sitä mitä se vois olla ni tän tyyppinen ku susie ois se helpompi, et se ois.. pääsis helpommin siihen fiilikseen, emotionaaliseen puoleen käsiks. ku sitte taas mennään loppuvaiiheeseen, et miten mikäkin toimii niin sit.. ehkä niiku myöhemmässä vaiheessa validoindaan sitä konseptia, niin Danny ois hyödyllisempi. Koska sillä ois helpompi yhidstää se teknologia ja et mitä käytetään. Et nääkin vois olla.. Niinku et vähän eri tyyppiset. En tiiä onko yhtään kärryillä, mut näin tää niinku mun mielestä tuntu. Muuten mulle tuli Dannysta vähän sellanen kuva et se ei ihan hirveesti piittaa sen lapsista, koska tota.. Se valittaa että sillä ei oo tarpeeks vapaa-aikaa, ja sit ku sillä on ni se katto ovaan dokkareita. Et niiku.. Et se niinku tykkää oppia. Mut tää danny vaikutti vähän semmoselta, mä yhdiistin niinku tosi paljon itseeni. Et täl on niiku hyvin samanlaiset intressit ku mitä mulla itselläni on. Mut sit tää oli niinku 44-vuotias, mä sitä vähän niiiku säikähdin. Mä oon 26 ni ei pitäs silleen kiinnostaa vielä samat asiat. Mut kuulostaa silleen samalta, et käy niinku lukee jotain, mut palaa sit myöhemmin jos ei jotenkin kerkee lukemaan mukamas. Tämmösii asioita. Joo, mtu muita eroja.. Muita eroja nin totaa.. Molemmat on musta niiku toimivia persoonia, ut ehkä eri vaiheessa suunnittelua. Teenage social butterfly.. Okei.

Mitä?

Mä voin vaan niin kuvitella mun kämppiksen. Lisätään tinder sille ni sit seon.. Menee täydestä. Joo. Se nyt näistä. Ehkä niinkun.. Otsikot on pitkälti samat. Mitä mä näistä pystyn näkemään. Pain points, mä laitoin tonne et täs oli Dannyllä et his watching is often interruptet. et se oli niiku n et se aina keskseytetään, et onks se niiku pain point? vai et onks se niiku vaan.. faktaa ja sit et..

Et näin asiat on...

Et näin niiknu asiat. Näin on marjat. Et se ei niinku suoranaisesti oo hänen valintansa et se pystyis vaikuttamaan. Mut joo. Oliks tää niiku sinne päinkään nää mun havainnot?

Emmä tässä vielä kommentoi niitä. Joo. Oliks noissa sit jotain yllättävää noissa persoonissa? Jotain mitä et odottanu.

Ekana kysymyksenä tulee mieleen et nää on niiku.. Tai kysymyksenä niinkään mieleen.. Mä nyt mietin sitä, et joo tulotaso on aika merkittävä juttu tässä, mutta.. Mut niiku heijastuuks se tulotaso sitten.. Joo tässä puhuttiin susien kohalla, et hän ei niinku pysty pitää yllä semmosta lifestyleä mitä se halua tai haluais, ni sit silti se ostelee ylimääräst tavaraa. Ja tota.. Tekee niinku heräteostokisa ku sit taas Danny oli aika niinkun hissun kissun sen rahan käytöksestä sen kummemmin puhuttu. Mutta oletan et ne menee niinku kahden lapsen ja vaimon, vaimon niinku kanssa asuessa ja eläessä ni menee kyllä kaikki siihen liittyen. Muuta näistä.. Yllättävää, niin tota. Joo, itseasiassa kyllä. Danny oli early adapter of technologies. Ja jos mä niin kuvittelen et se on 44-vuotias, mun mielestä se on liian vanha olemaan aikainen adaptoija. Koska kaikki 44 ja sen tyyppiset ihmiset ni mitä mä tunnen ei oo kyllä ehkä teknologian edelläkvijöitä, et neki on tippunu kärryillä varsinkin nykypäivänä. Sen saatan niiku toi.. Yksityiskohta munmielestä saa tän persoonan vaikuttaa vanhanaikaselta. Tai et niikun tän persoonan itsessään, et tää ois ollu niiku mahdollista kymmennen vuotta sitten, et ei ehkä nykyään enää. en usko että osaa käyttää periscopea tai snapshattiä. Et teknologiakin on kehittynyt sinänsä tosi paljon. Et siitä mä oon ehkä vähän.. Olisin varuillani siitä asiasta. Mut toihan on tollanen vastuuvapautus, jos puhutaan suunnittelutyöstä et on niinku early adapter, ni se voi olla ihan mitä tahansa ni se ottaa sen aina käyttöön heti ensimmäisen. Se on vastuuvapautus. Sen takia sen kans pitäs olla niiku todella varovainen persoonassa. Joo joo, se ymmärtää sen kyllä ja sit tulee et käytettävyys on paskaa. Et se on riski. Mut susielle oli taas sitte tää vaikuttaa sillee et vaikka tähän on helppo päästä käsiks tähän tyyppiin.. Niin.. Täs oli myös tämmönen mikä tekee asiasta tosi vaikeeta, että se käyttää niinkun palveluita ja sosiaalista meidaa, mut se ei tiedä miten ne toimii. Niinkun mä katon tätä samaa skills and knowledge kohtia. Ja sit et se sosiaalisen median käyttö perustuu lähinnä siihen et mitä sen kaveritkin tekee. Et se oli aikalailla.. Sitä on jotenkin vaikee lähestyä, mutta mitään niiku suoranaisesti muuta yllätävää mä en sit hänestä löytänyt. Ehkä se mikä tuli mieleen tätä lukiessa oli että tää vaikuttaa jopa niinkun stereotyyppiseltä. Stereotyyppiseltä, niinku tämmönen social butterfly ni täs on se otsikoitikin tässä. Että.. Mä en tiedä onks se hyvä alleviivata sitä, sitä niinkun liiikaa. Et se on.. Tää on vähän jo vähän niiku liian stereotyyppinen. Mut niin on toisaalta mun kämppiskin, että.. Niinku.. Se ei niinkun suoranaisesti yllättäis mua, jos tällanen henkilö ois niinku olemassa. Joo, ei oo must tollai yllättävää.

Entä jos sun pitäs järjestää noi niinku uskottavuuden mukaan nää kaikki kolme, mihin järjestykseen sä laittasit?

No auttomaattisesti laittas Maryn ensimmäiseks ihan vaan sen takia et mä käytin niin paljon enemmän aikaa sen lukemiseen.

Niin sä tunnet sen paremin

Mä tunnen sen niinku henkilökohtasemmin jo tässä vaiheessa. Mutta.. Kyl se mary varmaan ois niinkun uskottavin, koska mulla on niinkun heti työntää siihen joku persoona johon se liittyy. Se liitty olennaisesti niinkun mun äitiin ja se on hyvin saman tyyppinen ja tiiän et se vois olla olemassa. Ja danny on taas sitten.. Niinkun uraohjus ja mä tunnen kyllä tämmösiäkin henkilöitä tosi paljon, mutta ihan vaan sen takia, et se on early adapter of technologies, se niinkun tiputtaa sen uskottavuuspisteitä. Joo, uskottavaks menee, että se ois ensimmäinen mary, toinen ois susie ja kolmas ois danny. Uskotttavuusjärjestyksessä. Susien kans ihan vaan sen takia et tunnen todellakint än tyyppisen henkisön. Sen uskottavuutta syö, se ei oo ensimmäinen sen takia et tää on liian stereotyyppinen. Danny taas menettää uskottavuuspisteitä ton early adapterin takia et sen takia et siinä ei oo persoonaa juurikaan mukana, tai hyvin teknislähes.. teknislähestyvä. Joo.

Kuinka uskottavat noin yleisesti pidät noita?

Mitä tarkoitat yleisellä?

Sii sniinkun. Niinkun järjestyksen lisäks et onko noi uskottavia vai ei, koska järejstyshän voi olla ihan missä kohta skaalaa...

Joo, niin kyllä. Kyllä mä pitäsin näitä uskottavana. Tää niin vaikuttaa vähän et nää kaikki ois eri designerin tekemiä. Tai vaikuttaa koska nää on niin kaukana toisistaan tavallaan tää haarukka.

Millä tavalla kaukana toisistaan?

Ehkä siinä mielessä.. Se mitä täs on korostettu kaikissa ävhän eri asioita, mut ehkä se liittyy tähän suunnittelutehtävään myöskin et mikä on briiffi, et se ei tässä näy. Että.. Mun mielestä se niinku kontekstisidonnaisuus siihen briiffiin jollakin tasolla olis hyvä juttu, et tulis uskottavampi et.. Et sitä ois oikeesti selvitetty sen briiffin kautta. Tai.. Nyt tää niinku vaikuttaa vähän siltä että nää on niinku uskottavia, mut samalla nää vaikuttaa siltä et nää on kehitetty että briiffi on valmis.. et niikun briiffi on valmista. Mutta se johtuu ehkä myös siitä et mä en tunne teknologiaa vielä hirveen hyvin tohon hbbtv:hen liittyen. Mut joo, uskottavia kyllä. Ja helppo niinkun.. Uskottavuutta parntaa myös tosiaan se et enempi just nitä quoteja, ainakin toi yks, ja sitte kaikista ois kuvat. Mä uskosin et näin, et jos näist ois valokuvat ni se riittäs mulle todisteeks suunnittelijana ihan hyvin.

Joo. Mitä sä tarkotit sitä.. sillä et vaikuttaa siltä et nää on tehty ennen briiffin valmistumista tai briiffin tekoa?

Ehkä sitä, että tossa Maryn kohalla esimerkiksi puhuttiin sen teletextin käytöstä ja epg:Stä jatämmösestä asiasta.. et miten se niikun käyttää niitä. Mut sitten taas Dannyn kohdalla, Dannyn kohdalla niinkun ei puhuttu niistä samoista teknologioista edes. Et mentiin vaan et miten hän elää sitä omaa päiväänsä, mikä niikun oli tän otsikon tarkoitus tietysti. Mut sit samaan aikaan et niin.. Et ei vertailtu. Et se Susie käyttää pelkästään puhelinta ja läppäriä tääl jos oikeen muistan. Ni siin ei tavallaan.. Siin ei vertailtu sitä.. Se oli niikun apples and oranges tyyppinen tilanne mun mielestä. Et jos meil ois olemassa se briiffi, ni nää ois.. Puhuttas samoista teknologioista et miten niitten käyttö.. Mutta, se että jos ne on niinkun.. Kuvauksia niitten jokapäiväisestä elämäsätä... Sillon mä ehkä vähän niiku pyörrän omia puheita siinä myöskin että.. Että niinkun.. Mutta niinkun se kontekstisidonnaisuus siihen tiettyyn teknologiaan, se niinku auttas siinä suunnittelussa tosi paljon. Et sekin on jo tietoo, et jos niinkun ei oo.. ei katso televisiota ollenkaan. Se ei tee siitä yhtään vähemmän relevanttia persoonaa. Joo.

Vaikuttaaks tota siihen niinkun uskottavuuteen se mitä sä tiedät tai et tiedä persoonien tausta tai tekemisestä?

Mites sä nyt.. Annappas joku esimerkki tästä.

Siis.. Vaikuttaako siihen uskottavuuteen se, miten sä luu.. uskot ett nää on tehty, millä perusteell anää on luotu nää persoonat?

Kyllä tietyssä mielessä silleen että, mähän haluisin niinkun suunnittelijana ajatella että näähän on oikeeta persoonia tai niinkun oikeita ihmisiä. Et mä niinkun automaattisesti lähen rakentamaan sitä kuvaa päässäni siinä. Että, se ei oo ehkä niinkään merkityksellistä se että mikä niiden.. Kun.. Mikä niitten.. Ne perhetaustat on tai tämmöset asiat. Tottakai niikun lähiperheet ja tämmöset relevantit asiat kiinnostaa, mut et ei mun tarvii niinkun tietää sen historiikkiä syntymästä lähtien.

Nyt tarkotin muuten nyt persoonista puhe, niin näitä paperilappusia. Niin niiku tavallaan tätä metodia ja työkalua, eikä näitä henkilöitä. Eli persoonakuvausta eikä persoonaa.

Joo no sitte, ei oo mulla.. Mulla ei oo muuta ku et visut vaan kuntoon ni sit on kaikki hyvin. Uskottavuuteen vaikuttaa just se, että. Jos tää sais esimerkiksi. Jos mun pitäis jotenkin parantaa tätä itse, niin mä laittasin sinne hipauksen lisää.. Sipauksen lisää omaa persoonaa, esimerkiks niin se quote mikä siellä mun mielestä pitäs olla niiku tossa kohti [osoittaa persoonakuvauksen oikeaa yläkulmaa]. niin tota. Oikeessa yläkulmasssa for the record. Niin tota.. Mä esimerkiks krijottasin sen niikun quoten näiden persoonien omalla käsiallalla ni se vaikuttas niiku tosi paljon sen uskottavuuteen ja sitten yhdistettynä valokuvaan tai sit se illustraatio mikä siitä on tehty. Silläkin niikun sais jo uskottavuutta. Mut se niikun persoona puuttuu näistä suorista taulukoista.

No entäs sitten tota ku kysyit, että mihin tää perustuu, mitä dataa ni. Ni mä kysyn et mitä sä luulet siel olevan taustalla? Tai minkälaista dataa sä uskot näitten taustalla olevan? Ku nää on luotu sekundäärisestä datasta.

No nää ei selkeesti oo niinkun.. Jos, jos mun pitäs niikun yhtäkkiä päätellä et kenen kirjottamia nää on ja sit et sä kerrot että mä päättelisin kyllä, että tää on sekundaari tällai datasta suunniteltu. Osaisin päätellä sen sen takia että nää ei oo hirveen johdonmukaisia nää kertomukset esimerkiksi. Et se on niikun lyhyitä pätkiä ja sit seuraavaan asiaan. Mitä tietysti persoonassa onkin aina. Et siinä ei kaikkee haluta kertoo auki. Mutta... et se niinkun valheesta jää helpommin kiinni kun selittää liikoja. Et sen mää myönnän.. Samanlainen asia tässä. Et jostakin välistä sen niikun näkee, et siinä on vähän niikun ristiriitaisuutta joissakin asioissa.

Onks sulla esimerkkejä? Tai esimerkkiä?

No esimerkiks niikun ristiriitasta mun mielestä kuulostaa mitä niikun dannyn kohdalla, että on early adapter of technologies, et se ehkä haluais sanoo sen itse.. Mut mikä se niikun sepsifi mittari on siinä ollu. Kuka sen oikeesti on päätelly näin. Et onks tää niiku sen perusteella mitä nää on itse sanonu esimerkiks puhelinhaastattelussa vai onks täää niikun tutkijan kautta suunnittelijan niiku omia havaintoja. Et semmost mun mielestä tällä pitäs tulla ilmi. Et tässä ei nyt tuu sitä irti. Kuka tän on niikun tiedon keränny. Mut et niin varsinaisii ristiriitoja ehkä ei nyt suoraan.. Mutta semmosia niikun ajatusmallisia. Et jos on niikun tulee joku mielenmalli tästä tyypistä aina, rakens päässä, sit tuli joku yllättävä juttu. Mut ei niinkun suoranaisia ristiriitoja.

Tota. Miten sä uskot et noi persoonat on.. Tai, no ootas. Mitinpäs et mitäs mä olinkaan kysymässä. Niin, miten sä uskot tän taustalla olevan datan vaikuttaneen ton persoonien lumiseen, minkälaisiks ne on muodostunut?

Et jos on ollu sekundäärista dataa esimerkiks?

Niin

No yks esimerkki on just toi, mitä sanoin. Toinen on ehkä että siellä niikun helposti voi tulla semmonen toisto. Mä uskon että, täs voi olla tässä niikun kehitetty yks persoona kahdesta eri kyselytutkimuksesta esimerkiks ja on niikun mahdollista että korostetaan eri asioita. Ja sitte et jos se niikun briiffi on eläny siel taustalla, siinä helposti niikun lähtee rönsyilemään se persoona. Mikäs oli kysymys?

Että onks se vaikuttanut jotenkin persoonien luomiseen se data?

Kyl mä uskon et se on paljon vaikeempaa. Ihan niikun rehellisesti sanoen, mä uskosin että tämmönen persoona pitäs luoda heti kun se on haastateltu se persoona ja mielellään myösin sen henkilön toimesta joka on sitä haastatellu. Jos sitä persoonaa ei oo lähellekään olemassa ni sit se on ainakin.. Sitä ainakin ois mahollisimman pieni tiimi niikun tekemästä. Et ois ainakin mahdollisuus kysyy jostain että hei, et oliks tää sun havainto vai sanoiks joku oikeesti näin vai että mihin tää niikun tieto perustuu. Se ois jotenkin helpompi sille suunnittelijalle, persoonan suunnittelijalle. Näin tulee mieleen.

Luuleks sä et.. Tai siis, oisko nää sulle hyödyllisiä persoonia sun suunnittelutyössä?

Ois ihan varmasti tosi hyödyllisiä. Et jos toinen vaihtoehto on se, et mulle annetaan excel taulukko ni mä valitsen tän sata kertaa aina ennen sitä. Exceltaulukkobriiffit ei oo koskaan toiminut. Riippuu suunnittelijsats. Mä en oo millään tavalla matemaattisesti lahjakas tai loogisesti päättelyn mestari siinä mielssä et mun pitäs tujittaa numeroita ja tulla innovaatioita siitä. Ni ei onnistu. Et mulla on helpompi suunnitella ihmiselle.. Ihmiselle aina.. Kuin niinku käytännössä kerätylle datalle. Kyllä, oli vastaus. Ehdottomasti.

Joo. Siihen loppu itseasiassa mun kysymykset. Onks sulla jotain niikun kysymyksiä tai jotain mitä sä haluisit viel nostaa esiin, mitä ei ehkä juteltu läpi tarpeeks?

Joo, mun mielestä persoonia tehään ihan liian vähän. Muotoilua nähneenä ja niikun erilaista toimistoista nähneenä ni mun mielestä käytetään todella liian vähän. Ja jos niitä käytetään ne niikun helposti unohtuu sinne matkan varrella ja mua häiritsee se että persoonien merkitystä ei välttämättä tajuu muut ku muotoilijat. Tai niikun puhun designereista ylipäätään. Et ne ei niikun.. ne on heille varmasti hyödyllistä työkaluja, mut niitä ei ehkä osata arvostaa niikun päätöksentekoelimissä. Eli missä että persoonien luonti on joskus vaikeeta saada resursseja, et se ottaa oman aikansa, mtu et sit se on semmonen tilanne, että et se niikun nopeuttaa sitä suunnitteluprosessia tosi paljon myöhemmässä vaiheessa. Et sitä ei nähdä investointina. Mua häiritsee se asia. Tää perustuen edelliseen toimistoon. Siinä tärkeimmät. Ei tuu muuta mieleen. Kauheesti oli papereita.

No nyt on lippulappuja. Mikä fiilis jäi yleisesti noista persoonista?

Näist persoonista, näistä lapuista vai näistä persoonista. Puhutsa muuten sekasin niistä.

Tota täs nyt puhut.. Niiku.. Kielenkäyttö vaihtelee muutenkin kauheesti. Persoonakuvauksista, näistä.

Joo. Tota.. Miten mä oon tehny aikasemmin ite ni mä oon niiku lähteny luomaan sitä henkilö et mitä sillä on päällä, minkälaista korua sillä on tai.. Tällee niiku hyvin visu edellä. Ni se nyt johtuu siitä, et rajaa ollu tosi rajatusti koska ne käytännössä heitetään niiku omasta hatusta [epäselvä]. Mutta tota.. Et kyllä näistäkin niikun sais.. Tää on niiku data on tässä, mut nää ei näytä mun mielestä valmiilta sen takia et näitä ei oo taitettu mihinkään havainnollistavaan, mielikuvitusta herättävään muotoon. Toiki auttaa jo ihan hirveesti toi kikkarapäinen mary [Osoittaa itse persoonakuvaukseen piirtämäänsä tikku-ukko-kasvoihin].

Piirretty kuva.. Joo, hyvä. Hei, kiitoksia plajon.

Kiitos. Tämä oli mukavaa ränttiä.

Joo tää on siis supermielenkiintoista pois. Mä laitan nauhan pois päältä.