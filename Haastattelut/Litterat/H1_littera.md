..Ei siinä, ei oo mikään kauheen formaali haasttattelu. mulla on tossa runko, mut sillai tosi vapaasti jutustellen, jutustellen voidaan mennä. Pilottihaastattelussa meni just yli tunti. Puoltoist tuntia varasin kalenterista, varmaan päästään nopeemminkin ohi. Ja tää on tosiaan osa mun diplomityötä, jossa tutkitaan persoonia, joiden luomisessa ei ole käytetty käyttäjätutkimusta. Eli tutkitaan sitä, miten sellaisia persoonia voi luoda ja miten niihin suhtaudutaan käytössä. Ja täs haastattelun osana on myös pieni suunnittelutehtävä, mutta siinä ei arvioida sitten ollenkaan suunnitteiljan taitoja tai sitä lopputlosta. Se on hyvä muistaa erityisesti siinä. Joo, onko sulla tässä vaiheessa kysymyksiä tai kommentteja? Ei. Asiaa! Tota, käydään eka vähän sun taustoja, että mikä sun, tota... Kuka sä oot ja mitä sä teet? Mitä sun nykyiset työtehtävät on?

Tarviiks tietää nimet ja iät ja kaikki ja tämmöstä tietoo. Kuinka tarkaan sä haluut?

No siis mielellään riittää kyl myö surahistoria

Okei. Hmm. Siis mä oon valmistunut taiteiden maisteriksi pari vuotta sitten, melkein kaks vuotta sitten. Mut työelämän mä aloitin sillai oikeasti kolme vuotta sitten, eli sillon mä oon alottanut mun ekassa työpaikassa designerina. Ja se oli tälleen IT-alalla, pankki ja vakuutusfirma. Ja siel mä olin sitten reilu vuoden ja sit sen jälkeen mä oon menny tohon bränditoimistoon ja ollu siel creativena ja nyt mä oon täällä. Mut niinku kolme tommosta oikeaa alan työpaikkaa. Tottakai myös kesätöitä, mut mä en ehkä niitä niin tykkää laskea.

Tota, mitä sun duuniin kuulu designerina ja creativena?

No siin designerin duunissa, joka oli enemmän it-alan firma, niin siellä oli enempi erilaisten digitaalisten ratkasuiden konseptointia ja leiskausta ja suunnittelua. Siellä oli tosi laajasti kyl kaikkea, kun mä olin siellä ainut visuaalisen alan ihminen, niin  mä sain tehdä vähän kaikkea ja se oli myös uus firma eli niilläkään ei oiken ollut tietoa mistään mitään ja se oli myös mun eka työpaikka ja mäkään en oikeen tiennyt mistään mitään. Se oli paljon sellaista googlaila ja niinku asioiden selvittämistä. Ja sitten tota seuraavassa duunissa se oli enempi sit brändin rakentamiseen painottuva. Eli siellä tehtiin paljon webbileiskaa ja jonkinverran sfotaa. ei niinn paljon, et enempi se keskittyi brändin rakentamiseen liittyviin juttuihin. Mitäs muuta? No täällä siitten. Täällä teen aika paljon tota UI-settiä. Liittyy aika paljon tässä vaiheessa ainakin itellä konseptintiin. Koska palvelu on olemassa, mutta se on vähän kökkö. Siit pitää tehä uus versio. Suunnittelua muutenkin, kuin vain visuaalisesti.

Onks tota, tai siis missä määrin sä oot sitten ethnyt käyttäjälähötistä suunnittelua tai käyttäjätietoon pohjautuvaan suunnittelua?

No ehkä eniten tässä. Bränditoimistossa ei hirveesti niinkään välitetty välttämättä siitä, niinku loppukäyttäjästä. Siellä oli jonkinverran käyttäjätestasuta jos oli isompi kokonaisuus, mutta se oli aika paljn vain sitä että tehtiin uusi ilme asiakkaale. Se oli sitten siinä. Otsolla oli.. siis tässä mun ekassa työpaikssa.. sielläkin oli kyllä jonkinverran käyttäjätestausta, mut mä en oikeen muista kuinka paljon sitä otettiin sit loppupeleissä huomioon niinku suunnitteluvaiseessa. se oli tosi paljon niinku konseptointia, eli vähän niinkun kokeiltiin eri ideoita. Et siinä ei ehkä niin sit otettu viel niitä loppuaskelia huomioon tai loppukäyttäjää.

Niin Sanotko vielä mikä sun koulutusala on? sä oot taiteen masiteri, mutta mikä ala?

Teollinen muotoilu

Joo, tota... Eli sä oot nyt ollut tässä nykyisessä työssä mitä puolisen vuotta?

Marras Joulu tammi helmi maalis, eli viitisen kuukautta.

Sit tota persoonista. Ooks sä käyttänyt koskaan persoonia sun työn apuna?

Ehkä nyt niinku nyt on tullut jonkin verran ja se on tullut aika vahvasti niinku palvelumuotoilujan puolelta. Siellä on sit niinkun aika edistynyttä se homma, et se on luotu paljon erilaisia persoonakuvauksia ja sit erilaisia tarinoita, et miten ne käyttää tätä tiettyä palvelua mitä mä oon nyt tekemässä. Et sieltä puolen tulee sit aika paljon hyvää infoa.

Miten sä käytät niitä sitten sun työssä?

Mä otan ehkä niitä huomioon niinkun sisällön kannalta enemmän, eli sieltä käyttäjältä voi tulla se tarve, niinkun johonkin tiettyyn juttuun. Ja sit me yritetään sisällyttää se ite suunniteltuun tuotteeseen jollain tavalla. Ja ehkä sielt tulee myös ilmi sitä mitä ollaan mietitty aika paljon että millon se käyttäjä käytätä tota palvelua tai miksi se käyttää tai... Mut silleen tavallaan sitä alkuvaihetta ollaan puitu aika paljon niin ku siillai.

Mitä tarkoitat alkuvaiheella?

Tai silleen että, kun sä.. kun käyttäjä alkaa käyttää palvelua, et miten se.. mistä se lähtee se triggeri siihen palvelun käyttöön. Niinku silleen [epäselvää].

Joo, tota tota. Onks sul sitten muita projekteja tässä, sanotaan viimeisen kymmenen vuoden aikana, lukee haastattelurungossa, joko duunissa tai koulussa missä sää ot sit käyttänyt persoonia avuksi.

Joo, oon itseasiassa käyttänyt. Tota, mä oon ollu sillon koulssa. Meillä oli ollut palvelumuotoilun projekti, joka tehtiin Rovaniemen teatterille. Ni me luotiin siinä erilaisia persoonia, niin sit niinku hyödynnettiin niitä persoonia sit palvelun suunnittelussa.

Ja sillon s olit mukana luomassa niitä?

Joo

Onks muita kun nää kaks?

En oo kyl ihan varma. Ainakin on hyvin oon unohtanut, jos on ollu. Mut noi on ollut semmosia et niinku itelle tällä hetkellä niinku tärkeimmät tai mitkä muistaa persoonien käytöstä. Jossa ne on ollut oikeesti aika hyödyks

Millä lailla ne oli hyödyks? Molemmat oli hyviä kokemuksia vai?

Joo, tai tää nykynenhän on nyt viel aika vaiheessa et siitä ei vielä tiedä. Tulokset tulee sit ilmi. Täs Rovaniemen teatterikeississä se oli aika hyvä. Me luotiin ne persoonat silleen haastattelujen pohjalta, mitä tehtiin haastatteluita. Ja sit niistä muodostettiin kolme erilaista persoonaa, jotka käy.. tai niitten persoonien tavoitteena oli selvittää se että minkä perusteella ihmiset käy teatterissa. Eli siellä oli joku semmonen joka hakee kulkttuurikokemuksia ja sit oli joku semmonen tyyppi joka tykkää elämyksistä. Kolmatta en taida enää muistaa et mikähän se oli. Mut sit niistä me lähdettiin näyttelee sitä palvelupolkua läpi niinkun niitten henkilöitten avulla niin sit sielt sai huomattuu semmosii. ehkä jotain juttuja mitä ei ois muuten välttämättä sit.. Niin siin mielssä oli kyl hyvä.

Onks sul ollu jotain niinku huonoja kokemuksia tai niinku sen suuntasia kokemuksia persoonista?

No ei oikeestaan. Mä en oo niin.. tai en on oo viel niin paljook äyttäny et ois mitenkään hireen laaja kokemus persoonien käytöstä. Mut ei oo ainakaan vieä tullut sit semmosta et ahistais tai et ois huono. Et itelle tuntuu sit jollain tavalla luontevalta tavalta miettii asioita.

Joo, no siinä oikeastaan tulikin vastaus. seuraava kysymys ois ollu, et mitä mieltä oot persoonasta yleisesti työkalnua?

Joo. kyl. pidän. Tai mun mielestä ne on kyllä...

Joo, siin oli persoonataustan kartottelua ja seuraavaks sit ois tää pieni design tehtävä, jossa tarkoituksena on konseptoina pieni palvelu yhen persoonan pohjalta ja mulla on tehtävänanto siihen ja niinkun vähän taustamatskua. Ja ideana se että voit tekstillä tai piiroksilla kertoa siitä mulla, että minkälainen se konsepti oli. Ja aikaa on noin viistoista minuuttia, et se on semmonen nopee. Ja tosiaan tässä ei nyt arvioida sua suunnittelijana vaan näitä materiaaleja. Jees, eli siinä on tehtävänanto. Ja niin tää on tosiaan. Kontekstina se, että tää on TV:ssä käytettävä palvelu ja rajapintana on kaukosäädin. Ja siin on vähän siitä TV:stä matskua ja tossa on vielä ersoona jolle tää suunnittelutyö tehdään. Kysy vaan jos on kyksyttävää. Siinä on tarkemmin tehtävänanto siinä paperilla. Ja tota mulla on kyniä ja paperia eri sorttia, jos haluut käytää niitä sitten. Siinä vielä paperia.

[Aikaa kuluu, haastateltava lukee materiaaleja]

Siinä on vaan muistutukseks et miltä kaukosäädin näyttää.

[Aikaa kuluu, haastateltava lukee materiaaleja]

Eli minkälaisen loppuratkaisun sä haluut tästä tai lopputuleman?

Silla että sulla on jotain konkreettista näyttää siitä, niinku tekstiä tai piirustuksia siitä

Ja tää voi periaattessa voi lla ihan hömppää

Voi olla

Tän ei tartte olla mitään järkevää varsinaisesti

Nos sillai et suunnittelet silla oikesti

Joo

[Aikaa kuluu, haastateltava tekee tehtävää]

Tunnistaaks se.. Tää on vaan semmonen kysymys. Tänne nyt ollaan tekemässä nyt draamasarjaa. Niin, tällä on nää tv-lähetykseen voidaan silloin lakea mukaan myös niinkun pädit. tai sillain mietin et, perustuen tähän persoonakuvaukseen se katsoo pädeillä ni sitä ratkaisua voi miettiä voi miettiä tavallaan myös pädiin

Joo

Okei..

Saaks kysyy vielä tarkemmin vielä tosta HbbTV teknologioista. Eli mitäs nää kaikki mahdollisuuudet on

Siin on se HbbTV intro-paperi, jossa on niitä vähän valotettu, niin tota siinä on lopussa niitä vähän miihin niitä voisi käyttä änoin seimerkikis. Se on aika lavea, mutta voi käyttää luovuutta.

Ymmärtääks tää tämmönen ohjeistamista? Mahdollistaako HbbTV semmosta niinku.. Tai mä heti mietin mitä niinkun tulee tuolta persoonista hyvin ilmi. et se käyttäjä ei oo niinkun hirveen tuttu minkään teknologisten laitteiden kautta. ja siellä onkin just tosta että se katsoo pädillä noita jotain ohjelmia ja se ei osaa siirtää niitä tv:hen. Et pystyykö tän avulla.. Mä en oikeen ymmärrä tätä vielä.. ymmärtääks se semmosta et mä kaston pädillä et voiks se ohjeltaa sillai et sul ois se interaktio sillai et sä tunnistaa et sä katot pädillä. että hei, tiedätkö..

Se niinku.. No niinku HbbTV pohjautuu HTML:ään eli se tunnistaa millä sitä katsotaan.

No se on mun ensimmäinen ehdotus.. Tai tän vaiheen.

Ja nyt on sillai suurinpiirtein viis minuuttia aikaa.

Okei. No täähän on sitten [epäselvää]

[Aikaa kuluu, haastateltava lukee materiaaleja]

Onks se siinä?

Joo

No niin! okei!

Oli aika nopee toi aika

Se on, se on tosi nopee

Varsinkin kun oli aika paljon luettavaa

Joo, pilottihaastattelussa oli 10 minuuttia et sen huomas hyvin nopeesti et se ei riitä mihinkään

Siinä on se et ei oo kerenny ku lukee paperit läpi

Joo, mites sä konseptoit, ideoit?

Tosi nopeesti konseptoin täs semmosta palvelua, mitä tääl kävi ilmi, et se ei oo oikeen hirveen tuttu käyttämään mitään teknisiä vempaimia, minkä takia se tyytyy aika helposti siihen pädin käyttöön. Niin mä tein tänne tämmösen call to action -bling- kohdan, joka tunnistaa sen et sä käytät pädillä, joka sit vois ohjata sen käyttäjän siirtämään sen kuvan tv:seen. Ja mä laitoin tänne et se vois näyttää myös tätä kaukosäädintä ja sit "hei tääl on tää [epäselvää]" et paina sieltä et voit valita sieltä et tätä.. Ja sit mä laitoin myös tommosen. Se voi toimia myös ohjeistuksna, et sä voit nyt alkaa vaik äänittää tai semmosta. Helppokäyttöisyyttä. Mut tää oli mun mielestä ehkä tämmönen nopee viiden minuutin raktaisu.

Joo ihan hauska. Miten sä päädyit tohon? MIkä oli se prosessi.. miten sä lähit tohon tehtävään?

No aika pitkälle mä pohjasin sen nimenomaan tähän.. Luin tai mä ite tai mä.. Pain pointseja, varmaan toi teknologiaratkaisujen huono tuntemus. Niin aika lailla siihen. Kyl mä kävin tosi hyvin läpi nuo myös et saisinko mä jostain muusta revittyä ton idean, mut mun mielestä oi on ehkä semmonen.. Se on sen ongelmakohta mitä mä voisin helpottaa sen ongelmakohdan kanssa. [epäselvää, tauko] Joo.

Mitä hyötyä et sä koit saavas tosta persoonakuvauksesta noin yleisesti?

No siit saa sen kaikki kiinnostuksen kohteet ja no mulle toi oli tärkee toi niinku pain pointsit. Et tavallaan saa ne sit häivytettyä pois. Ja tottakai noi mihin se pyrkii toi käyttäjä. Ehkä hirveesti en saanut irti noista niinku... Tavallaan niinku.. Sen perheen kuvauksesta.. Tota.. Mitäs muuta.. Tää oli tosi pitkä. Tähän aikaan. Mut siis sillai enemmän kerkeis perehtyä ni varmaan sais enemmänkin irti. Nyt oli vähän semmonen, että luin kerran ja unohdin puolet tyylinen... [tauko] ja sit mä takerruin myös tähän sosiaaliseen tapaa käyttää tv:tä, koska se liittyi tohon tehtvänantoon. mä yritin siihen miettiä joku chattitoimintoa tai ku tääl oli nimenomana, tääl puhuttiin siitä et se on yhteydessä sen kavereihin ja Adam on sen mies ja jotenkin siihen yritin myös, mut en kerennyt. Mut se oli ihan hyvä niinku pointti.

No oliks tos sit jotain niinkun puu.. tai oliks toi persoona jollain tavalla puutteellinen?

No mä oon ite tosi visuaalinen, niin mä ehkä kaipaisin vähän enempi kuvia tai jonkinlaista.. Jotain visaulisointia. Nyt oli niinkun tosi tekstipainotteinen. Toki se on myös hyvä, mutta mä ite nään niin helpommin kuvien kautta, niin sit mä ehkä kaipasin...

Minkäalisia kuvia?

No musta olis kiva, jos mä tietäisin tän henkilön kasvot. En mä välttämättä sillä tiedolla mitään välttämättä tee, mut jotenkin se tekee siitä semmosen oikean. Tavallaan. Ja emmä tiiä mä tykkään vaan niinku ymmärtää enemmän kuvien kautta. Ja ehkä nopeammin. Kuvien kautta pystyy silali [viuhahdus]

Minkälaisiin kohtiin, jos niinku saisit vapaat kädet kuvitaa, niin mitä sä lähtisit tuolta visualisoimaan?

Ehkä vois.. En oo ihan varma oisko se järkevää tän tuotoksena vai tähän, semmonen tavallaan ehkä joku niinku palvelupolku, et miten sen vaikka päivä menee tai jotain semmosta.

Niin tällästä tarinaa.

Joo.

Minkä takia?

Jotenki se yksinkertaistaa. Ehkä. Helpompi hahmottaa.

Entä erosko tää persoona jollakin tavalla noista sun aiemmista persoonista, mitä sä oot nähny tai käyttänyt?

No, molemmat on ollut enemmän visuaalisia. Mut täs oli laajempi, ehkä jollain tavalla. Täällä on tosi monta eir niinku kohtaa, mitä ei oo ehkä aikasemmin ollut. Ja tosiaan noi persoonat mitä on nyt Koneella käytetty, ne menee sillai tarinamuodossa. Et siellä on eka.. Se on niinku powerpoint-esitys tavallaan, mitä siel käydään. Eli siellä on ekalla sivulla kerrotaan että tässä on henkilö joku. Ja henkilön ammatti on se ja hän on kiinnostunut siitä ja tästä. Ja myös että on semmonen niinku.. Mikskä sitä sanotaan.. et on semmonen kartta niinku...

Nelikenttä?

Niin semmonen just. Et sinne on vedetty alueita ja siellä mäpätään se henkilö tiettyyn alueeseen. Ja sitten siinä kerrotaan se henkilö tarinan kautta et miten sen päivä alkaa. Ja sit se tottakai liitetään myös siihen tuotteeseen, mitä me ollaa ny ttekemässä, mut siis sellään että päivä alkaa nyt näin. Hän miettii sitä ja tätä ja sitten hän tajuaa tätä ja tätä et siinä on niinkun joka slaidilla se tarina menee aina etenepäin, siinä on aina kuva. Se jotenkin tekee siitää.. Tai niinku tarinan. Et siin mielessä tää on erilainen. Tää on niinu. Mulle tää oon niinku tiiviste tai tiivistelmä, mikä on kyl siin mielessä hyvä et saa nopeesti tiedon henkilöstä. Mut joo. Aika laaja. Mut hyvä.

Miten sitten jos miettii uskottavuutta tai luotettavuuta, arvioiksä sitä tossa kun sä teet teet persoonaa. Eikun siis kun sä teet tota konseptia?

Eli mitenkä?

Arvioiks sä sitä, kuinka luotettava tai uskottava toi persoona on samalla kun sä käytit sitä

En. En kyl miettiny yhtään. Jotenkin myös ehkä tää tilanne on semmonen, et mun ei tarvinnut miettiä et onks tää henkilö nyt oikeesti todellinen tai.. ... Mulle tuntuu harjoitustehtävältä tavallaan, mä en ajatellut sitä siltä kannalta... oikeaan henkilöön.

Miten sitten käytännön työssä, ooks sä pohtinut sitä vai onkohan tullut vastaan et onkohan tää nyt paikkansapitävä vai..?

En oo kyl ihan varma oonks mä yhtään kyseenalaistanut sitä. Toisella kerralla mä olin ite tekemäässä sitä niin mä tiesin et ne on oikeita henkilöitä niin mun ei tarvinnut miettiä sitä et onks nää yhtään totuudenmukaisia. koneella niin sielläkin mä tiesin et ne on haastatteluiden pohjalta tehyt, et ehkä mun ei oo vaan tarvinnut miettiä sitä et onks nää oikeita vai ei. Ni ei oo tarvinnut sellasta.

Nyt tota mä teen tälläsen pienen keljun tempun ja otan tän pois sun eestä. Ja sit mä.. Jos sä haluut lyhyesti kuvailla minkälainen tyyppi tää oli tää mary, ihan omin sanoin?

Perheen äiti, ei osaa käytää teknologiaan. Pitää yhteyttä ystäviinsä, tai on sosiaalinen. Mitäs muuta. No keski-ikäinen, se varmaan kertoo jonkin verran ku suunnittelee et minkä ikäisille suunnitellaan. Ja tota.. Sit siinä oli myös siinä ammatti mainittu, mikä on mun mielestä hyvä tieto kun suunnitellaan.

Mikä se ammatti oli?

Sairaanhoitaja tai joku.. Sairas.. Oliko?

Sihteeri.

Sihteeri? Oliko? Ehkä mä sekotin sen siihen sairaalasarjaan

Tehtävänantoon, joo

Mut joo, se on kyl ihan hyvä tieto niinkun muutenkin. Ja sit sillä oli lapsia, mistä tulee aina semmonen että lapset yleensä auttaa vanhempia teknologian kanssa. Ihan hyvä pointti. Mitäs muuta.. Emmä muista.

Oliks siinä persoonassa jotain yllättävää tai yllättikö joku sut siinä?

Ei. Se jotenkin kuulosti aika semmoselta perus tyypiltä. Mut joo. Ei ollut mitään semmosta mikä ois vähän ollut outoa.

Tuliks sulla joku tuttu henkilö mieleen tai niinkun muistuttiko se jotakin tyyppiä jota sä tunnit?

No ei kyl. Ei oikeestaan. Ehkä niinku tollee, no joo. Löytyy varmaan aika montakin perheenäitiä, jotka kattoo telkkaria telkkarista komediaa ja sairaalasarjaa, mut sit ehkä.. Jotenkin semmonen, et ehkä.. Kutoo telkkarin äärellä tai soittelee kavereilleen yhtäaikaa, se on ehk vähän semmonen et en mä ehkä tunne. Mut joo. Ei ehkä kuitenkaan.

Joo. Katon täs ettei jäänyt mitään kysymättä. Joo. Ei. Tota. Sit täs ois vielä vähän luettavaa. Kaksi persoonaa lisää. Ei tartte niinkun tarkkaan lukea, mut sillai selaat läpi ja vilkaset vähän. Lukaset läpi, tä äon ehkä se sanamuoto. Niin sit jutellaan vähän näistä kolmesta. Täs on viel tää Mary sitten vielkä.

[Haastateltava lukee persoonia]

Ah, mä näköjään hyppäsin ton asuinalueen ihan kokonaan tota..

[Haastateltava lukee persoonia]

Huomasiks sä jotain eroja näitten persoonien välillä?

Joo aika selvästi

Aika paljon

minkälasta?

No aika selkeesti nään tän mun mielessä tälläsenä keski-ikäisenä miehenä, joka katsoo uutisia. Sit tää [Susie] on enempi niinkun itelleen samaistuttavampaa itelleen iän takia, enemmän kun takia. Elämäntilanneki on enemmän tää ku tää. On ihan selkeet erot kyllä. Ja sit myös tähän verrattavissa ihan selkeet erot. Just niinkun.. Just niinkun TV:n ja netin käyttötaitona. Joo. Hyvin selkeetä.

Onks jotain muita eroja ku nää persoonahahmojen erot? Nouseeks esiin mitään?

No varmaan noi pain pointsit on aika erilaiset kaikki. No kaikki on eriä. Näitä mä en kerennyt ihan kunnolla lukemaan [Susie & danny] Joo, on ne kyl ihan niinku eri settiä ja eri hahmoja. Erilaiset tarpeet.

Mut enemmän erot on siin niinku persoonissa henkilöinä tai hahmoina

No emmä tiiä onks ne pelkästään ne persoonat. No tottakai mä nään ne erilaisina, mut on käyttötavat selkeesti erilaiset et jokainen hakee vähän eri juttuja. Erilaiset tavat käyttää.

Oliks noissa persoonissa sitten jotain yllättävää? Tossa Maryssa ei ollu

Ei, joo ei siinä ollu. Tota. [epäselvää] ...tutustuu. Mut ei äkkiseltään iskenyt silmään.

Mites jos sun pitäs arvioida noiden persoonien uskottavuutta ja sun pitäs laittaa ne järjestykseen sen mukaan, miten sä uskot noitten vastaavan todellisuutta. Niin mihin järjestykseen sä laittasit ne?

Jotenkin tää ehkä tuntuu uskottavimmilta [Mary]. Ainoastaans e ehkä neulominen tuntuu.. Mut ehkä ne jotkut tekee sitä. Sitte.. Sitten tota.. Näistä on itseasiassa aika vaikee.. Mul on jotenkin.. Mä oon ite ollu IT-firmassa töissä. Ja tästä tulee sellanen fiilis, että se on tosi ajan tasalla kaikesta. Ja sit kun vertaa omiin kokemuksiin, ja sit ku vertaa omiin kokemuksiin ni on sit... tai siis siel tuntu et nää on jotenkin aika.. miten sen nyt sanois.. ei niin ajassa kiinni ehkä, ku mitä tää henkilö on. Toki voi varmaan olla ja tääl oli tosiaan se et se oli project manager et ei pelkkä koodari. Ja täs mä ehkä vähän mietin tota et täällä on tää, et katsoo telkkaria.. tai lempäsarjat liittyy fitnessiin ja wellbeing liittyviä. Yheksäntoistavuotias.. Ehkä. Noi kaikki muut menee kyl ihan heittämällä, reality show ja soap opera ja sisustus ja elokuvat ja draamat. Toi on vähän semmonen onks se vähän tai.. Että onks se vaan nakattu sinne.

Mihin sä vertaat tota.. tai niinkun millä sä arvioit sitä niinku uskottavuutta? tai millä perusteella ne ei ehkä sopis siihen persoonaan?

En tiiä oliks täällä sitte mitään esimerkiks siitä et se on kiinnostunut niinku daily lifessä mistään liikunnasta tai terveydestä et se tuntuu vähän siltä et miks sä sit katot niitä jos sä et harrasta tai oo kiinnostunut. ni sen takia. Mut nii, en tosiaan tiiä kumpi näistä on uskottavampi. Ehkä tää, jos vedetään pois toi. [Lopullinen järjestys Mary, Susie, Danny]

Kuinka uskotavina noin ylipäätänsä sä pidit noita hahmoina?

Kyl ne on varmaan aika. Apaut. Ehkä jotenkin, ehkä jotain kohtia jotka ei oikein tunni ihan oikeilta. Mut pääpiirteittäin varmaan ihan mahdollisia.

Mitä kohtia? Oliks muita ku tää wellbeing ja fitenss?

Täällä just se, kun mun kokemus pohjaa IT-alan ihmisiin oli jäykkiä ja kankeita ja vähän vanhanaikaisia

Sä vertaat niitä vähän omiin kokemuksiin

Niin. Toki se on vaan yks firma ja niinku. Tää on ehkä neljä... Ei kl se on apaut saman ikäsiä. Mutta joo.

Mitkä tekijät sit teki niistä.. Tai mitkä tekijät vaikutti siihen uskottavuuteen?

Tässä?

Yleisesti niissä, vois myös käydä jonkun tietyn persoonan kautta.

No nyt en oo ihan varma.. En tiiä. Ehkä se, et jotenkin koko kokonaisuudesta tulee joku semmonen tietty mielikuva.

Onks se joku yhtenäinen.. yhtenäisyys vai mikä kokonaisuudessa?

Tää.. Tosi vaikee sanoa. Jotenkin varmaan pystys vastaamaan paremmin jos ois kerennyt käymään nää paremmin. Nyt on vähän sillai et mitäs täällä oikeen olikaan, ei oikeen muista. Emmä tiedä. En tiedä. Ehkä sen löytää, jotain ihan pikku juttuja, mitkä voi olla vähän sillai outoja. Mut pääpiirteittäin kyl ihan uskottavia.

Entä sit vaikuttiko sun tohon arviointiin toi tausta, että noissa ei ole käyttäjä tutkimusta taustalla?

No ei. Ei kyllä oikeestaan.

Minkälaisista tietolähteistä tai taustoista sä uskot tai luulet et noi on kerätty

Mä luulen kyl. Mä luulen et mä oon nähny kyl..

Siinä mielessä hankala haastattelupaikka. Älä nyt tässä vaiheessa kato sinne.

Onks se viel siinä?

On.

Mut siis mä luulen tän perusteella.. Tai itseasiassa mä en tiedä mistä sä oot keräännyt nää. Mut nää on varmaan yhdistettyjä tietoja

Luuleksä et se on jotenkin vaikuttanut noihin persooniin et minkälaisia niistä on tullut tai?

Kyl varmana. Joo

Millä tavalla?

Jos löytää niinkun tietyn nipun saman tyylisiä ihmisiä, sielt varmaan pystyy hakee niitä piirteitä jotka on ominaisua useammallekkin ihmiselle, jotka voidaan sit tuoda tänne. Mut joo, ehkä niin. Mut en kyl ihan keksi ihan mistä noi on napattu.

Onks se hyvä vai huono juttu.. sä sanoit täs niinkun piirteitä on useammalta ihmiseltä joita voi nappaa tonne?

Mun mielestä se on hyvä juttu koska sillon myös se henkilö joka näitä kattoo voi joko samaistua tai kokea, että mä tiedän yhen tän tapaisen tyypin ja sit sieltä onkia lisää. Eli on mun mielestä hyvä juttu siinä mielessä vähän yleistää.

Nii et sulla persoonat toimii ideoin. tai ketä muita mä tunnen tällasia tyyppejä?

Joo tavallaan. Ja sit se tavallaan auttaa antamaan sille henkilölle kasvot, varsinkin täs tapauksessa kun niitä ei oo määritelty. Ja sit ehkä hakee sillai.. Mä aloin ihan automaattisesti, joo it-firma sit mä aloin sillai et entiset työkaverin [epäselvä] lähti rullaa läpi ja et semmonen et kuka istuu tähän. Ja ehkä sen takia tulee jostakin et ei, ei se kyllä näin. Mikä on siis ihan hölmää siinä mielessä, että mä niin vahvasti lyön tän johonkin tiettyyn henkilöön.

Luuleksä et se vaikuttaa jotenkin niinkun niitten käytännön käyttöön toi niinkun niitten muodostustapa?

Eli mitäs tarkoitat?

Nii et vaikuttaako se jossain määrin merkittävästi noitten siihen että voiko niitä käyttää?

Ai se et mä ite alan..?

Ei. Siis et noi on muodostettu ei käyttäjätutkimuksen perusteella.

Ei. Ei välttämättä. Kyl ne on mun mielestä on kuitenkin suunnittelulle ihan hyvänä tukena. [epäselvää] ...jos mä oisin saanu ton tehtävänannon ilman käyttäjäpersoonaa ni.. Sit mä oisin miettiny miettimään et mitä minä haluaan. Ee sillai minä olen se tärkein täss. Et mä aloin nyt heti sit pohjaamaan tohon persoonaan.

[Epäselvää] muuten, et sanoit että "keski-ikäiselle pitää tehä tiettyjä juttuja" niin mitä sä sillä tarkoitit

No ehkä niitten.. Tai mä miellän että monet keski-ikäiset voi olla sit vähän kömpelömpiä käyttämään palveluita et nuoret aika.. tai niinkun nappaa ne helpommin omakseen tai.. Eli pitää tehä yksinkertaista.

Ja toinen mitä sanoit noihin liittyen et nää oli aika pitkiä?

Joo

Et mitä ajatuksia siihen? Et onks se hyvä juttu vai huono juttu vai joku sekotus

Ei siinä et se on ihan hyvä juttu jos on enemmän aikaa et se oli ehkä tähän aikaan liittyvä et ei oikeen kerennyt kunnolla lukemaan läpi. Mut jos mä tekisin ojtain oikeaa suunnittelutyötä, et ei tää olis mikään ongelma. Sillon se vois melkeen olla pidempikin kun on aikaa perehtyä.

Onks sulla tossa viaheesa tässä vaiheessa tullu.. tai siis mulla rupee niinku kysymykset loppumaan. Et tota onks sulla jotain mitä sä haluisit nostaa mitä ei olla käsitelty vielä tai omia kysymyksiä?

Ei nyt ehkä. Ei nyt heti.

Joo. Mikä fiilis jäi noista persoonista?

Joo, ihan hyvä. Ihan hyvä suunnittelun tuki.

Joo. Ei kai siinä sitten. Minä kiitän ja laitan nauhoituksen pois päältä.