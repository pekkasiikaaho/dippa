# Memo 16.2. - Tapaaminen Markon kanssa vallilan toimistolla

# Pääpointit

"Kyllä tästä vielä hyvä nelosen dippa saadaan" -Marko

Sekundaarinen data on ok. Pitää kirjoittaa hyvä kuvaus sen keräämisen prosessista niin dipan vaatimukset täyttyy.

Seuraavaksi pitää tehdä sekundaarisesta datasta synteesi, jotta saadaan tuotoksia, joita voidaan arvioida haastatteluissa.

Haastatteluista pitää tehdä litterointi ja siihen hyvä laadullinen analyysi. Tässä kvali-tutkimuksen juju. TAMS Analyzer on hyvä softa sitä varten mäkille. Myös post-itit toimii, jos prosessi on riittävän hyvä ja tarkkaan kuvattu.

Seuraava tapaaminen Markon kanssa 29.3., jolloin haastattelut pitäisi olla tehtynä. Aiheena analyysin suorittaminen.

## TODO

1. Datan synteesi, lopputuloksena persoonakuvauksia (W7&W8)
2. Haastattelun suunnittelu (W9)
3. Haastatteluiden tekeminen: maaliskuun 2.3. viikko (W10&W11)
4. Litteroi haastattelut
5. Analysoi haastattelut

# Persoonien syntetisointi

Dipan kinkkinen vaihe. Toisaalta myös dipan ehkä olennaisin asia.

Pitäisi selvittää mikä on tyypillinen persoonakuvaus ja miten sekudaanridatan pohjalta voisi luoda semmoisen.

Avoin kysymys:

> *Miten valitaan joukko persoonia tätä kontekstia varten?*

Yksi vaihtoehto on käyttää Technology Diffusion modelia tai lead user teoriaa ryhmittelyn tekemiseen.

Kun persoonaryhmittely on tehty, on helpompi osuus yhdistää lähdemateriaalin data näihin ryhmiin.


# Haastattelut

4-6 teemahaastattelua firman sisällä, 1,5h per haastattelu

Haastatteluiden pointtina on selvittää:

1. Onko persoona suunnittelijan mielestä hyvä?
	- Mikä siinä on hyvää?
	- Mikä ei?
	- Dekonstruoidaan luodut persoonakuvaukset
2. Miksi persoonat on hyvä/huonoja?
	- Tästä saadaan yleistettävyys muihin konteksteihin

## Haastattelurunko

- Avoin teemahaastattelu
- Lopussa checklist käsiteltävistä teemoista, nousiko
	+ Jos teema ei noussut itsestään, voi sen nostaa keskusteluun, kunhan nostamisesta mainitsee metodikuvauksessa

## Haastatteluiden määrän vaihteohdot

4+ haastattelua projekin suunnittelijoista ja heidän näkökulmistaan persooniin

3+3 haastattelua projektin sisällä olevista suunnittelijoista ja projektin ulkopuolisista suunnittelijoista


# Haastatteluiden analyysi

Haastattelut tulisi litteroida. Se on "helppo" tapa saada kvalidata analysoitua tieteellisesti uskottavalla tavalla.

Tekstinpätkien luokittelu/labelointi

Luokitteluperusteet syntyvät osin kirjallisuuden pohjalta ja osin emergentisti haastatteluista.

