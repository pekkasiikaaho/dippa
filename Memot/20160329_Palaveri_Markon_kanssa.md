# Agenda

- Dipan aikataulu
	+ Viralliset DL:t
		* Aiheen hyväksyminen
		* Kansitus
		* Kurssisuoritukset oodissa
	+ Epäviralliset DL:t
		* 1. versio
			- Sisältö?
		* Millon palaute/kommentit mulle?
		* 2. versio
- Analyysivinkit
	+ Tutkimuskysymysten täsmäys. Missä vaiheessa? Miten? Millä vapaudella?
- Kirjoitusvinkit
- Mitkä on parhaat tavat helpottaa tekemistä?
- Mitkä on helpoimmat virheet välttää dipan tekemisessä?

# Memo

- On ok, että muokataan siihen, että onko ne suunnittelijoiden mielestä luotettavia
- Miten on noussut es
- Pohdintaan
	+ Onko nämä todenmukaisia? (jää varsinaisen tutkimuksen ulkopuolelle)
	+ Vaikuttaako persoonien samaistuttavuus ohjelmoijien persoonakäyttöön?
- Hypoteesi tulevaisuuteen
	+ Oikea käyttäjien tunteminen on tärkeä osa laatua
	+ Onko oikotietä ohi oman käyttäjätutkimuksen?
- Next steps
	+ Tarkennettu kirjallisuuskatsaus
	+ Tsekkaa Katjan persoonakokemus
- Vinkit
	+ Tärkeää on miten analyysi suhteutetaan kirjallisuuteen
		+ Miten tulokset vertautuu ajankohtaiseen akateemiseen kirjallisuuteen?
		+ Miten minun aineistoni indikoi jotain sellaista mitä ajankohtaisessa kirjallisuudessa on keskusteltu inkonkluusivisesti
		+ Mitä minun aineisto voi tuoda uutta alalle
		+ Onko aineistossa jotakin sellasta, joka osuu alan tutkijoiden hermoon
		+ Jonkin sellaisen esittäminen, jota ei suoraan löydy olemassaolevasta kirjallisuudesta (ei vastausta, mutta vinkkejä)
	- Metodologisesti oikein
		+ Tarkka akateeminen perustelu
* Persoonadippa
	- Kristoffer Vasara
* Analyysissä voi tuoda uuttakin kirjallisuutta
	- Spesifiä vertailua voi tehdä analyysissä
	- Perusteet relate researchissa
* Aikataulu
	- Eka versio
		- Helatorstai perjantaiaamu
		- Ensimmäinen ehdotus kansiin menevästä versiosta
	- Aiheen hyväksyntä
		+ ASAP
	- Helatorstain versio