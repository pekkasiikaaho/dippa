# Deadlinet

4.6. Kansissa
1.5. Lähes valmis versio, jonka pohjalta voi tehdä kypsyysnäytteen ja diplomiesitelmän.

# Ohjaajan velvollisuudet

Yhden kappaleen lausunto itsenäisestä työnteosta ja lyhyt lausunto siitä, mitä hyötyä Leadinille tuli dipasta.

# Tutkimuskysymyksiä

Minkälaiseen tietopohjaan persoonan rakennus voi perustua?

# Evaluointia

- Persoonien käyttö UI Designin pohjana
- Haastattelut loppukäyttäjien kanssa?
- Miten kehittäjät voi hyödyntää persoonia?

# TODO

- Kirjoita oma kuvaus diplomityöstä
	- "Minkälaisen rekrytointi-ilmoituksen kirjoittaisit diplomityön tekijälle"
	- Teeman asetus
	- Käytännössä intron alku
* Kirjoita sisällysluettelo
	- IMRAD + Related Research + Conclusions
	- A = Analysis, ei And
* Mitkä ovat akateemiset lähteet? Mistä lähteä liikkeelle?
* Mistä aineisto?
	- Miten persoonia muodostetaan datan pohjalta?
* Mitä uutta dippa tuo kirjallisuuteen? Mikä on uutta persoonakuvaukset muodostukset metodissa

# TODO 2

- Lue 2-4 diplomityötä
