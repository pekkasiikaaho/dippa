sources = ./Persona_Foundations/*.md ./Memot/*.md ${dippa}
dippa = ./Luvut/*.md
target = ./dest/dippa.docx
review = ./dest/dippa-for-review.docx
reference = "./reference.docx"
bibliography = ../viitteet.bib


all: commit-changes build watch

install:
	brew install entr pandoc

build-old:
	find ${dippa} \
	| xargs cat \
	| pandoc --filter pandoc-citeproc --from markdown --to docx -o ${target} --reference-docx=${reference} --bibliography=${bibliography} --toc \

build:
	find ${dippa} \
	| xargs cat \
	| grep -vE '^(!?\t)' \
	| pandoc --filter pandoc-citeproc --from markdown --to docx -o ${target} --reference-docx=${reference} --bibliography=${bibliography} --toc


commit-changes:
	find ${sources} \
	| xargs git add \
	&& git commit -am "[automated build]" \
	|| echo "No changes to commit"

watch:
	find ${sources} | entr make commit-changes build wc-stats

wc:
	find ${dippa} \
	| xargs cat \
	| grep '^\S' | grep '^[^>#]' \
	| wc -w

wc-stats:
	find ${dippa} \
	| xargs cat \
	| grep '^\S' | grep '^[^>#]' \
	| wc -w \
	| xargs echo "$(shell date);" >> "wc.csv"