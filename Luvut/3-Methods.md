
# 3. Methods

***TODO***

- Personan luonnin prosessin luonti VAI käytetyn prosesin case kuvaus
- Datan rajaus
- Datan keruu
- Mikä sana löydetyille ryhmittelyille?

***TODO Haastattelut***

- Suunnittele
	+ Ketä?
		* 3-5 Leadinilaista
	+ Miksi?
	+ Miten?
	+ Milloin?
- Fokusryhmä?

----

> Tämän kappaleen rakenne, 2-5 lausetta
> 
> Oman persoonaluontiprosessin esittely
>
> Miten tehty, minkälaisesta datasta ja miten arvioitu
>
> Miten vastaan tutkimuskysymykseen
>
> Viitteitä metodien tuleksi sinne tänne
>
> Mihin pohjaa nää metodit, persoonien muodostus ja evaluointi, datan analyysi
>
> Kerrotaan myös miten analysoidaan dataa ja miksi analysoidaan juuri niin, grounded theory tms tapa
>
> Strauss & Corbin


## 3.1 Persona data collection

User demographics and data are garthered from public research, public data sources and from partners. Possible data sources include HbbTV research, TV usage statistics, hybrid television usage research, open data libraries from public entities in Finland and abroad, TV marketing research.

> Taustaperustelu datan etsimiselle

@pruitt_persona_2006 recommend (*TODO MITÄ?*)

> Kertaus Related research osion persona formation process-osiosta

To produce personas, an extensive literature search of 48 relevant search keyword combinations (e.g. HbbTV persona, HbbTV media usage, “interactive TV” media consumption etc.). Main focus was to find sources that grouped users based on their media consumption or behaviour and had the users in North America or Europe. Articles that provided supporting findings were also collected, but they were not used to form the personas. Top 50 search results for each keyword combination were reviewed for further review based on article title. Based on quick review, the most promising studies were selected for full study. In addition to scholarly literature, commercial reports were searched for supporting data.

> Koko hakusanalista näkyviin?
>
> Promising millä perusteella?

## 3.2 Persona formation

> Onko tarpeen olla eri otsikon alla?

In the end, four high quality and two medium quality studies with user groupings based on media use and behavior were found and numerous other articles with supporting data. The six studies each had one grouping of users. As recommended by persona generation best practices, the six different user groups were analyzed and combined with help of affinity mapping. The found user groups were initially mapped on two axises: the intensity and amount of their media use and their technological skills set. The affinity mapping produced seven different clusters of user groups, which were combined to form proto-personas.

Based on the proto personas, two user groups were discarded. The first discarded proto-persona did not use television media at all and was not interested in it. The second discarded proto-persona was discarded on the basis that it was a very small user group with little use of traditional media. The five remaining proto-personas were developed further to form personas.

> @putnam_bridging_2010 used data grouping methods (*TODO TÄYDENNÄ*) to find the initial groups to flesh out the personas.

	#### Cooper [@cooper_inmates_1999]

	"We give every project its own cast of characters, which consists of anywhere from 3 to 12 unique personas. We don't design for all of them, but they are all useful for articulating the user population. Some are defined only to make it clear that we are not designing for them."

	"To be primary, a persona is someone who must be satisfied but who cannot be satisfied with an interface designed for any other persona"

	"If we find more than three primary personas, it means that our problem set is too large and that we are trying to accomplish too much at one time"

	"The personas we invent are unique for each project. Occasionally, we can borrow from previous projects, but because precision is the vital key, it is rare to find two personas exactly alike."

	#### Persona lifecycle [@pruitt_persona_2006]:

	@pruitt_persona_2006 recommends (*TODO MITÄ SUOSITTELEE?*) to group personas.

	"Step 1: Identify important categories of users. If you can, identify categories of users that are important to your business and product domain. Identifying these categories now (even if they are based solely on assumptions) will help you structure your data process- ing and build a bridge between the ways people think of users today and the data-driven personas you will create."

	"We have found that roughly three to five personas is a good number to target"

	"During the conception and gestation phase, your goal is to create a set of personas that are:
	● Relevant to your product and your business goals
	● Based on data and/or clearly identified assumptions
	● Engaging, enlightening, and even inspiring to your organization"

	"Note that your goal is not to describe every possible user or user type, nor to detail every aspect of your target users’ lives."


	"If these categories of users are not clear from your work in the family planning phase, hold a meeting with your stakeholders to make this notion explicit. Plan to meet for about an hour to discuss preexisting categories of users used in your company, as well as roles, goals, and segments to determine the best strategic target groups for your product. The following is a recommended agenda for the user categories meeting.

	1. Describe the goals, rules, and outcome of the meeting.
	2. Discuss preexisting language for customers and users at your company.
	3. Discuss the concept of roles, goals, and segments.
	4. Brainstorm on your company’s most appropriate categorization scheme or consider generating assumption personas (introduced in Chapter 3 and further discussed in material to follow).
	5. Generate the proposed high-level groups or categories of users.
	6. Get consensus on the high-level target groups before closing (you will want roughly three
	to five groups)."

	#### Nielsen's Personas [@nielsen_personas:_2013]:

	Secondary data use pp. 44-43

	"Systems of coordinates combine relationships and contrasts and can also show how many belong to each group. ... I deduce the number of personas by means of a system of axes stating differences and that has room for many dimensions."


	> Other persona literature says (*TODO MITÄ SANOO?*)

	> Lead user theory (*TODO LÄHDE*) finds potential users in this manner.


### Process used for the persona formation

As was recommended by @pruitt_persona_2006 and @nielsen_personas_2014, affinity mapping was used to cluster the personas together.

@nielsen_personas_2014 recommends using two or more axises to sort the initial persona profiles. The found user groups were initially ordered by two axises: the intensity and amount of their media use and their technological skills set.

#### The process used to create the personas

4 high quality and 2 medium quality TV consumer segmentations / personas were found.

Numerous articles to support fleshing out peronas.

	> In a number of studies ([@courtois_second_2012; ] *TODO MITKÄ, LÄHTEET, TÄHÄN*) there were no significant demographic differences between the users. The defining charasteristics were either their media consumption (*TODO LÄHDE TÄHÄN*) or the intensity of their media interaction (*TODO LÄHDE TÄHÄN, JOKU MUU EROTTAVA TEKIJÄ TÄHÄN*).




## 3.3 Evaluation of personas

	> Kysymys mikä vielä jää hieman avoimeksi on se miten varmistetaan persoonien paikkansapitävyys. Se että kysytään meidän suunnittelijoilta on hyvä kuvaamaan kuinka käyttökelpoiseksi he kokevat persoonan sisällön/formaatin (tut. Kys. 3.). Se ei kuitenkaan kerro sitä kuinka luotettava representaatio persoona on todellisuudesta (tut. kys 2. ). Ehkä tällä aikataululla se että linkkaa persoonan mahdollisimman hyvin dataan ja näyttää (visualisoi?) sen hyvin on paras tapa kertoa luotettavuudesta. Sanoisin että jos löytää tuohon jonkun hyvän tavan on se jo sellaisenaan hyvä kontrbuutio. (voisiko olla jopa dipan pääkontribuutio?)

Six interviews were be conducted with Leadin UX designers. Three of the interviewees were familiar with the persona context and three were not. The UCD experience of designers varied.

Description of interviewees can be found in Table X (TODO)

Used measures: (TODO)

- Do the personas communicate relevant user insight to the designers to base their design on?
- Do designers feel confident continuing with the design task based on the personas?
- Do designers feel that the personas were reliable?

One researcher attended each session. The interviews were audio recorded. The transcripts were analysed with preliminary coding that was during the transcription process.

	[@matthews_how_2012] Kirjoittaa hyvin haastattelun analysointiprosessista.:

	Two researchers attended each session, one leading and the other taking detailed notes. Study sessions were audio recorded. Loose transcripts and notes from the sessions were initially analyzed using open coding. We next examined our initial codes and data to identify and formalize recurring themes, which represented meaningful aspects of the empirical material that occurred repeatedly. For example, one theme was that only a few, distinctly different perceptions of personas were expressed repeatedly and we discovered that we could categorize participants by these repeated viewpoints. We performed a second coding pass through the data focused on the formalized themes. Two analysts who had conducted the interviews together went through the themes with examples. Where there was disagreement they discussed the themes and examples until agreement was reached. Our presentation of results focuses on these themes, including quotes that support each theme and illustrate important nuances.

> Haastateltavien kuvaus:
> Kokemus jne
