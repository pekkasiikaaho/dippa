
# 5. Analysis

> Tämän kappaleen rakenne, 2-5 lausetta

> Do the results answer the research questions? What are the answers?
>
> Sources of the success/failure of personas?
> Data sources
> Persona generation process
> Test setting or other external factors?
> Something else?

## 5.1 UX Designers' response to personas

## 5.2 Factors affecting perceived reliability of personas

	[@mulder_user_2007]

	Your coworkers will be willing to use personas as decision-making tools only if they believe the personas were created with objectivity and at least some science.

	[@nielsen_engaging_2004]

	To make a decision is to dismiss other choices and the overall impression is that the method forces enlightened choices based on a solid foundation of user inquiries, but the need for validation and testing of the choices occurred. This perception created an awareness of the importance of user participation in the process. Both personas and scenarios need to be tested against real users.

	Ideas for future development of the method and how users could be involved in the development and design process were created. All participants seem to want user participation in the process. The group has discussed how to achieve this and have tried to arrange sessions but apparently without success.

	Participation in the workshops rather than the written personas and scenarios was considered to be of most value.

	The written personas and scenarios descriptions are not considered to be of value to outsiders unless they are familiar with the methods. An outsider can interpret the personas and scenarios as they like, and D. points to another factor that can influence the reading process: the reader might be afraid to acknowledge the user description if they do not have any experience with IT.

	Even though it is the participation that is most praised, the personas and scenarios are now part of the daily routines and when they consider future developments.

	For an outsider who did not participate in the discussions, like N., the decisions are impenetrable and in the reading process it was difficult to understand the arguments behind the choices.

	As the method is intense and discussions play an important part in the process of creation those not involved in the process might find it difficult to understand the written description.

	[@pruitt_persona_2006]

	As we have stated several times previously, we believe very strongly that personas should be based on data. Even the perception that the personas are not based on data can damage their credibility and utility. However, it is practically unavoidable that some elements of personas are generated from educated guesses or are simply made up. Your job in the conception and gestation phase is to make informed decisions about how much fiction and storytelling is needed to make your per- sonas feel real and be truly engaging.

	[@matthews_how_2012]

	However, designers did require continuous access and immersion in user data for their own design activities. Most participants seemed to prefer a less personified abstraction of user data focusing on role-based information to support their design work.

	[@putnam_bridging_2010]

	In other words, if this had been an audience in which they held many (strong) beliefs, the personas may have been less effective at (1) enticing surprise and (2) at convincing the participants that their previous assumptions were ill-conceived.


	Furthermore, this is evidence that perceptions about the creation of the personas can be an influential variable to Designers finding personas as effective summarizations of research.


	Combined, these findings suggest that Designers generally want to know about the research and that they were not impressed with quantitative methods. In fact, the term 'statistical significance' was either meaningless or had a negative connotation implying impersonal number crunching for four of the ten study participants.


### Positive effects

### Negative effects

### Social identifiability

	[@nielsen_engaging_2004]

	There are diverse opinions about how thorough the pictures of the users they painted were during the workshop. Some find that they can easily recognise the three personas, others that they are too stereotyped and that without the discussions they would not have worked. But the participants agree that the focus on concrete but fictitious users was of value

## 5.3 Possibly a chapter about a prominent finding - e.g. The social identiafibility of personas as a driver of perceived reliability

## 5.4 Effects of secondary data on persona creation

## 5.5 Effects of secondary data on perceived reliability
