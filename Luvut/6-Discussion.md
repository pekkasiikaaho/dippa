# 6. Discussion

> Tämän kappaleen rakenne, 2-5 lausetta

> What might the answer imply and why does it matter? How does it fit in with what other researchers have found? What are the perspectives for future research?
>
> Is the process generalizable? Can it be used in other projects? What are the limits of the process?
>
> How do the results compliment the existing literature?
>
> Future research topics?
>
> How well the personas match the later user research in the project?
> Can the developers of the platform utilize the personas?
> Can the developers of the plugins utilize the personas?
> How well the personas translate into user experience of end users?


## Effect of data sources

	[@cooper_about_2014]:

	The marketing profession has taken much of the guesswork out of determining what motivates people to buy. One of the most powerful tools for doing so is market segmen- tation.

	However, understanding if somebody wants to buy something is not the same thing as understanding what he or she might want to do with it after buying it. Market segmen- tation is a great tool for identifying and quantifying a market opportunity, but it’s an ineffective tool for defining a product that will capitalize on that opportunity.

	[@nielsen_personas_2014]

	Personas provide insights into user needs and focuses the decision processes on both user needs and company goals. Including that the process of developing personas has in itself the benefit of gaining deeper insight into the users and to mainting focus on the users.


Literature highlights the importance of including the design or developement team in the persona creation process [e.g., @pruitt_persona_2006; @nielsen_engaging_2004] and it is supported by @nielsen_personas_2014.

> Does this affect the outcomes of the thesis?

	[@nielsen_personas_2014]

	All the persona descriptions we have had access to, have worked with visual strategies to get the descriptions to foster empathy.

## Effect of persona structure

## Effect of research methods

Only one company, varying bacgrounds of UX researchers, varying experience of persona use and creation, varying backgrounds of education.