
# 4. Results - Generated personas

***TODO***

- Select primary and secondary personas


> Tämän kappaleen rakenne, 2-5 lausetta


## 4.1 Generated personas

> Kuinka laaja esittely? Miten esitellään?
> Kolme vai viisi?
>
> Jos on dipan tutkimuskysymyksien kannalta relevantteja, niin ota viisi.
> Päätä myöhemmin, molemmat ok

	Number of personas:

	@cooper_inmates_1999 = 1

	@pruitt_persona_2006 = 3-5

	@nielsen_personas_2014 = Few, but not many (3-4)

Below can be found short descriptions of each persona, the links between the personas and the scenarios, and one full example persona.


+-------+--------------------------+------------------------------------------+
|  Name |         Tagline          |            Short description             |
+=======+==========================+==========================================+
| Elsa  | Pensioner with           | Media use is part of Elsa's daily        |
|       | stable habits            | routine. She starts and finisishes       |
|       |                          | her days with the news. She isn't        |
|       |                          | interested in new technology but         |
|       |                          | relies on the traditional broadcast      |
|       |                          | TV, watched live, and the newspapers.    |
|       |                          | She is interested in trustworthy         |
|       |                          | media content and rely on it to get      |
|       |                          | their information on the world.          |
|       |                          | Elsa does not interact with media,       |
|       |                          | but is a consumer in the strictest       |
|       |                          | meaning of the word.                     |
|       |                          |                                          |
+-------+--------------------------+------------------------------------------+
| Mary  | Content comfort seeker   | Mary uses media for comfort and          |
|       |                          | escapism. She uses media to relax        |
|       |                          | after a busy day either at work or       |
|       |                          | with family. She knows about the         |
|       |                          | newer technology and is slowly           |
|       |                          | adapting it as it becomes easier to      |
|       |                          | use. However, she prefers their set      |
|       |                          | and comfortable ways of using the        |
|       |                          | media. Mary is pretty content at how     |
|       |                          | her life is, but sometimes wishes for    |
|       |                          | a bit more of excitement.                |
|       |                          |                                          |
+-------+--------------------------+------------------------------------------+
| Susie | Teenage social butterfly | Susie is a teenager who uses media       |
|       |                          | constantly and without stop.             |
|       |                          | Interacting through media is a           |
|       |                          | normal part of her life and she          |
|       |                          | spends her days in full interaction      |
|       |                          | with her friends. She consumes           |
|       |                          | moderate amount of traditional media,    |
|       |                          | but a lot of social media. Susie         |
|       |                          | understand how media works on an         |
|       |                          | instinctual level, without technical     |
|       |                          | knowledge of how it actually works.      |
|       |                          |                                          |
+-------+--------------------------+------------------------------------------+
| Danny | Career oriented          | Danny adopted internet at a later age,   |
|       | family man               | but has adapted it fully. His history    |
|       |                          | of traditional media usage is clearly    |
|       |                          | seen. He watches content through         |
|       |                          | television, but uses digital television  |
|       |                          | recordings and VOD services to set the   |
|       |                          | times he watches the content. Danny’s    |
|       |                          | life is very busy because of his family  |
|       |                          | and career. He likes to use media for    |
|       |                          | learning or other useful purposes.       |
|       |                          |                                          |
+-------+--------------------------+------------------------------------------+
| Mike  | True digital native      | Harry was born digital and it shows      |
|       |                          | in his media use. He is a mature media   |
|       |                          | user in their 30s and uses media to      |
|       |                          | interact with his friends, media and the |
|       |                          | world. He shares content online          |
|       |                          | continuously and participate in          |
|       |                          | discussions online. He’s willing and     |
|       |                          | able to pay for content and consumes     |
|       |                          | most of their content online. Harry      |
|       |                          | watches TV media when it suits them      |
|       |                          | through VOD services.                    |
|       |                          |                                          |

*Table X - Generated personas TODO*

Persona to be inserted here.

*Table X - Persona example TODO*

### Persona example


	@nielsen_personas_2014:

	"When the spectator or reader engages with the character, it happens on three levels: recognition, alignment, and allegiance (Smith 1995). Recognition happens when the reader uses the information available to construct the character as an individual person. ... The alignment happens when the reader can identify with the actions, knowledge, and feelings of the character. ... The last level, allegiance, that includes the moral evaluation that the reader makes vis-à-vis the character but also the moral evaluation that the text allows the reader to create. The allegiance is thus both dependant on the access to understand the actions of the character and the reader’s ability to understand the context within which the story plays out."

	"But for the reader to engage, it has to be possible to feel both sympathy and empathy. This means that engagement partly is created based on the informa- tion that the text provides and partly based on the reader’s own experiences."

	@pruitt_persona_2006:

	"At the very least, complete personas must include core information essential to defining the persona: the goals, roles, behaviors, segment, environment, and typical activities that make the persona solid, rich, and unique (and, more importantly, relevant to the design of your product)."

	"A lofty but worthy goal is to have every statement in your foundation document supported by user data. You likely will not achieve this, but the attempt helps you to think critically about your details, and highlights places where you might want to do further research."

	"As you build these detailed depictions, you will be making educated guesses and adding fictional elements, some of which will be directly related to the data you have collected and some of which will not. (It is a good idea to document these assumptions and to consider them possible research questions that may need answering during the validation of your personas.)"

	"Allow realism to win out over political correctness. Avoid casting strongly against expectations if it will under- mine credibility"



## 4.2 Evaluation interviews

> How to differentiate the results of quantitavie interviews from the analysis?
>
> The results are the analysis. Can it just be "demographics, number of interviews and statistics" and rest of it in analysis?
>
> Metodikappaleeseen? Ei tänne
