
# 2. Related research

***TODO***

- Persona review
- Personas based on secondary data
- Industry practices on personas

----

> Related research kappaleen rakenne, 2-5 lausetta

## 2.1 Personas

> @pruitt_persona_2006, p21 has history of user representations. To be included in thesis?
>
> There is also presentation of persona-like representations in UCD research
>
> Eri näkökulmia persooniine: Cooper, Gudin & Adlin, Nielsen, Muut?
>
> E.g. [@nielsen_personas:_2013; @pruitt_persona_2006, pp. 146-155]

This chapter presents the literature on personas. First I give an overview of persona history and developement, then I review the claimed benefits and critisism of personas.

Personas are composite archetypes based on behaviour patterns uncovered during research [@pruitt_persona_2006; @mulder_user_2007; @cooper_about_2014]. They are tools for formalizing user research for informing the product design.

While user representations have been used for a long time [e.g., @dreyfuss_designing_2003], their use in user centered design picked up in the early 1990s [@pruitt_persona_2006]. Persona as a term and format for the user representations was introduced by Alan Cooper in the 1999 [@cooper_inmates_1999]. Since his book, the personas have been subject to much discussion, debate and further developement.

Cooper's personas are very goal oriented and context specific.

His book doesn't give a method for creating them, only describes a single case

	Personas are the single most powerful design tool that we use. They are the foundation for all subsequent Goal-Directed design. Personas allow us to see the scope and nature of the design problem. They make it clear exactly what the user's goals are, so we can see what the product must do—and can get away with not doing.

For @cooper_about_2014 and @mulder_user_2007, the personas are very goal oriented, that is they are defined through their primary goals.

@pruitt_personas:_2003 and @pruitt_persona_2006 later expanded persona method. Their main addition was the use of quantitative and qualitative data during research, before persona formation. For [@pruitt_persona_2006] the primary focus of personas might vary depending on the circumstances. Their personas are derived from differences in user groups, segments and goals.

As persona literature has evolved, it has also converged. Cooper has taken in the developements in the field in his latest edition of the persona method in @cooper_about_2014.

The persona is based on behavioural patterns, but for design the personas revolve on different level goals.

Another slighty different approach to personas is Lene Nielsen's engaging personas. She focuses more on the narratives of personas to bring them to life and help them engage the designers more [@nielsen_engaging_2004, @nielsen_personas:_2013]. Her critique to the goal oriented personas is that the characters are often flat or one dimensional, and are not relatable. She borrows from filmwriting literature to help flesh out the persona characters to be rounded and relatable.


---


	[@cooper_about_2014]

	Per- sonas are not real people, but they are assembled from the behaviors and motivations of the many actual users we encounter in our research. In other words, personas are composite archetypes based on behavior patterns uncovered during the course of our research, which we formalize for the purpose of informing the product design.

	For personas to be effective tools for design, considerable rigor and finesse must be applied during the process of identifying the significant and meaningful patterns in user behavior and determining how these behaviors translate into archetypes that accurately represent the appropriate cross sec- tion of users.

	The best way to successfully accommodate a variety of users is to design for specific types of individuals with specific needs.

	Although personas are depicted as specific individuals, because they function as arche- types, they represent a class or type of user of a specific interactive product. A persona encapsulates a distinct set of behavior patterns regarding the use of a particular prod- uct (or analogous activities if a product does not yet exist).

	We can’t overemphasize how important goals are for personas. In fact, we would go so far as to say that if your user model doesn’t have any goals, what you have is not a persona.


	[@pruitt_persona_2006]

	Personas are fictitious, specific, concrete representations of target users. ... Personas put a face on the user — a memorable, engaging, and actionable image that serves as a design target. They convey information about users to your product team in ways that other artifacts cannot.

	[@nielsen_personas:_2013]

	Thus, a persona is a description of a fictitious user. A user who does not exist as a specific person but who is described in a way so that the reader can recognise the description and believes that the user could exist in reality. A persona is described based on relevant information from potential and real users and thus pieced together from knowledge about real people.

	Often, the method is perceived as a usability method. But as it will become apparent, personas are more of a design method covering all phases and all aspects of a development project. It has a wider focus than usability.

	A persona is not the same as an archetype or a person. As it has been described, the special aspect of a persona description is that you do not look at the entire person but use the focus area as a lens to highlight the relevant attitudes and the specific context associated with these.

	Too many project participants get their first taste of the company using the method when they discover that there are persona posters on the wall or when they see an image of a persona on a mug.7 Therefore, it’s important that designers and developers early on in the process get to experience the strengths of the method.

	But my special point of view is that by including as many as possible in the development of personas and by learning as much as possible about using the method, the knowledge will be disseminated.

	If it is seen as a communication strategy, however, it can unfortunately sometimes be part of preventing that the method is used in daily project decisions. Therefore, I think it is more expedient to view the method as a process ensuring user focused design in all phases of the project: a process that includes as many from the project team as pos- sible and that ensures that all have understood what it entails to use the method.

	In literature about the method, there are today four differ- ent perspectives regarding personas: Cooper’s goal-directed perspective; Grudin, Pruitt, and Adlin’s role-based perspective; the engaging perspective that I myself use, which emphasises how the story can engage the reader (Sønderstrup-Andersen 2007); and the fiction-based perspective. The first three perspectives have in com- mon that the persona descriptions should be founded on data. The last perspective, the fiction-based perspective, does not include data as basis for persona descrip- tion but creates personas from the designers’ intuition and assumptions; they have names such as ad hoc personas (Norman 2004) and extreme characters (Djajadiningrat et al. 2000).

	[@nielsen_engaging_2004]

	The engaging persona is a description of the user in a scenario. This description is based on field studies. The engaging persona can be the user in one or more scenarios depending on goals and the situations of use. The objective of using the engaging persona is to enable the designers to engage in users that differ from the designers in terms of sex, culture and/or age.

	The scenario is a narrative written in a natural language. It focuses on a user using the system. The goal of the scenario is to explore design solutions.

	A persona is a description of a fictitious user. ... The fictitious user can function both as a vehicle to create empathy and identification, a storage for information, and as a method to create a focus on particular market shares.

	Contrary to the persona, the engaging persona has no goal as such but the goals arise with the needs and from the situations and do not appear until in the context of the scenario.

	There does not exist a one to one relation between data and persona but as the persona is fictitious and based on many users, there will be parts that are fictitious while other parts are closely related to the data.

	It is my aim to develop a model for the engaging persona and scenario and as it can be seen in this initial part of the model, the engaging persona has five characteristics:
	• The engaging persona is described with a bodily expression and a posture, a gender, and an age
	The engaging persona has a psyche described as the present state of mind, persistent self-perception, character traits, temper, abilities, and attitudes.
	• The engaging persona has a background as well as cultural and social relations. Described as present knowledge, job and family relations, and persistent beliefs, education, and internalised values, and norms.
	• The engaging persona has present emotions, intentions and attitudes including ambitions and frustrations, wishes and dreams.
	• The engaging persona has character traits in opposition as well as peculiarities.

	[@mulder_user_2007]

	A persona is a realistic character sketch representing one segment of a Web site's targeted audience. Each persona is an archetype serving as surrogate for an entire group of real people. Personas summarize user research findings and bring that research into life in such a way that a company can make decisions based on these personas, not based on themselves.

	Personas are primarily defined by their goals they have when they come to the site.



### Provisional or Ad hoc personas

	[@cooper_about_2014]

	Although it is highly desirable that personas be based on detailed qualitative data, some- times there is simply not enough time, resources, budget, or corporate buy-in to perform the necessary fieldwork. In these cases, provisional personas (or, as Don Norman calls them, “ad hoc” personas) can be useful rhetorical tools to clearly communicate assumptions about who the important users are and what they need. These personas also can enforce rigorous thinking about serving specific user needs (even if these needs are not validated).

	They are typically based on a combination of stakeholder and subject matter expert knowledge of users (when available), as well as what is understood about users from existing market data. Provisional personas are, in fact, a more fleshed-out persona hypothesis (as described in Chapter 2).

	Our experience is that, regardless of a lack of research, using provisional personas yields better results than no user models.

	While provi- sional personas may help focus your design and product team, if you do not have data to back up your assumptions, you may do the following:
	- Focus on the wrong design target.
	- Focus on the right target but miss key behaviors that could differentiate your product.
	- Have a difficult time getting buy-in from individuals and groups who did not participate in their creation.
	- Discredit the value of personas, causing your organization to reject the use of personas in the long term.

	If you are using provisional personas, it’s important to do the following:
	• Clearly label and explain them as such. We often give them only first names.
	• Represent them visually with sketches, not photos, to reinforce their provisional
	nature.
	• Try to use as much existing data as possible (market surveys, domain research, subject matter experts, field studies, or personas for similar products).
	• Document what data was used and what assumptions were made.
	• Steer clear of stereotypes (which is more difficult to do without field data).
	• Focus on behaviors and goals, not demographics.

	[@pruitt_persona_2006]

	Sometimes personas are not actually created with data: they are based on loose assumptions or are completely fictional. In those cases, they need to be communicated as such, and the degree to which they can be trusted and used should be kept in check.

	[@nielsen_engaging_2004]

	There does not exist a one to one relation between data and persona but as the persona is fictitious and based on many users, there will be parts that are fictitious while other parts are closely related to the data.




### Benefits of personas

> Suunnitelmissa tämän osuun mitta on 3 sivua. Nyt mitta on 2 sivua, ja mielestäni olen tiivistänyt olennaisen kirjallisuuden hyödyistä. Pitäisikö tätä laajentaa tai avata enemmän? Miltä osin?

Until recently, much of the literature on benefits of personas came from popular literature that advocates the use of personas and gives directions on how to implement the persona design process in organisations [@putnam_bridging_2010]. There are still too few larger studies of the persona method in practice to make conclusive pros and cons of persona use [@nielsen_personas_2014]. That said, there is a lot of practical literature [e.g. @mulder_user_2007; @cooper_about_2014] and single case studies [e.g. @pruitt_personas:_2003; @guhjonsdottir_personas_2008] that provide positive experiences in using personas as part of design process.

The main benefits the literature agrees on are that the personas help focusing the design effort to specific users and their needs, that the personas aid in communication within the team and the stakeholders, that they make implicit assumptions of users explicit, and that they bring empathy to the design process.

Personas bring focus to the product development effort [@pruitt_persona_2006]. In a study of benefits perceived by persona experts, the added focus in product developement was the most important benefit of personas [@miaskiewicz_personas_2011]. Implementing personas in the design process helps designers focus prioritize and limit the product feature set. As one expert in @miaskiewicz_personas_2011 said: ‘A persona helps project teams answer two fundamental questions: who are we solving for and who are we not solving for?’.

@pruitt_persona_2006 describes the value of focus and limitations in design in the following way. In product developement cycle, in the beginning there are typically multitude of feature ideas. The resources are limited and it isn't possible to build every feature and more importantly, building every feature would result in a product that would satisfy no one. Every detail put in persona limits the number of choices to make. Personas define a tight domain, where user's needs guide the product and feature decisions, which leads to a product that better matches user's needs and leads to higher user satisfaction. This is supported by experience of persona use [@pruitt_personas:_2003; @miaskiewicz_personas_2011] where they see great benefit in the way that personas help making the otherwise nebolous and abstract product decisions explicit and related to user research.

Since their introduction by @cooper_inmates_1999, personas have been aid of communication both within the team and with project stakeholders. Personas help develop a common language about the users [@pruitt_persona_2006]. This helps in communication within the design team. The designers know that they are speaking of the same things and have common vocabulary for different user needs. Personas also help communicating the user needs to stakeholders outside the design team [@pruitt_personas:_2003].

Product development often happens in teams of multiple people. The teams may involve for example designers, developers, business owners and subject matter experts. Personas help product design team share the language they use about their users. 'User' can mean different things to different people and its meaning can vary depending on the situation [@pruitt_persona_2006]. Personas make the language concrete. Instead of undefined 'user', the discussion can focus on a certain persona with well defined set of charasteritics [@cooper_about_2014].

In relation to improved communication within design team and shared vocabulary about user, personas creation process also forces design team to voice their implicit assumptions about the user, making them explicit [@pruitt_persona_2006]. The designers often have internal assumptions about the users that guide their decision making. By employing personas in the desing process, the designers have to re-evaluate their assumptions [@pruitt_personas:_2003; @miaskiewicz_personas_2011].

The experience of @pruitt_personas:_2003 is that without personas, development teams make product and feature decisions without recognizing or communicating their internal assumptions. The developement team's own favourite ideas might not be what the user actually needs. Using personas forces them to re-evaluate the features based on real user needs. This is mirrored in @cooper_about_2014, where they claim that personas help avoid self-referential design, i.e. when developement team projects their own goals, motivations and skills to user, based on the assumption that the user is similar to themselves.

Personas are a way of humanizing large collections of data and user reach to a format that is easy for designers to relate to and emphatize with [@pruitt_personas:_2003]. Putting themselves in the position of the user, helps designers better understand what user's wishes in certain situation are and they will use the product [@nielsen_personas:_2013]. The emphatizing aspect of personas is taken furthest by @nielsen_personas:_2013, who employs filmwriting techniques to write engaging persona descriptions with rich characters and narratives to help designers better relate to personas.

> What about engagement? [@pruitt_persona_2006]: Personas are fun. Just like characters in books, TV shows, and movies, personas evoke empa- thy and inspire the imagination. People on a product development team can relate to personas and become active participants in bringing the personas “to life.”


	[@pruitt_persona_2006, p15]

	Our first goal as product designers should be to build a shared, data-driven, well-communicated vision of the user to focus the efforts of the product team.

	Personas humanize vast and disparate data sources by capitalizing on our ability to remember details about individual people. In so doing, they provide a usable alternative to referring to the nebulous “user.” In other words, personas do the job of creating a concrete, focused, and stable definition of your audience.

	If a stakeholder avoids making the decision of which features not to build, the result is feature creep and a product that in trying to appeal to everyone satisfies few.

	Personas are helpful because they are constrain- ing. Personas clearly define who is and who is not the target user (or customer) for the product and thereby make some of the decisions for us. ... Personas define a tight domain within which the product needs to perform. Within that domain, personas free us to explore all of the “marvelous possibilities” for the product we are designing.

	From the very beginning of a product development cycle, personas can be there to provide data in the form of the “voice” of the user, which can reduce feature debates and refocus projects. In this regard, personas offer a consistent target-audience vision

	!!! Of course, you could likely obtain the benefits mentioned to this point by invoking other UCD techniques and by using representations of users other than personas. So, what is the overriding benefit of personas compared to similar techniques? We believe it lies in the way personas can engage your team.

	Personas are fun. Just like characters in books, TV shows, and movies, personas evoke empa- thy and inspire the imagination. People on a product development team can relate to personas and become active participants in bringing the personas “to life.”

	Chapter 12: WHY PERSONAS WORK: THE PSYCHOLOGICAL EVIDENCE, ei kovin relevanttia. Kevyt katsaus taustalla vaikuttavaan psykologiaan, pop-sci

	[@cooper_about_2014]
	The persona is a powerful, multipurpose design tool that helps overcome several problems that plague the development of digital products. Personas help designers do the following:
	• Determine what a product should do and how it should behave. Persona goals and tasks provide the foundation for the design effort.
	• Communicate with stakeholders, developers, and other designers. Personas provide a common language for discussing design decisions and also help keep the design centered on users at every step in the process.
	• Build consensus and commitment to the design. With a common language comes a common understanding. Personas reduce the need for elaborate diagrammatic models; it’s easier to understand the many nuances of user behavior through the narrative structures that personas employ. Put simply, because personas resemble real people, they’re easier to relate to than feature lists and flowcharts.
	• Measure the design’s effectiveness. Design choices can be tested on a persona in the same way that they can be shown to a real user during the formative process. Although this doesn’t replace the need to test with real users, it provides a powerful reality-check tool for designers trying to solve design problems. This allows design iteration to occur rapidly and inexpensively at the whiteboard, and it results in a far stronger design baseline when it’s time to test with actual people.
	• Contribute to other product-related efforts such as marketing and sales plans. The authors have seen personas repurposed across their clients’ organizations, informing marketing campaigns, organizational structure, customer support centers, and other strategic planning activities. Business units outside of product development want sophisticated knowledge of a product’s users and typically view personas with great interest.


	Focus design, talk about same users:
	Although satisfying the users of our products is our goal, the term user causes trouble when applied to specific design problems and contexts. Its imprecision makes it danger- ous as a design tool, because every person on a product team has his own conceptions of who the user is and what the user needs. ... A lack of precision about the user can lead to a lack of clarity about how the product should behave.

	Help avoid self-referential design (also in development):
	Self-referential design occurs when designers or developers project their own goals, moti- vations, skills, and mental models onto a product’s design.

	Another syndrome that personas help prevent is designing for edge cases—situations that might happen but that usually won’t for most people. Typically, edge cases must be designed and programmed for, but they should never be the design focus.

	One of the reasons personas are so successful as user models is that they are personifications: They engage the empathy of the design and development team around the users’ goals.

	[@nielsen_personas:_2013]

	The descriptions of personas are used by designers, developers, project par- ticipants, and others to get ideas for design of products, IT systems, and services. The descriptions help project participants to identify with users and think of these instead of themselves. And they provide all participants in a project with the same understanding of who the users are.

	To put yourself in the shoes of the users gives you an idea about what their wishes are and how they will use the product to be designed, whether it is a website, a mobile phone, or a new bike.

	Also, a persona makes it possible to create a clear idea about what the user will use the product for and in what situation or context the product is to be used.

	Benefits:

	+ Empathy
	+ Focus


	[@mulder_user_2007]

	Benefits:

	- Personas bring focus
	- Personas build empathy
	- Personas encourage consensus
	- Personas create efficiency
	- Personas lead to better decisions

	[@nielsen_engaging_2004]

	The advantage of fictitious user descriptions – personas – is the ability to encourage communication about the target groups and support arguments about features.

	The largest benefit from the methods is the ability to be specific in the understanding of the users both in the descriptions and in discussions.

	There are diverse opinions about how thorough the pictures of the users they painted were during the workshop. Some find that they can easily recognise the three personas, others that they are too stereotyped and that without the discussions they would not have worked. But the participants agree that the focus on concrete but fictitious users was of value

	Even though there is a slight insecurity, the positive effect of the method is that it forced the group to take decisions and the choices made are better informed and more focussed than previous decisions.

	The lifelike descriptions are considered to originate from the discussions, as in hindsight, most of the participants found the personas were written as stereotypes or caricatures.

	But the discussions and the scenarios compensate for the caricatures and, as it will be seen in the next chapter, create more engaging personas.

	The focus on the engaging persona creates an awareness of how targeted the site should become, that it cannot function as a solution for everybody.

	[@pruitt_personas:_2003]

	It is clear to us that Personas can create a strong focus on users and work contexts through the fictionalized settings.

	The act of creating Personas has helped us make our assumptions about the target audience more explicit.

	Personas are a medium for communication; a conduit for information about users and work settings derived from ethnographies, market research, usability studies, interviews, observations, and so on.

	Finally, Personas focus attention on a specific target audience.

	[@nielsen_personas_2014]

	Shared by all is that personas provide a common language, this covers both the method’s ability to provide internal and external project participants with a common language and a common understanding of the users.

	Personas provide insights into user needs and focuses the decision processes on both user needs and company goals. Including that the process of developing personas has in itself the benefit of gaining deeper insight into the users and to mainting focus on the users.



### Critisism of personas

Based on my literature review, the main criticism of personas is the lack of scientific evidence for the claimed benefits of the personas. Persona literature is largely based in practice and anecdotal evidence. Still, the persona method has gained wide popularity and is an established method in UCD process.

Some of the benefits of personas have been under criticism. For example @turner_is_2011 argue that stereotypes are almost an inheret part of personas, contrary to goals @cooper_inmates_1999 and others.

Putnam argues that the personas

In literature, there are examples of failed persona projects: [@ronkko_personas_2004; @blomquist_personas_2002]

In the case of @ronkko_personas_2004, the failure was not for professional reasons that the personas were not implementied. Instead, the project failed to recognize the patterns of dominance in the developmeent process. It was not enough to get interaction designers, developers, marketing and sales and management on board to the process. The real power groups were outside of the company in different stakeholders. For different reasons the constellations of stakeholders could not be convinced to accept personas as a driving design tool and hence the persona project was abandonded.


This supports the experience of @pruitt_persona_2006, who listed four main reasons why persona projects fail: the persona effort isn't accepted or supported by the leadership, the personas were not credible and not associated with methodological rigor and data, the personas were poorly communicated and the product design and developement team did not understand how to use them.

@matthews_how_2012 found that even with experienced designers it couldn't be assumed that they understood the persona method. Those who had received training in how to use and create the personas, were a lot more enthousiastic about personas and used them in their work extensively. This is supported by @nielsen_personas_2014, where the only negative experience of using personas came from a company that did not understand when in the product developement process the personas should be used.



	[@nielsen_personas:_2013, p. 17; @putnam_bridging_2010]

	Personas (with scenarios) were found to be effective in helping designers focus on specific users and facilitating communication about those users; however, they were less successful at helping designers empathize or avoid ill-informed assumptions about users. Additionally, designer attributes which included professional experience level appeared to be associated with persona effectiveness.

	[@cooper_about_2014]
	Don’t confuse persona archetypes with stereotypes. Stereotypes are, in most respects, the antithesis of well-developed personas. Stereotypes usually are the result of designer or product team biases and assumptions, rather than factual data. Personas developed by drawing on inadequate research (or synthesized with insufficient empathy and sen- sitivity to interview subjects) run the risk of degrading to caricatures.

	Responses to some criticism: [@cooper_about_2014, pp. 93-95]

	[@pruitt_persona_2006]

	Even well-crafted personas can result in little or no focus on users in the development process, and poorly executed persona can keep the development team from investing in other UCD techniques or other efforts that improve product quality

	1. The effort was not accepted or supported by the leadership team.
	2. The personas were not credible and not associated with methodological rigor and data.
	3. The personas were poorly communicated.
	4. The product design and development team employing personas did not understand how to use them.

	To do personas well, you need to garner the support of the key leaders on your team or elsewhere in the company. Doing an upfront analysis of your organization and product team needs is critical. But perhaps more important is getting the support of high-level people within your organization.

	If you are not careful, your personas (or your process) can be perceived as lacking validity or rigor (i.e., that your process was not thorough, precise, or methodical).

	Even where personas are created rigorously out of carefully analyzed data, if the relationship to that data and process is not clear there is a risk of lost credibility. The perception of rigor is important.

	[@pruitt_personas:_2003]

@nielsen_engaging_2004 criticizes the ability of persona descriptions to engage the designer and proposes the term engaging personas to define her approach to persona developement. Her personas are narrative driven and focus on making the personas rounded characters, as opposed to flat characters.

	[@nielsen_engaging_2004]
	The different articles and the books have the problem in common that it is very difficult to find the theoretical arguments for the different takes on the described methods.

	Personas are developed from practice and it is difficult to criticise good experience but it is also difficult to understand why the experience is good if there is no connection to any theory or description of context.

	Regarding engagement:
	The plot-driven story is seen in most action films where the hero has very few character- traits and the story moves from one action to the next. The character is defined as a flat character. In the character-driven story the character is described with several character traits and it is the development of the character that moves the story forward. The character is defined as a rounded character.

	Contrary to the persona, the engaging persona has no goal as such but the goals arise with the needs and from the situations and do not appear until in the context of the scenario.

	And there is a notion that the choices they made about whom to target and how to describe the users might not be well enough grounded. A different choice of types and personas might have had implications

	The lifelike descriptions are considered to originate from the discussions, as in hindsight, most of the participants found the personas were written as stereotypes or caricatures.

	But the discussions and the scenarios compensate for the caricatures and, as it will be seen in the next chapter, create more engaging personas.

	Even though the participants feel they have gained a valuable insight and understanding of the importance of considering the users in the process they feel it will be difficult to spread this perception to the rest of the organisation. It is considered that the e-business group can change the way they work and they can change priorities in smaller projects, but the company as such uses the same strategy towards net-media as they do when selling a medical product.

	The persona and scenario methods provide a mindset that follows up on the reports in a structured way and help focus on the users in decision processes and even though the four reports have been studied intensely and have been used in meetings with medical practitioners, they are not considered to be able to stand- alone.

	!!! Participation in the workshops rather than the written personas and scenarios was considered to be of most value.

	The written personas and scenarios descriptions are not considered to be of value to outsiders unless they are familiar with the methods. An outsider can interpret the personas and scenarios as they like, and D. points to another factor that can influence the reading process: the reader might be afraid to acknowledge the user description if they do not have any experience with IT.

	Even though it is the participation that is most praised, the personas and scenarios are now part of the daily routines and when they consider future developments.

	For an outsider who did not participate in the discussions, like N., the decisions are impenetrable and in the reading process it was difficult to understand the arguments behind the choices.

	As the method is intense and discussions play an important part in the process of creation those not involved in the process might find it difficult to understand the written description.

	The method is suitable for the e-business group because they are the type of persons they are: talkative and ready for experiments. While the rest of the organisation (with a few exceptions) are considered traditional in their mindset and victims of traditional medical marketing strategies.


	The five areas (i.e body, psyche, background, emotions and cacophony), which form the building blocks of the engaging persona, were derived from narrative theory. My research shows that they were important for understanding and engaging in the personas as the designers used the five areas as a basis for their personas creations. If the user inquiries did not provide the data relevant to the five areas, the designers inappropriately used inferences to cover the missing areas.

	[@nielsen_personas:_2013]

	If it is seen as a communication strategy, however, it can unfortunately sometimes be part of preventing that the method is used in daily project decisions. Therefore, I think it is more expedient to view the method as a process ensuring user focused design in all phases of the project: a process that includes as many from the project team as pos- sible and that ensures that all have understood what it entails to use the method.

	Criticism of the method pertains to empiricism, especially the relationship between data and fiction. The implementation of the method in companies has also come under fire (Rönkkö et al. 2004; Chapman and Milham 2006; Chapman et al. 2008; Portigal 2008).

	[@pruitt_personas:_2003]

	we found that the early Persona-like efforts suffered from four major problems:

	1. The characters were not believable; either they were obviously designed by committee (not based on data) or the relationship to data was not clear.
	2. The characters were not communicated well. Often the main communication method was a resume-like document blown up to poster size and posted around the hallways.
	3. There was no real understanding about how to use the characters. In particular, there was typically nothing that spoke to all disciplines or all stages of the development cycle.
	4. The projects were often grass-roots efforts with little or no high-level support (such as people resources for creating and promoting Personas, budget for posters or other materials to make the Personas visible, or encouragement from team leaders: ìthou shalt use these charactersî).

	Getting the right Persona or set of Personas is a challenge.

	Related to this is the temptation of Persona reuse. ... It can be good or bad when our partner teams adopt or adapt our Personas. Different teams and products have different goals, so the Personas are stretched a bit.

	In addition, marketing and product development have different needs that require different Persona attributes, and sometimes different target audiences.

	Personas can be overused. At worst, they could replace other user- centered methods, ongoing data collection, or product evaluation. Personas are not a panacea.

	[@nielsen_personas_2014]

	One such is that it takes time and money to create personas, this is perceived as the only actual downside of the method.

	And it can be hard to keep in mind that the personas are not real users and cannot replace meetings with real users

## 2.2 Persona formation process

In this chapter I synthethize the literature on persona generation process that informed the generation of studied personas. The methods of persona generation have developed significantly since their introduction.

Different focus in persona use drives different kinds of generation processes. In this chapter I combine the essential literature and present a process that was used to create the personas studied later in this thesis.


Various descriptions of persona formation process exist in literature. Their focus and specifics differ, but their general process can be summariezed in the following way:

1. Data gathering
2. etc
3. etc
4. etc

First there is töttöröö, jne.

@nielsen_personas_2014 and @pruitt_persona_2006 emphatize that benefits of personas come from the entire process of generating personas. @pruitt_persona_2006 go into greath depth on how to communicate the personas to the entire organisation. They argue that if one of the major benefits is the shared understanding of the user, it should be shared as widely as possible in the organisation. @nielsen_personas_2014 says that personas are a process, not an user portrait. She agrees that persona knowledge and understanding should spread through the organisation, but she critisizes the use of personas as a end product of data gathering to be disseminated in the organisation. For her the most effective way of spreading deep understanding of the users is to engage people from the entire organization into the persona creation process.

> esittele generalisoitu prosessi vaiheineen ja käytä eri lähteitä vaiheiden selittämiseen
>
> Mikä tässä tavassa jää epäselväksi, on linkitys omaan tapaan tehdä persoonat. Se esitellään metodissa.
>
> Pitäisi kuitenkin esitellä teoria siellä tehtyjen valintojen tueksi. Eli fokus siihen, mitä voi tehdä, kun asiakasryhmä on kansainvälinen ja tutkimusta ei voi tehdä. Mitkä on eri lähtökohdat siihen? Siksi secondary research kappale. Pitäisikö se tuoda tämän rposessin lomaan vai sen jälkeen erillisenä?
>
> "Okei, näin tää yleensä tehdään. Mutta jos tilanne on tää, niin pitää tehä seuraavat peliliikkeet"



> eri näkökulmia, miten luodaan

[@cooper_inmates_1999; @nielsen_personas:_2013; @putnam_bridging_2010]

	[@cooper_about_2014]

	For personas to be effective tools for design, considerable rigor and finesse must be applied during the process of identifying the significant and meaningful patterns in user behavior and determining how these behaviors translate into archetypes that accurately represent the appropriate cross sec- tion of users.

	The best way to successfully accommodate a variety of users is to design for specific types of individuals with specific needs.

	Personas, like any models, should be based on real-world observation. As discussed in the preceding chapter, the primary source of data used to synthesize personas should be in-context interviews borrowing from ethnographic techniques, contextual inquiry, or other similar dialogues with and observation of actual and potential users. The quality of the data gathered following the process (outlined in Chapter 2) impacts the efficacy of personas in clarifying and directing design activities. Other data can support and sup- plement the creation of personas (listed in rough order of effectiveness):
	• Interviews with users outside of their use contexts
	• Information about users supplied by stakeholders and subject matter experts (SMEs)
	• Market research data such as focus groups and surveys
	• Market-segmentation models
	• Data gathered from literature reviews and previous studies
	However, none of this supplemental data can take the place of direct user interviews and observation. Almost every aspect of a well-developed persona can be traced back to sets of user statements or behaviors.


	Personas used for multiple products:
	Organizations with more than one product often want to reuse the same personas. How- ever, to be effective, personas must be context-specific: They should focus on the behaviors and goals related to the specific domain of a particular product. Personas, because they are constructed from specific observations of users interacting in specific contexts, can- not easily be reused across products, even when those products form a closely linked suite.

	For a set of personas to be an effective design tool for multiple products, the personas must be based on research concerning the usage contexts for all these products. In addi- tion to broadening the scope of the research, an even larger challenge is to identify man- ageable and coherent sets of behavior patterns across all the contexts. You shouldn’t assume that just because two users exhibit similar behaviors in regard to one product, those two users would behave similarly with respect to a different product.

	As the focus expands to encompass more and more products, it becomes increasingly difficult to create a concise and coherent set of personas that represents the diversity of real-world users. We’ve found that, in most cases, personas should be researched and developed individually for different products.

	Ideally, persona demographics should be a composite reflection of what researchers have observed in the interview population, modulated by broader market research. Personas should be typical and believable, but not stereotypical. If the data is inconclusive or the characteristic is unimportant to the design or its acceptance, we prefer to err on the side of gender, ethnic, age, and geographic diversity.

	Personas explore ranges of behavior:
	The target market for a product describes demographics as well as lifestyles and some- times job roles. What it does not describe are the ranges of different behaviors exhibited by members of that target market regarding the product and related situations. Ranges are distinct from averages: Personas do not seek to establish an average user; they express exemplary or definitive behaviors within these identified ranges.

	Because products must accommodate ranges of user behavior, attitudes, and aptitudes, designers must identify a persona set associated with any given product.

	You usually can’t ask a person what his goals are directly. Either he’ll be unable to artic- ulate them, or he’ll be inaccurate or even imperfectly honest. People simply are unpre- pared to answer such self-reflective questions accurately. Therefore, designers and researchers need to carefully reconstruct goals from observed behaviors, answers to other questions, nonverbal cues, and clues from the environment, such as the titles of books on shelves. One of the most critical tasks in the modeling of personas is identi- fying goals and expressing them succinctly: Each goal should be expressed as a simple sentence.

	As previously discussed, personas are derived from qualitative research—especially the behavioral patterns observed during interviews with and observations of a product’s users and potential users (and sometimes customers). Gaps in this data are filled by sup- plemental research and data provided by SMEs, stakeholders, quantitative research, and other available literature.

	Creating believable and useful personas requires an equal measure of detailed analysis and creative synthesis.

	Be careful about the amount of detail in your descriptions. The detail should not exceed the depth of your research.

	[@pruitt_persona_2006]

	In other words, personas should be more than a collection of facts. Personas should be compelling stories that unfold over time in the minds of your product team. We believe that successful personas and persona efforts are built progressively. Just as we get to know people in our lives, we must get to know personas (and the data they contain) by developing a relationship with them. No single document, read in a few minutes or posted on a wall, can promote the type of rich and evolving relationship with information about users that is the cornerstone of good product development.

	[@mulder_user_2007] has chapter on quantitative research

	[@nielsen_engaging_2004]
	It is my aim to develop a model for the engaging persona and scenario and as it can be seen in this initial part of the model, the engaging persona has five characteristics:
	• The engaging persona is described with a bodily expression and a posture, a gender, and an age
	The engaging persona has a psyche described as the present state of mind, persistent self-perception, character traits, temper, abilities, and attitudes.
	• The engaging persona has a background as well as cultural and social relations. Described as present knowledge, job and family relations, and persistent beliefs, education, and internalised values, and norms.
	• The engaging persona has present emotions, intentions and attitudes including ambitions and frustrations, wishes and dreams.
	• The engaging persona has character traits in opposition as well as peculiarities.

	[@nielsen_engaging_2004]
	Photos: Chapter 13.2.2, p 294

	The five areas (i.e body, psyche, background, emotions and cacophony), which form the building blocks of the engaging persona, were derived from narrative theory. My research shows that they were important for understanding and engaging in the personas as the designers used the five areas as a basis for their personas creations. If the user inquiries did not provide the data relevant to the five areas, the designers inappropriately used inferences to cover the missing areas.

	[@nielsen_personas_2014]

	Photos: All the persona descriptions we have had access to, have worked with visual strategies to get the descriptions to foster empathy.


### Secondary data use in personas

According to @cooper_about_2014, primary data in persona research should come from in-context interviews, contextual inquiry or other similar dialogues with and observation of actual and potential users. According to them, personas should be based on real-world observation. Non primary data can support and supplement the creation process. They stress that secondary data can't take the place of direct user interviews and observation.

@cooper_about_2014 list possible secondary data sources to be interviews outside of their use context, information provided by stakeholders and subject matter experts, market research data, market-segmentation models and data gathered from literature reviews and previous studies.

@cooper_about_2014 emphasizes that amount of detail should not exceed the depth of the research conducted. Any details that are not found in the data are assumptions of designers.

	[@cooper_about_2014]
	Personas, like any models, should be based on real-world observation. As discussed in the preceding chapter, the primary source of data used to synthesize personas should be in-context interviews borrowing from ethnographic techniques, contextual inquiry, or other similar dialogues with and observation of actual and potential users. The quality of the data gathered following the process (outlined in Chapter 2) impacts the efficacy of personas in clarifying and directing design activities. Other data can support and sup- plement the creation of personas (listed in rough order of effectiveness):
	• Interviews with users outside of their use contexts
	• Information about users supplied by stakeholders and subject matter experts (SMEs)
	• Market research data such as focus groups and surveys
	• Market-segmentation models
	• Data gathered from literature reviews and previous studies
	However, none of this supplemental data can take the place of direct user interviews and observation. Almost every aspect of a well-developed persona can be traced back to sets of user statements or behaviors.

	A persona encapsulates a distinct set of behavior patterns regarding the use of a particular prod- uct (or analogous activities if a product does not yet exist). You identify these behaviors by analyzing interview data. They are supported by supplemental data (qualitative or quantitative) as appropriate.

	You usually can’t ask a person what his goals are directly. Either he’ll be unable to artic- ulate them, or he’ll be inaccurate or even imperfectly honest. People simply are unpre- pared to answer such self-reflective questions accurately. Therefore, designers and researchers need to carefully reconstruct goals from observed behaviors, answers to other questions, nonverbal cues, and clues from the environment, such as the titles of books on shelves. One of the most critical tasks in the modeling of personas is identi- fying goals and expressing them succinctly: Each goal should be expressed as a simple sentence.

	As previously discussed, personas are derived from qualitative research—especially the behavioral patterns observed during interviews with and observations of a product’s users and potential users (and sometimes customers). Gaps in this data are filled by sup- plemental research and data provided by SMEs, stakeholders, quantitative research, and other available literature.

	Be careful about the amount of detail in your descriptions. The detail should not exceed the depth of your research.

	If you are using provisional personas, it’s important to do the following:
	• Clearly label and explain them as such. We often give them only first names.
	• Represent them visually with sketches, not photos, to reinforce their provisional
	nature.
	• Try to use as much existing data as possible (market surveys, domain research, subject matter experts, field studies, or personas for similar products).
	• Document what data was used and what assumptions were made.
	• Steer clear of stereotypes (which is more difficult to do without field data).
	• Focus on behaviors and goals, not demographics.

	Any attempt to reduce human behavior to statistics is likely to overlook important nuances, which can make a huge differ- ence in the design of products. Quantitative research can only answer questions about “how much” or “how many” along a few reductive axes. Qualitative research can tell you what, how, and why in rich detail that reflects the complexities of real human situations.

	Social scientists have long realized that human behaviors are too complex and subject to too many variables to rely solely on quantitative data to understand them. Design and usability practitioners, borrowing techniques from anthropology and other social sciences, have developed many qualitative methods for gathering useful data on user behaviors to a more pragmatic end: to help create products that better serve user needs.

	[@pruitt_personas:_2003]
	We had neither time nor resources to do original research, but were fortunate that others had completed several field studies and market research pertinent to our product.



## 2.3 UX Practitioner use of personas

[@matthews_how_2012; @nielsen_personas_2014; @putnam_bridging_2010]

In this chapter I review how the personas have actually been used by UX practitioners. Many the benefits associated with personas come from literature advocating and teaching the use of personas. The actual UX practitioner use of personas has been researched surprisingly little.

	[@nielsen_personas_2014]

	!!! Extensive literature review

	Most participants refer to projects with extensive ethnographic studies others to small-scale research, and a few projects have no data but have built the personas entirely on own existing assumptions.

	The participants who expressed less satisfaction with the method were found in the companies that had some data, but where the data sample was not experienced as being rich.

	we can distinguish between two approaches to the use of personas: personas as an integrated part of the development process or an ad hoc approach where the method is used less systematically

	International projects:
	The challenges are mostly connected to data collection, as it is described as especially challenging to get enough data and to know when there is enough data. The project participants try to solve this by collecting data from different parts of the world, but none of the participants have data from all the markets they address.

	The companies either develop persona descriptions that represent users from different countries (e.g. one Finnish, one Polish, etc.), or descriptions that are so general that they can fit a wide variety of countries.

	Furthermore, if personas are entirely based on qualitative data, there is opposition against the research approach, as quantitative data is perceived as more credible than qualitative data.

	[@bodker_personas_2012]

	!!! Really good literature review!

## 2.4 Research gap

There has been little focus in research on persona generation based on secondary data. Sometimes project doesn't allow for primary data collection, e.g. ethnographies, interviews or other user research methods. This thesis aims to expand the current knowledge on light weight secondary data based persona generation process and designers' attitudes towards the generated personas.

> Taas, based on my extensive literature review tai lähde (little research on..)
