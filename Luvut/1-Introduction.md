
# 1. Introduction

***TODO***

- Kirjoita kommentit auki kunnon kappaleiksi

http://www.cs.tut.fi/kurssit/SGN-16006/academic_writing/cars_model_handout.pdf

----

The use of personas in user centered design has received considerable attention since their introduction by @cooper_inmates_1999. Their use has been generally accepted into the common design methodology, especially in the popular literature.


	[@pruitt_persona_2006]
	Personas are fictitious, specific, concrete representations of target users. The notion of personas was created by Alan Cooper and popularized in his 1999 book The Inmates Are Running the Asylum: Why High Tech Products Drive Us Crazy and How To Restore The Sanity [Cooper 1999]. Personas put a face on the user — a memorable, engaging, and actionable image that serves as a design target. They convey information about users to your product team in ways that other artifacts cannot.

	[@putnam_bridging_2010]

	A persona is an archetypal character which represents a group of users who share common goals, attitudes and behaviors when interacting with a particular product or service (A. Cooper, Reimann, & Cronin, 2007; Mulder & Yaar, 2007; Pruitt & Adlin, 2006)

	[@nielsen_personas:_2013]

	In literature about the method, there are today four differ- ent perspectives regarding personas: Cooper’s goal-directed perspective; Grudin, Pruitt, and Adlin’s role-based perspective; the engaging perspective that I myself use, which emphasises how the story can engage the reader (Sønderstrup-Andersen 2007); and the fiction-based perspective. The first three perspectives have in com- mon that the persona descriptions should be founded on data. The last perspective, the fiction-based perspective, does not include data as basis for persona descrip- tion but creates personas from the designers’ intuition and assumptions; they have names such as ad hoc personas (Norman 2004) and extreme characters (Djajadiningrat et al. 2000).

> Persoona määritelmä
> Meta-analyysi persoonista? Lyhyesti tärkein/tärkeimmät ominaisuudet, kirjallisuuden reaktiot

Research has tended to focus using primary data sources for personas. However, less attention has been paid to the personas generated based on secondary data.

> Miksi on tärkeää tutkia secondary data, miksi muidenkin mielestä mielenkiintoista?

> Secondary/primary data määritelmä

[@nielsen_personas:_2013, pp. 44-43]

This thesis studies the persona generation process when no primary data is available and to study how UX Designers perceive the reliability of the generated personas.

> Miksi reliability?

> @putnam_bridging_2010:
> Therefore, the usability and perceived usefulness of personas/scenarios (or any research summarizations) will directly affect a persona/scenario's capacity to not only convey the mock user, but to persuade the designer to adopt the mock designer role. As such, persona and scenario value can be affected by both presentation and designer perception of the methods that were employed in the persona and scenario creation.

The research questions set for the the thesis are:

- How personas can be generated based on secondary data?
- Can secondary data produce personas that are perceived as reliable?
- How well the generated personas can convey the feeling of knowing real user needs to the designers

> Metodi, mikä on tämän dipan tapa ratkaista ongelma, mun approach

My results indicate that...

> Päälöydäkset

The thesis is divided into five sections. First, the research around personas, their construction and use is explored. Second, the study methods are presented. Third, the study results are presented. Fourth, the analysis of the ressults is presented. Fifth, the implications of the findings are discussed.



	## 1.1 Background

	Persona is a design tool popularized by @cooper_inmates_1999 that helps the designer to empathize with the end user. @cooper_inmates_1999 defines persons to be hypothetical archetypes of actual users. Persona is formed by combining aspects of multiple target users into one relatable 'person', a persona. Usually personas are formed based on empirical research: e.g. interviews and ethnographies.

	Focus of much of the persona research is how to use extensive interview and ethnographic research of the end users to form data driven personas. However, designers do not always develop personas based on data but they still use them to inform their design [@chang_personas:_2008].

	Little research has been conducted on how well personas can be generated by secondary data with no direct end user involvement and how their use affects the design process.

	> Viite TAI 'based on my extensive literature review'
	>
	> Putnam väikkäri


	While @cooper_inmates_1999 strongly advocated the use of data and research driven personas, he also argued that it is more important for a persona to be precise than accurate to be a valuable design tool.

	## 1.2 Objectives

	The aim of the thesis is to study the persona generation process with secondary data and study how the personas, while not necessarily accurate, can still function as valuable design tools.


	> Scope:
	>
	> EU-projektin kohderyhmä
	>
	> Saanko kertoa julkisesti?
	>
	> Rajaus MPAT kontesktiin
	>
	> Diplomityössä ei tarvitse tuottaa teoreettisesti uutta tietoa. Uutuusarvo tulee kontekstista.


	The thesis is going to explore persona creation with the help of secondary data. The secondary data can come from statistical or demographic data, market research, public research or other internal projects. The secondary data can be either qualitative or quantitative in nature. The common factor is that the data has not initially been collected for this project.

	The research questions are:

	+ how personas can be formed based on secondary data
	+ can secondary data produce reliable personas that are perceived as reliable
	+ how well the generated personas can communicate real user needs to the designers
	+ how well the generated personas can convey the feeling of knowing real user needs to the designers

	User demographics and data will be gathered from public research, public data sources and from partners. Persona formation research will be consulted and reviewed for best practices on persona formation. Marketing research on customer segments and customer insight will supplement the quantitative methodologies. Personas will be generated based on the gathered data and the literature review.

	To measure the success of the personas, interviews will be conducted with UX designers. By the time of the interviews, the designers will have been using the personas to support their design work.


	The outcome of the research is expected to be a process that can be used to generate valid and design-inspiring personas from secondary data. The process should be general enough that it can be used in other contexts and projects.


