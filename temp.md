


Hyvä artikkeli. Kertoi casesta, jossa oli luotu persoonat toissiajisesta datasta Kirgistanissa.

Persona generation process:

1. Collect data
	- Data collected beforehand
	- Two data sources: qualitative survey of 1000 participants with answers available & interview results of focus group and individual interviews with total n=12
2. Segment the users
	- Segmentation based on the survey results
	- Throughout statistical analysis to find statistically significant groups
3. Create persona for each user segment and develop context scenarios for each persona
	- Personas created and fleshed out based on the focus group and the individual interviews

Quotes:

This work also recognizes that it is difacult and resource intensive to conduct user research in devel- oping or culturally distant regions, and our goal is to demonstrate ways that researchers can use unconventional data sources to communicate with design teams about end users.

Additionally, the methods dis- cussed in this article might help user researchers and designers repurpose qualitative work from anthro- pologists, sociologists, and other researchers to cre- ate personas and scenarios.

The creation of personas and scenarios commonly uses face-to-face interviews and observation studies to examine current and possible future uses of a speciac product or service (Cooper et al., 2007).

This case study demonstrates that researchers can use accepted UCD methods (personas and scenarios) to help designers make appropriate technology design decisions, even when they lack the resources to con- duct arsthand research on a given product or ser- vice.

we used two sets of previously collected data to create the personas and scenarios.

The arst set of data was from an April–May 2007 large-scale social survey of 1,000 respondents, aged 15 and older, that was administered in urban and rural areas in several regions in Kyrgyzstan. [*Survey answers available -Pekka*]

The second set of data used to inform the personas and scenarios was from focus groups and interviews conducted by three University of Washington researchers in two Kyrgyzstan sites: the capital city of Bishkek and Kara Balta, a smaller suburban city.

While there is no one recipe for creating personas and scenarios, the literature agrees on three basic steps: (a) collect data about users, (b) segment the users, and (c) create a per- sona for each user segment and develop context scenarios for each persona (Cooper et al., 2007; Pruitt & Adlin, 2006).